<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Smart Bazaar an E-commerce Online Shopping Category Flat Bootstrap Responsive Website Template | Login :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Bazaar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> 
<link href="css/animate.min.css" rel="stylesheet" type="text/css" media="all" /><!-- animation -->
<link href="css/menu.css" rel="stylesheet" type="text/css" media="all" /> <!-- menu style -->   
<!-- //Custom Theme files -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-2.2.3.min.js"></script>
<script src="js/notify.js"></script> 
<script src="js/jquery-scrolltofixed-min.js" type="text/javascript"></script><!-- fixed nav js -->
<script>
    $(document).ready(function() {
    	
    	$(document).ajaxStart(function(){
            $("#wait").css("display", "block");
           
    	   });
        $(document).ajaxComplete(function(){
    	        $("#wait").css("display", "none");
    	 });

          $("#password").keypress(function(e){
        	  if(e.which==13){
        	  if($("#email").val()==""){
    			  $("#email").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });  
    		  }else if($("#password").val()==""){
    			  $("#password").notify("Empty Field Not Allowed!!!!",{
    				  position:"right"
    			  });
    		  }else{
    		  
    		$.ajax({
    		url:"LoginServlet",
    		method:"post",
    		data:{
    		method:"login",
    		email:$("#email").val(),
    		password:$("#password").val()
    		
    		},
    		success:function(a){
    			if(a=="success"){
    				
    				
    			window.location.assign("index.jsp");
    				
    			}
    			else{
    				
    				$("#forgot").show();
    				$("#show").show(1000);
    				
    			}
    			
    			
    		}
    			
    		
    		
    		
    	});	 
    		  } 	
        	  }
        	 
         });
         
        
    	  $("#login").click(function (){	
    			

    		       			
    		   if($("#email").val()==""){
    			  $("#email").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });  
    		  }else if($("#password").val()==""){
    			  $("#password").notify("Empty Field Not Allowed!!!!",{
    				  position:"right"
    			  });
    		  }else{
    		  
    		$.ajax({
    		url:"LoginServlet",
    		method:"post",
    		data:{
    		method:"login",
    		email:$("#email").val(),
    		password:$("#password").val()
    		
    		},
    		success:function(a){
    			
    			if(a=="success"){
    				
    		
    			window.location.assign("index.jsp");
    				
    			}
    			else{
    				
    				$("#forgot").show();
    				$("#show").show(1000);
    				
    			}
    			
    			
    		}
    			
    		
    		
    		
    	});	 
    		  } 	
    	});

    	  $("#submit").click(function (){
    			
    		  $.ajax({
    			url:"LoginServlet",
    			method:"Post",
    			data:{
    				method:"verifyEmail",
    			email:$("#check-email").val()
    				
    			},
    			success:function(a){
    				
    			 	 if(a=="success"){
    			 		 
    			 		  $("#check-email").hide(1000);  
    			 		  $("#check-answer").show(1000);
    			 		  $("#select").show(1000);
    	                  $("#submit").hide();
    	                  $("#confirm").show();
    			 		 
    			 	 }else if(a=="ban"){
    			 		 
    			 		 $("#ban").show();
    			 	 }
    			 	 else{
    			 		 $("#ban").hide();
    			 		 $("#check-email").notify("Email Not Found");
    			 	 }
    	         			  
    				
    			}
    		});
    		
    		    
    		 
    	  });
    	  $("#confirm").click(function (){
    		  
    	  $.ajax({
    		 url:"LoginServlet",
    		 method:"POST",
    		 data:{
    			 method:"checkAnswers", 
    			 question:$("#select option:selected").index(),
    			 answer:$("#check-answer").val(),
    		
    				
    			 
    		 },
    		 success:function(a){
    			 
    			 if(a=="success"){
    				 
    				 window.location.assign("newpassword.jsp");
    				 
    			 }else if(a=="ban"){
    				 
    				$("#ban").show(); 
    			 }else{
    				 
    				 $("#check-answer").notify("Answer Dosent Exists!!");
    			 }
    			 
    			 
    		 }
    		  
    		  
    	  });
    	  
    	  });
    	  
    	
        $('.header-two').scrollToFixed();  
        // previous summary up the page.

        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];

            summary.scrollToFixed({
                marginTop: $('.header-two').outerHeight(true) + 10, 
                zIndex: 999
            });
        });
    });
  
   
</script>
<!-- //js --> 
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lovers+Quarrel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Offside' rel='stylesheet' type='text/css'> 
<!-- web-fonts -->  
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function() {
		
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->
</head>
<body>
<%@ include file="header.jsp" %>	
	<!-- login-page -->
			 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Forget Password </h4>
      </div>
      <div class="modal-body">
        <select id="select" style="display:none;" class="form-control">
							<option>What is your mother name?</option>
							<option>Which is your favourite game?</option>
							<option>Which is your favourite pat name?</option>
							
	    </select>
	    
	    <input  style="display:none;margin-top:10px;"id="check-answer" type="text" class="form-control" placeholder="Enter Answer"/>
	    <input   id="check-email" class="form-control" type="email" placeholder="Enter Email" />
       <br>
        <p id="ban" style="display:none;">You are ban for 60 sec.</p> 
       
       </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button style="display:none;" type="button" class="btn btn-default" id="confirm">Confirm</button>
         <button type="button" class="btn btn-default" id="submit">Submit</button>
      
      </div>
    </div>

  </div>
</div>
	<div class="login-page">
		<div class="container"> 
			<h3 class="w3ls-title w3ls-title1">Login to your account</h3> 
			 
			<div class="login-body">
				<form>
					<input type="text" class="user" name="email" id="email" placeholder="Enter your email" required="">
					<input type="password" name="password" class="lock" id="password" placeholder="Password" required="">
					<input class="btn btn-primary" style="width:430px;" type="button" value="Login" id="login">
					<div id="wait" style="width:80px;height:90px;position:absolute;display:none;padding:2px;left:47%;margin-top:5px;"><img src='images/Preloader_8.gif' width="70" height="70" /><br></div>
					
					<br>
					<p style="font-size:20px;color:red;display:none;" id="show">Invalid username or password</p>
					<div class="forgot-grid">
						<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>Remember me</label>
						<div class="forgot">
							<a data-toggle="modal" data-target="#myModal" href="" id="forgot" style="display:none;">Forgot Password?</a>
						</div>
						<div class="clearfix"> </div>
					</div>
				</form>
			</div>  
			<h6> Not a Member? <a href="signup.jsp">Sign Up Now </a> </h6> 
			<div class="login-page-bottom social-icons">
				<h5>Recover your social account</h5>
				<ul>
					<li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
					<li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
					<li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
					<li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
					<li><a href="#" class="fa fa-rss icon rss"> </a></li> 
				</ul> 
			</div>
		</div>
	</div>
	<!-- //login-page --> 
	<!-- footer-top -->
	<div class="w3agile-ftr-top">
		<div class="container">
			<div class="ftr-toprow">
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-truck" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>FREE DELIVERY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div> 
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-user" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>CUSTOMER CARE</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div> 
					<div class="clearfix"> </div>
				</div>
				<div class="col-md-4 ftr-top-grids">
					<div class="ftr-top-left">
						<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
					</div> 
					<div class="ftr-top-right">
						<h4>GOOD QUALITY</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tempus justo ac </p>
					</div>
					<div class="clearfix"> </div>
				</div> 
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	<!-- //footer-top --> 
	<!-- subscribe -->
	<div class="subscribe"> 
		<div class="container">
			<div class="col-md-6 social-icons w3-agile-icons">
				<h4>Keep in touch</h4>  
				<ul>
					<li><a href="#" class="fa fa-facebook icon facebook"> </a></li>
					<li><a href="#" class="fa fa-twitter icon twitter"> </a></li>
					<li><a href="#" class="fa fa-google-plus icon googleplus"> </a></li>
					<li><a href="#" class="fa fa-dribbble icon dribbble"> </a></li>
					<li><a href="#" class="fa fa-rss icon rss"> </a></li> 
				</ul>
				<ul class="apps"> 
					<li><h4>Download Our app : </h4> </li>
					<li><a href="#" class="fa fa-apple"></a></li>
					<li><a href="#" class="fa fa-windows"></a></li>
					<li><a href="#" class="fa fa-android"></a></li>
				</ul>
			</div> 
			<div class="col-md-6 subscribe-right">
				<h4>Sign up for email and get 25%off!</h4>  
				<form action="#" method="post"> 
					<input type="text" name="email" placeholder="Enter your Email..." required="">
					<input type="submit" value="Subscribe">
				</form>
				<div class="clearfix"> </div> 
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //subscribe --> 
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="footer-info w3-agileits-info">
				<div class="col-md-4 address-left agileinfo">
					<div class="footer-logo header-logo">
						<h2><a href="index.html"><span>S</span>mart <i>Bazaar</i></a></h2>
						<h6>Your stores. Your place.</h6>
					</div>
					<ul>
						<li><i class="fa fa-map-marker"></i> 123 San Sebastian, New York City USA.</li>
						<li><i class="fa fa-mobile"></i> 333 222 3333 </li>
						<li><i class="fa fa-phone"></i> +222 11 4444 </li>
						<li><i class="fa fa-envelope-o"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul> 
				</div>
				<div class="col-md-8 address-right">
					<div class="col-md-4 footer-grids">
						<h3>Company</h3>
						<ul>
							<li><a href="about.html">About Us</a></li>
							<li><a href="marketplace.html">Marketplace</a></li>  
							<li><a href="values.html">Core Values</a></li>  
							<li><a href="privacy.html">Privacy Policy</a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Services</h3>
						<ul>
							<li><a href="contact.html">Contact Us</a></li>
							<li><a href="login.html">Returns</a></li> 
							<li><a href="faq.html">FAQ</a></li>
							<li><a href="sitemap.html">Site Map</a></li>
							<li><a href="login.html">Order Status</a></li>
						</ul> 
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Payment Methods</h3>
						<ul>
							<li><i class="fa fa-laptop" aria-hidden="true"></i> Net Banking</li>
							<li><i class="fa fa-money" aria-hidden="true"></i> Cash On Delivery</li>
							<li><i class="fa fa-pie-chart" aria-hidden="true"></i>EMI Conversion</li>
							<li><i class="fa fa-gift" aria-hidden="true"></i> E-Gift Voucher</li>
							<li><i class="fa fa-credit-card" aria-hidden="true"></i> Debit/Credit Card</li>
						</ul>  
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //footer -->		
	<div class="copy-right"> 
		<div class="container">
			<p>© 2016 Smart bazaar . All rights reserved | Design by <a href="http://w3layouts.com"> W3layouts.</a></p>
		</div>
	</div> 
	<!-- cart-js -->
	<script src="js/minicart.js"></script>
	<script>
        w3ls.render();

        w3ls.cart.on('w3sb_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();

        		for (i = 0, len = items.length; i < len; i++) {
        			items[i].set('shipping', 0);
        			items[i].set('shipping2', 0);
        		}
        	}
        });
    </script>  
	<!-- //cart-js --> 	 
	<!-- menu js aim -->
	<script src="js/jquery.menu-aim.js"> </script>
	<script src="js/main.js"></script> <!-- Resource jQuery -->
	<!-- //menu js aim --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>
</html>