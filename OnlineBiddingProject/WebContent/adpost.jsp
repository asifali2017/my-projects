<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Smart Bazaar an E-commerce Online Shopping Category Flat Bootstrap Responsive Website Template | Login :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Smart Bazaar Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> 
<link href="css/animate.min.css" rel="stylesheet" type="text/css" media="all" /><!-- animation -->
<link href="css/menu.css" rel="stylesheet" type="text/css" media="all" /> <!-- menu style -->   
<!-- //Custom Theme files -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- js -->
<script src="js/jquery-2.2.3.min.js"></script>
<!-- <script src="js/notify.js"></script> -->
<script src="js/alertify.js"></script> 
<script src="js/jquery-scrolltofixed-min.js" type="text/javascript"></script><!-- fixed nav js -->
<script>
    $(document).ready(function() {
    	
    	$.ajax({
    		url:"HomeServlet",
    		method:"post",
    		data:{
    		method:"fillCategoryCombo"	
    			
    		},
    		success:function(a){
    		
    			
    			for(x in a){	
    				
    				$("#category").append("<option>"+a[x].categoryName+"</option>");
    				
    			}
    			
    			
    		}
    	});
    	$('#file').change(function(){
    		var files = $(this)[0].files;
    	    if(files.length > 4){
    	        alert("you can select max 4 files.");
    	    }
    	});
    	
    	$("#submit").click(function (){
    	
    	   if($("#title").val()==""){
    	  		$("#title").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });
    	  	}else if($("#description").val()==""){
    	  		$("#description").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });
    	  	}else if($("#minprice").val()==""){
    	  		$("#minprice").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });
    	  	}else if($("#endingdate").val()==""){
    	  		$("#endingdate").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });	
    	  	}else if($("#contact").val()==""){
    	  		$("#contact").notify("Empty Field Not Allowed!!!!",{
    				  
    				  position:"right"
    			  });
    	  	}else{
    	        //stop submit the form, we will post it manually.
    	        event.preventDefault();

    	        
    	        // Get form
    	        var form = $('#upload-form')[0];

    			// Create an FormData object
    	        var data = new FormData(form);

    		
    			// If you want to add an extra field for the FormData
    	        data.append("title",$("#title").val());
                data.append("category",$("#category option:selected").index());
                data.append("description",$("#description").val());
                data.append("minprice",$("#minprice").val());
                data.append("endingdate",$("#endingdate").val());
                data.append("contact",$("#contact").val()) ;
    			// disabled the submit button
    	        $("#submit").prop("disabled", true);

    	        $.ajax({
    	            type: "POST",
    	            enctype: 'multipart/form-data',
    	            url: "PostAdServlet",
    	            data:data,
    	            processData: false,
    	            contentType: false,
    	            cache: false,
    	            timeout: 600000,
    	            success: function (data) {
    	            	
    	            	$("#title").val("");
     	                $("#category").val("");
     	                $("#description").val("");
     	                $("#minprice").val("");
     	                $("#endingdate").val("");
     	                $("#contact").val("");
               	
    	                console.log("SUCCESS : ", data);
    	                $("#submit").prop("disabled", false);

    	            },
    	            error: function (e) {
    	                console.log("ERROR : ", e);
    	                $("#submit").prop("disabled", false);

    	            }
    	        });
    		
    	  		}
 
 });
    	

	  
    	
      //  $('.header-two').scrollToFixed();  
        // previous summary up the page.

        var summaries = $('.summary');
        summaries.each(function(i) {
            var summary = $(summaries[i]);
            var next = summaries[i + 1];

            summary.scrollToFixed({
                marginTop: $('.header-two').outerHeight(true) + 10, 
                zIndex: 999
            });
        });
    });
   
</script>
<!-- //js --> 
<!-- web-fonts -->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lovers+Quarrel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Offside' rel='stylesheet' type='text/css'> 
<!-- web-fonts -->  
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>	
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function() {
		
		 	var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			
			/* $().UItoTop({ easingType: 'easeOutQuart' });
		 */	
		}); 
	</script>
<style>
input[type="text"],input[type="date"],textarea,select{
height:40px;

}
</style>	
	<!-- //smooth-scrolling-of-move-up -->
</head>
<body>
<header> <!--Header Start-->
<%@ include file="header.jsp" %>
</header> <!--Header Close-->
 <div >
		<div class="container">
		<div class="row"> 
			<h3 style="margin-top:20px;" class="w3ls-title w3ls-title1">Post An Ad</h3> 
			<div style="width:500px;margin-left:350px;">
			   
				<form id="upload-form"  action="PostAdServlet" method="post" enctype="multipart/form-data">
				<div class="form-group">
				<label  for="title">Title<span style="color:red;">*<span></label>
				<input type="text"  id="title"  class="form-control" required="">
				</div>
				<div class="form-group">
				<label for="category">Category<span style="color:red;">*<span></label>				
					<select class="form-control" id="category">
					<option>Select Category</option>
					</select>
				</div>	
				<div class="form-group">
				<label for="description">Description<span style="color:red;">*<span></label>
					<textarea  id="description"  class="form-control"></textarea>
			    </div>
			    <div class="form-group">
				<label for="minprice">Minimum Price<span style="color:red;">*<span></label>				
					<input type="text"   id="minprice" class="form-control"   required="">
				</div>
				<div class="form-group">
				<label for="endingdate">End Date<span style="color:red;">*<span></label>
					<input type="date" id="endingdate" class="form-control"  class="form-control"  required="">
				</div>
				<div class="form-group">
				<label for="contact">Contact<span style="color:red;">*<span></label>
					<input type="text"   class="form-control" id="contact"  required="">
				</div>
			  <div class="form-group">
			  <label for="file">Upload Images<span style="color:red;">*<span></label>		       
              <input type="file" id="file" name="file1" accept="image/*"  multiple="muliple" required/><br>
				</div>	
					<input class="btn btn-primary" style="width:500px;" type="button" value="Submit" id="submit">
				
				</form>
				
			</div>
			</div>
			</div>
			</div>
	<!-- footer -->
	<div class="footer" style="margin-top:70px;">
		<div class="container">
			<div class="footer-info w3-agileits-info">
				<div class="col-md-4 address-left agileinfo">
					<div class="footer-logo header-logo">
						<h2><a href="index.html"><span>S</span>mart <i>Bazaar</i></a></h2>
						<h6>Your stores. Your place.</h6>
					</div>
					<ul>
						<li><i class="fa fa-map-marker"></i> 123 San Sebastian, New York City USA.</li>
						<li><i class="fa fa-mobile"></i> 333 222 3333 </li>
						<li><i class="fa fa-phone"></i> +222 11 4444 </li>
						<li><i class="fa fa-envelope-o"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
					</ul> 
				</div>
				<div class="col-md-8 address-right">
					<div class="col-md-4 footer-grids">
						<h3>Company</h3>
						<ul>
							<li><a href="about.html">About Us</a></li>
							<li><a href="marketplace.html">Marketplace</a></li>  
							<li><a href="values.html">Core Values</a></li>  
							<li><a href="privacy.html">Privacy Policy</a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Services</h3>
						<ul>
							<li><a href="contact.html">Contact Us</a></li>
							<li><a href="login.html">Returns</a></li> 
							<li><a href="faq.html">FAQ</a></li>
							<li><a href="sitemap.html">Site Map</a></li>
							<li><a href="login.html">Order Status</a></li>
						</ul> 
					</div>
					<div class="col-md-4 footer-grids">
						<h3>Payment Methods</h3>
						<ul>
							<li><i class="fa fa-laptop" aria-hidden="true"></i> Net Banking</li>
							<li><i class="fa fa-money" aria-hidden="true"></i> Cash On Delivery</li>
							<li><i class="fa fa-pie-chart" aria-hidden="true"></i>EMI Conversion</li>
							<li><i class="fa fa-gift" aria-hidden="true"></i> E-Gift Voucher</li>
							<li><i class="fa fa-credit-card" aria-hidden="true"></i> Debit/Credit Card</li>
						</ul>  
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //footer -->		
	<div class="copy-right"> 
		<div class="container">
			<p>© 2016 Smart bazaar . All rights reserved | Design by <a href="http://w3layouts.com"> W3layouts.</a></p>
		</div>
	</div> 
	<!-- cart-js -->
	<script src="js/minicart.js"></script>
	<script>
        w3ls.render();

        w3ls.cart.on('w3sb_checkout', function (evt) {
        	var items, len, i;

        	if (this.subtotal() > 0) {
        		items = this.items();

        		for (i = 0, len = items.length; i < len; i++) {
        			items[i].set('shipping', 0);
        			items[i].set('shipping2', 0);
        		}
        	}
        });
    </script>  
	<!-- //cart-js --> 	 
	<!-- menu js aim -->
	<script src="js/jquery.menu-aim.js"> </script>
	<script src="js/main.js"></script>
	<script src="js/notify.js"></script>
	 <!-- Resource jQuery -->
	<!-- //menu js aim --> 
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>
</html>