<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Bootstrap Chat Box Example</title>
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="msgcss/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME  CSS -->
    <link href="msgcss/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="msgcss/style.css" rel="stylesheet" />
    <script src="js/vendor/jquery.js"></script>
    <script>
    $(document).ready(function (){
    	 var buyerid = "${param.buyerId}";
    	 var id="${param.id}";
      	 var userid = "${sessionScope.id}";
      	 var username = "${sessionScope.name}"
      	 
    	$("#sendMessage").click(function (){
        $.ajax({
        		url:"MessageServlet",
        		type:"post",
        		data:{
        			method:"sendmessage",
        			productId:id,
        			userId:userid,
        			buyerId:buyerid,
        			message:$("#message").val()
        		},
               success:function(a){
            	  if(a=="success"){
            		  alert("Success");
            	  }else{
            		  alert("UnSuccess");
            	  }
            	   
            	   
               }
        	
        	
        });
    	
    });
       $.ajax({
    		url:"MessageServlet",
    		type:"post",
    		data:{
    			method:"getMessages",
    			userid:userid,
    			buyerid:buyerid,
    		},
    		success:function(a){
    		    var message  = "";
    			//alert(a);
    			 for(x in a){
    				 
    				 if(username==a[x].sender){
          			 message = "<div class='chat-box-left'>"
    				 +a[x].message
                     +"</div>"
                     +"<div class='chat-box-name-left'>"
                         +"<img src='img/user.png' alt='bootstrap Chat box user image' class='img-circle' />"
                         +"- "+a[x].sender+""
                     +"</div>";
    				 }  else{
    					message = "<hr class='hr-clas' />"
                        +"<div class='chat-box-right'>"
                        +a[x].message
                    +"</div>"
                    +"<div class='chat-box-name-right'>"
                        +"<img src='img/user.gif' alt='bootstrap Chat box user image' class='img-circle' />"
                        +"-"+a[x].sender+""
                    +"</div>"; 
    				 } 
    				  
    				 $("#message-container").append(message);
    			 }
    			
    			
    		}
    	})
    });
    </script>
</head>
<body>


    <div class="container">
        <div class="row pad-top pad-bottom">


            <div class=" col-lg-6 col-md-6 col-sm-6">
                <div class="chat-box-div">
                    <div class="chat-box-head">
                        GROUP CHAT HISTORY
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <span class="fa fa-cogs"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#"><span class="fa fa-map-marker"></span>&nbsp;Invisible</a></li>
                                    <li><a href="#"><span class="fa fa-comments-o"></span>&nbsp;Online</a></li>
                                    <li><a href="#"><span class="fa fa-lock"></span>&nbsp;Busy</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#"><span class="fa fa-circle-o-notch"></span>&nbsp;Logout</a></li>
                                </ul>
                            </div>
                    </div>
                    <div class="panel-body chat-box-main">
                        <div id="message-container">
                       <!--  <div class="chat-box-left">
                          Hello My Name is Amir Ali How May I help you
                        </div>
                        <div class="chat-box-name-left">
                            <img src="img/user.png" alt="bootstrap Chat box user image" class="img-circle" />
                            - Amir Ali
                        </div>
                        <hr class="hr-clas" />
                        <div class="chat-box-right">
                            Sir I want to ask that what is actually final rate of this product
                        </div>
                        <div class="chat-box-name-right">
                            <img src="img/user.gif" alt="bootstrap Chat box user image" class="img-circle" />
                            - Asif Ali
                        </div> -->
                       </div>
                    </div>
                    <div class="chat-box-footer">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter Text Here..." id='message'>
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="button" id="sendMessage">SEND</button>
                            </span>
                        </div>
                    </div>

                </div>

            </div>
           

        </div>
    </div>

    <!-- USING SCRIPTS BELOW TO REDUCE THE LOAD TIME -->
    <!-- CORE JQUERY SCRIPTS FILE -->
    
    <!-- CORE BOOTSTRAP SCRIPTS  FILE -->
    <script src="js/bootstrap.js"></script>
</body>

</html>
