<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Messenger</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/chat.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="js/vendor/jquery.js"></script>
    <script>
    $(document).ready(function (){
   	 var buyerid = "${param.buyerId}";
   	 var id="${param.id}";
     	 var userid = "${sessionScope.id}";
     	 var username = "${sessionScope.name}"
     	 
   	$("#send").click(function (){
       $.ajax({
       		url:"MessageServlet",
       		type:"post",
       		data:{
       			method:"sendmessage",
       			productId:id,
       			userId:userid,
       			buyerId:buyerid,
       			message:$("#message").val()
       		},
              success:function(a){
           	  if(a=="success"){
           		  alert("Success");
           	  }else{
           		  alert("UnSuccess");
           	  }
           	   
           	   
              }
       	
       	
       });
   	
   });
      
     $.ajax({
    		url:"MessageServlet",
    		method:"post",
    		data:{
    			method:"userMessages",
    			userid:userid
    			
    		},
    		success:function(a){
    			
    			var b="";
    			//$("#container2").empty();
    			for(x in a){
    				b += "<div class='row sideBar-body'>"
    	            +"<div class='col-sm-3 col-xs-3 sideBar-avatar'>"
    	              +"<div class='avatar-icon'>"
    	                +"<img src='https://bootdey.com/img/Content/avatar/avatar1.png'>"
    	              +"</div>"
    	            +"</div>"
    	            +"<div class='col-sm-9 col-xs-9 sideBar-main'>"
    	              +"<div class='row'>"
    	                +"<div class='col-sm-8 col-xs-8 sideBar-name'>"
    	                  +"<a href='chat.jsp?buyerId="+a[x].id+"&id="+a[x].productId+"''><span class='name-meta'>"+a[x].name
    	                +"</a>"
    	                +"</div>"
    	              +"</div>"
    	            +"</div>"
    	          +"</div>";	
    	          
    			}
    			$("#users").append(b);
    		}
    		
    	});
     
    setInterval(function(){	  
    $.ajax({
   		url:"MessageServlet",
   		type:"post",
   		data:{
   			method:"getMessages",
   			userid:userid,
   			buyerid:buyerid,
   		},
   		success:function(a){
   		    var message  = "";
   		    $("#heading").empty();
   		    if(a[0].receiver!= username){
   		 $("#heading").append("<a class='heading-name-meta' >"+a[0].receiver+"</a>"); 
   		    }else{
   		   $("#heading").append("<a class='heading-name-meta' >"+a[0].sender+"</a>"); 
   	   		    	
   		    }
   		 $("#conversation").empty();
   			 for(x in a){
   				  if(username==a[x].sender){
         			 message = "<div class='row message-body'>"
         	          +"<div class='col-sm-12 message-main-receiver'>"
         	            +"<div class='receiver'>"
         	              +"<div class='message-text'>"
         	               +a[x].message
         	              +"</div>"
         	              +"<span class='message-time pull-right'>"
         	                +a[x].messageArrivalTime
         	              +"</span>"
         	            +"</div>"
         	          +"</div>"
         	        +"</div>";
   				 }  else{
   					 message = "<div class='row message-body'>"
   		          +"<div class='col-sm-12 message-main-sender'>"
   	            +"<div class='sender'>"
   	              +"<div class='message-text'>"
   	               +a[x].message
   	              +"</div>"
   	              +"<span class='message-time pull-right'>"
   	           +a[x].messageArrivalTime
   	              +"</span>"
   	            +"</div>"
   	          +"</div>"
   	        +"</div>" 
   				 } 
   				  
   				 $("#conversation").append(message); 
   			 }
   			
   			
   		}
   	});
    },1000*5);
    });
   
    
    </script>
</head> 
<body>


<div class="container app">
  <div class="row app-one">
    <div class="col-sm-4 side">
      <div class="side-one">
        <div class="row heading">
          <div class="col-sm-3 col-xs-3 heading-avatar">
            <div class="heading-avatar-icon">
              <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
            </div>
          </div>
          <div class="col-sm-1 col-xs-1  heading-dot  pull-right">
            <i class="fa fa-ellipsis-v fa-2x  pull-right" aria-hidden="true"></i>
          </div>
          <div class="col-sm-2 col-xs-2 heading-compose  pull-right">
            <i class="fa fa-comments fa-2x  pull-right" aria-hidden="true"></i>
          </div>
        </div>

        <div class="row searchBox">
          <div class="col-sm-12 searchBox-inner">
            <div class="form-group has-feedback">
              <input id="searchText" type="text" class="form-control" name="searchText" placeholder="Search">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          </div>
        </div>

        <div class="row sideBar">
          <div id="users">
          </div>
         </div>
      </div>

      <div class="side-two">
        <div class="row newMessage-heading">
          <div class="row newMessage-main">
            <div class="col-sm-2 col-xs-2 newMessage-back">
              <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </div>
            <div class="col-sm-10 col-xs-10 newMessage-title">
              New Chat
            </div>
          </div>
        </div>

        <div class="row composeBox">
          <div class="col-sm-12 composeBox-inner">
            <div class="form-group has-feedback">
              <input id="composeText" type="text" class="form-control" name="searchText" placeholder="Search People">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          </div>
        </div>

        
      </div>
    </div>

    <div class="col-sm-8 conversation">
      <div class="row heading">
        <div class="col-sm-2 col-md-1 col-xs-3 heading-avatar">
          <div class="heading-avatar-icon">
            <img src="https://bootdey.com/img/Content/avatar/avatar6.png">
          </div>
        </div>
        <div class="col-sm-8 col-xs-7 heading-name">
           <div id="heading">
           </div>
          <span class="heading-online">Online</span>
        </div>
        <div class="col-sm-1 col-xs-1  heading-dot pull-right">
          <i class="fa fa-ellipsis-v fa-2x  pull-right" aria-hidden="true"></i>
        </div>
      </div>

      <div class="row message" id="conversation">     
      </div>

      <div class="row reply">
        <div class="col-sm-1 col-xs-1 reply-emojis">
          <i class="fa fa-smile-o fa-2x"></i>
        </div>
        <div class="col-sm-9 col-xs-9 reply-main">
          <input type="text" id="message" class="form-control">
        </div>
        <div class="col-sm-1 col-xs-1 reply-recording">
          <i class="fa fa-microphone fa-2x" aria-hidden="true" ></i>
        </div>
        <div class="col-sm-1 col-xs-1 reply-send">
         <i class="fa fa-send fa-2x" aria-hidden="true" id="send"></i>
          
        </div>
      </div>
    </div>
  </div>
</div>



<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"></script>

</body>
</html>