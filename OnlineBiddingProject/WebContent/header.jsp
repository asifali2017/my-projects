<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<c:if test="${param.logout!=null}">
<c:remove var="name" scope="session"></c:remove>
<c:redirect url="login.jsp"></c:redirect>
</c:if>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script src="js/vendor/jquery.js"></script>
<script>
$(document).ready(function (){
var userid = "${sessionScope.id}";
$("#messages").click(function (){
	
	$.ajax({
		url:"MessageServlet",
		method:"post",
		data:{
			method:"userMessages",
			userid:userid
			
		},
		success:function(a){
			
			var b="";
			$("#container2").empty();
			for(x in a){
		 b+="<li><a href='chat.jsp?buyerId="+a[x].id+"&id="+a[x].productId+"'><i class='fa fa-user' aria-hidden='true'></i>"+a[x].name+"</a></li>"
			}
			$("#container2").append(b);
		}
		
	});
});
$("#notifications").click(function (){
	$.ajax({
		url:"NotificationServlet",
		method:"post",
		data:{
			
			method:"getNotifications"
		},
		success:function(a){
			var b="";
			$("#container3").empty();
			for(x in a){
				if(a[x].sellerId==userid){
		 b+="<li><a href=''><i class='fa fa-user' aria-hidden='true'></i>"+a[x].notificationMessage+"</a></li>"
				}
				}
			$("#container3").append(b);
		}	
			
		
		
		
	});
	
	
});
});
</script>
</head>
<body>
<!-- header -->
	<div class="header">
		<div class="w3ls-header"><!--header-one--> 
			<div class="w3ls-header-left">
				<p><a href="#">UPTO $50 OFF ON LAPTOPS | USE COUPON CODE LAPPY </a></p>
			</div>
			<div class="w3ls-header-right">
				<ul>
				    <li class="dropdown head-dpdn">
				    	<a href="index.jsp" class="dropdown-toggle" ></i>Home</a>
					
				    </li>
					<li class="dropdown head-dpdn">
					    <c:choose>
					    <c:when test="${sessionScope.name!=null}">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i>${sessionScope.name}<span class="caret"></span></a>
						</c:when>
						<c:otherwise>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i>Account<span class="caret"></span></a>
						</c:otherwise>
						</c:choose>
						<ul class="dropdown-menu">
					        <c:if test="${sessionScope.name==null}">	
							<li><a href="login.jsp">Login </a></li>
							<li><a href="signup.jsp">Sign Up</a></li>
							</c:if>
							<c:if test="${sessionScope.name!=null}">
							<li><a href="header.jsp?logout">Logout </a></li> 
							<li><a href="">Change Password</a></li>
							</c:if>
						</ul> 
					</li> 
					 
					<li class="dropdown head-dpdn" >
						<a href="#" id="messages" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope" aria-hidden="true"></i>Messages<span class="caret"></span></a>
					<ul class="dropdown-menu" id="container2">
					</ul>
					
			 		</li>
			 		<li class="dropdown head-dpdn" >
						<a href="#" id="notifications" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell" aria-hidden="true"></i>Notifications<span class="caret"></span></a>
					<ul class="dropdown-menu" id="container3">
					</ul>
					
			 		</li> 
					<!-- <li class="dropdown head-dpdn">
						<a href="#" class="dropdown-toggle"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Credit Card</a>
					
					</li> 
					<li class="dropdown head-dpdn">
						<a href="help.html" class="dropdown-toggle"><i class="fa fa-question-circle" aria-hidden="true"></i> Help</a>
			</li>  -->
				</ul>
			</div>
			<div class="clearfix"> </div> 
		</div>
		<div class="header-two"><!-- header-two -->
			<div class="container">
				<div class="header-logo">
					<img src="images/logo5.png" width="250px" height="80px">
				</div>	
				<div class="header-search">
					<form action="#" method="post">
						<input type="search" name="Search" placeholder="Search for a Product..." required="">
						<button type="submit" class="btn btn-default" aria-label="Left Align">
							<i class="fa fa-search" aria-hidden="true"> </i>
						</button>
					</form>
				</div>
				<div class="header-cart"> 
					<div class="my-account">
						<a href="contact.html"><i class="fa fa-map-marker" aria-hidden="true"></i>Ad Post</a>						
					</div>
					
						<form action="adpost.jsp" > 
							
					        <button class="w3view-cart" type="submit"  value=""><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></button>
						
						</form>  
					
					<div class="clearfix"> </div> 
				</div> 
				<div class="clearfix"> </div>
			</div>		
		</div><!-- //header-two -->
		
	</div>
	<!-- //header --> 	
	
</body>
</html>