package com.oam.dao;

import java.util.List;

import com.oam.models.QualityModel;

public interface QualityDAO {

	public List<QualityModel> getAllQualities();
	
	
}
