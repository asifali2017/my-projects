<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Top Produces</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<div class="sub-banner my-banner3">
</div>
<div class="content">
	<div class="container">
		<div class="col-md-4 w3ls_dresses_grid_left" >
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Categories</h3>
					<div class="w3ls_dresses_grid_left_grid_sub">
						<div class="ecommerce_dres-type">
							<ul>
								<li><a href="men.html">Casuals</a></li>
								<li><a href="men.html">Slacks</a></li>
								<li><a href="men.html">Jeans</a></li>
								<li><a href="men.html">Shirts</a></li>
							</ul>
						</div>
					</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Color</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color">
						<ul>
							<li><a href="#"><i></i> Red(5)</a></li>
							<li><a href="#"><i></i> Brown(2)</a></li>
							<li><a href="#"><i></i> Yellow(3)</a></li>
							<li><a href="#"><i></i> Violet(6)</a></li>
							<li><a href="#"><i></i> Orange(2)</a></li>
							<li><a href="#"><i></i> Blue(1)</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<h3>Size</h3>
				<div class="w3ls_dresses_grid_left_grid_sub">
					<div class="ecommerce_color ecommerce_size">
						<ul>
							<li><a href="#">Medium</a></li>
							<li><a href="#">Large</a></li>
							<li><a href="#">Extra Large</a></li>
							<li><a href="#">Small</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="w3ls_dresses_grid_left_grid">
				<div class="dresses_img_hover">
					<img src="images/offer2.jpg" alt=" " class="img-responsive" />
					<div class="dresses_img_hover_pos">
						<h4>Upto<span>50%</span><i>Off</i></h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8 col-sm-8 women-dresses" id="test">
			
		</div>
	</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>