<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Fashion Club an Ecommerce Online Shopping Category  Flat Bootstrap responsive Website Template | Login :: w3layouts</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Fashion Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- css -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all" />
	<!--// css -->
	<!-- font -->
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- //font -->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.mask.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<!-- Alertify -->
	<link href="css/custom.css">
	
	<script src="js/alertify.min.js"></script>
	<!-- CSS -->
	<link rel="stylesheet" href="css/alertify.min.css"/>
	<!-- Default theme -->
	<link rel="stylesheet" href="css/themes/default.min.css"/>
	<!-- Semantic UI theme -->
	<link rel="stylesheet" href="css/themes/semantic.min.css"/>	
	<!-- 
	    RTL version
	-->
	<link rel="stylesheet" href="css/alertify.rtl.min.css"/>
	<!-- Default theme -->
	<link rel="stylesheet" href="css/themes/default.rtl.min.css"/>
	<!-- Semantic UI theme -->

	<script type="text/javascript" src="js/custom.js"></script>

</head>
<body>
	<div class="header-bottom-w3ls">
		<div class="container">
			<div class="col-md-6 logo-w3">
					<a href="index.jsp"><img src="images/logo3.png" alt=" " /><h1>Agriculture<span>Market</span></h1></a>
				</div>
				<div class="col-md-6 phone-w3l">
					<ul>
						<li><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span></li>
						<li>+18045403380</li>
					</ul>
				</div>
			<div class="clearfix"></div>
		</div>
	</div>
	
</body>
</html>