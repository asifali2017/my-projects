package com.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.models.CategoryModel;
import com.models.ImagesModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;

import daoImpl.PostedBidDaoImpl;

/**
 * Servlet implementation class PostAdServlet
 */
@WebServlet("/PostAdServlet")
public class PostAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PostAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (ServletFileUpload.isMultipartContent(request)) {
			//response.setContentType("application/json");
			HttpSession httpSession = request.getSession();
			int id = Integer.parseInt(httpSession.getAttribute("id").toString());
			PostedBidProductsModel bidProductsModel = new PostedBidProductsModel();
			SellerBuyerModel buyerModel = new SellerBuyerModel();
			buyerModel.setId(id);
			bidProductsModel.setSellerBuyerModel(buyerModel);
			try {

				List<FileItem> multiparts = new ServletFileUpload(

						new DiskFileItemFactory()).parseRequest((request));
				Set<ImagesModel> images = new HashSet<>();
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {

						String name = new File(item.getName()).getName();

						item.write(new File("C:\\upload" + File.separator + name));

						ImagesModel imagesModel = new ImagesModel();
						imagesModel.setPath("files" + File.separator + name);
						images.add(imagesModel);
						bidProductsModel.setImagesModels(images);
					} else if (item.isFormField()) {

						if (item.getFieldName().equals("title")) {
							String title = item.getString();
							bidProductsModel.setName(title);
						} else if (item.getFieldName().equals("category")) {
							String category = item.getString();
							CategoryModel categoryModel = new CategoryModel();
							categoryModel.setCategoryId(Integer.parseInt(category));
							bidProductsModel.setCategoryModel(categoryModel);
						} else if (item.getFieldName().equals("description")) {
							String description = item.getString();
							bidProductsModel.setDescription(description);
						} else if (item.getFieldName().equals("minprice")) {
							Double minPrice = Double.parseDouble(item.getString());
							bidProductsModel.setMinPrice(minPrice);
						} else if (item.getFieldName().equals("endingdate")) {
							String endingDate = item.getString();
							bidProductsModel.setStartingDate(endingDate);
						} else if (item.getFieldName().equals("contact")) {
							String contact = item.getString();
							bidProductsModel.setContact(contact);
						}

					}

				}

				// File uploaded successfully
				PostedBidDaoImpl bidDaoImpl = new PostedBidDaoImpl();
				bidDaoImpl.submitAdPost(bidProductsModel);
				PrintWriter printWriter = response.getWriter();
				printWriter.print("success");

			} catch (Exception ex) {

				request.setAttribute("message", "File Upload Failed due to " + ex);

			}

		} else {

			request.setAttribute("message",

					"Sorry this Servlet only handles file upload request");

		}
	}

}
