package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NoInitialContextException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.NotificationDTO;
import com.dtotransform.NotificationDtoTransform;
import com.google.gson.Gson;
import com.models.BiddingNotifications;

import daoImpl.NotificationDaoImpl;

/**
 * Servlet implementation class NotificationServlet
 */
@WebServlet("/NotificationServlet")
public class NotificationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NotificationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	if(request.getParameter("method").equals("getNotifications")){
		response.setContentType("application/json");
		NotificationDaoImpl notificationDaoImpl = new NotificationDaoImpl();
		List<BiddingNotifications> notifications = notificationDaoImpl.getAllNotifications();
		List<NotificationDTO> notificationDTOs = new ArrayList<>();
		for(int i=0;i<notifications.size();i++){
		NotificationDTO notificationDTO = NotificationDtoTransform.transformNotificationDto(notifications.get(i));	
			notificationDTOs.add(notificationDTO);
		}
		
		Gson gson = new Gson();
		System.out.println(gson.toJson(notificationDTOs));
		PrintWriter printWriter = response.getWriter();
		printWriter.print(gson.toJson(notificationDTOs));
		
	}
	
	}

}
