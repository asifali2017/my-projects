package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.MessageDTO;
import com.dto.UserMessagesDTO;
import com.dtotransform.MessageDtoTransform;
import com.dtotransform.UserMessagesDtoTransform;
import com.google.gson.Gson;
import com.models.MessagesModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;

import dao.MessagesDao;
import daoImpl.MessagesDaoImpl;

/**
 * Servlet implementation class MessageServlet
 */
@WebServlet("/MessageServlet")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     
		 
		
		if(request.getParameter("method")!=null){
		    if(request.getParameter("method").equals("getMessages")){
		    response.setContentType("application/json");	
		    MessagesDaoImpl messagesDaoImpl = new MessagesDaoImpl();
		    Integer userid = Integer.parseInt(request.getParameter("userid")); 
		    Integer buyerid = Integer.parseInt(request.getParameter("buyerid"));
		    MessagesModel messagesModel = new MessagesModel();
            SellerBuyerModel sender = new SellerBuyerModel();
            sender.setId(userid);
            SellerBuyerModel reciver = new SellerBuyerModel();
            reciver.setId(buyerid);
            messagesModel.setSellerBuyerModel(sender);
            messagesModel.setSellerBuyerModels(reciver);
		    List<MessagesModel>  messages = messagesDaoImpl.getMessages(messagesModel);
		    List<MessageDTO> messageDTOs = new ArrayList<>();
		     for(int i=0;i<messages.size();i++){
		    	 MessageDTO messageDTO = MessageDtoTransform.transformMessageDTO(messages.get(i));
		    	 messageDTOs.add(messageDTO);
		     }
		     Gson gson = new Gson();
		     System.out.println(gson.toJson(messageDTOs));
		     PrintWriter printWriter = response.getWriter();
		     printWriter.print(gson.toJson(messageDTOs));
		     
		    }
			if(request.getParameter("method").equals("sendmessage")){ 
			String decryptedId  = getDecryptedId(request.getParameter("productId"));
			int postBidProductId = Integer.parseInt(decryptedId);
			int userId = Integer.parseInt(request.getParameter("userId"));
			int buyerId = Integer.parseInt(request.getParameter("buyerId"));
			String message = request.getParameter("message");
			MessagesModel messagesModel = new MessagesModel();
			PostedBidProductsModel postedBidProductsModel = new PostedBidProductsModel();
			postedBidProductsModel.setId(postBidProductId);
			SellerBuyerModel sendTo = new SellerBuyerModel();
			sendTo.setId(buyerId);
			SellerBuyerModel sendBy = new SellerBuyerModel();
			sendBy.setId(userId);
			messagesModel.setBidProductsModel(postedBidProductsModel);
			messagesModel.setSellerBuyerModel(sendBy);
			messagesModel.setSellerBuyerModels(sendTo);
			messagesModel.setMessage(message);
			MessagesDaoImpl messageDaoImpl = new MessagesDaoImpl();
			int i = messageDaoImpl.sendMessage(messagesModel);
			if(i>0){
				PrintWriter printWriter = response.getWriter();
				printWriter.write("success");
			}else{
				PrintWriter printWriter = response.getWriter();
				printWriter.write("unsuccess");
			}
		 }
		if(request.getParameter("method").equals("userMessages")){
			  response.setContentType("application/json");
				int id = Integer.parseInt(request.getParameter("userid"));
				MessagesDaoImpl messagesDaoImpl = new MessagesDaoImpl();
				List<MessagesModel> userMessages = messagesDaoImpl.getAllUserMessages(id);
				List<UserMessagesDTO> userMessagesDTOs = new ArrayList<UserMessagesDTO>();
				for(int i=0;i<userMessages.size();i++){
					UserMessagesDTO userDTO = UserMessagesDtoTransform.transformUserMessages(userMessages.get(i));
					userMessagesDTOs.add(userDTO);
				}
				Gson gson = new Gson();
				System.out.println(gson.toJson(userMessagesDTOs));
				PrintWriter printWriter = response.getWriter();
				printWriter.print(gson.toJson(userMessagesDTOs));
			}
		}    	  
	
	}

	public String getDecryptedId(String id) {

		Base64.Decoder decoder = Base64.getMimeDecoder();
		String dStr = new String(decoder.decode(id));
		return dStr;
	}

}
