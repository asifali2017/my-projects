package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import com.models.SellerBuyerModel;
import com.sun.glass.ui.Application;

import dao.DBConfiguration;
import daoImpl.LoginDaoImpl;
import utils.PasswordEncryption;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public static int count = 0;
	public static int id = 0;
	public static String name = "";
	
	Cookie checkCookie = new Cookie("name", "block"); // new Cookie("name",
														// "block");

	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter printWriter = response.getWriter();
		String cookieName = "";
		Cookie[] user = request.getCookies();
		for (Cookie cookie : user) {
			if (cookie.getName().equals("name")) {

				cookieName = cookie.getName();
			}

		}

		if (request.getParameter("method") != null) {

			if (request.getParameter("method").equals("login")) { // main if
																	// start

				response.setContentType("text/html");
				String pass = PasswordEncryption.passwordEncryption(request.getParameter("password"));
				String email = request.getParameter("email");
				name = "";
				 id = 0;
				LoginDaoImpl daoImpl = new LoginDaoImpl();
				List<SellerBuyerModel> list = daoImpl.login(email, pass);
				for (SellerBuyerModel buyerModel : list) {
					name = buyerModel.getName();
					id = buyerModel.getId();
				}
				if (list.isEmpty()) {
					printWriter.print("unsuccess");

				} else {
					HttpSession httpSession = request.getSession();
					httpSession.setAttribute("name", name);
					httpSession.setAttribute("id", id);
					printWriter.print("success");
				}

			} // main if close
			else if (request.getParameter("method").equals("signup")) {

				String name = request.getParameter("name");
				String email = request.getParameter("email");
				String pass = PasswordEncryption.passwordEncryption(request.getParameter("pass"));
				String contact = request.getParameter("contact");
				String address = request.getParameter("address");
				Integer question = Integer.parseInt(request.getParameter("question"));
				String answer = request.getParameter("answer");
				if (name != "" && email != "" && pass != "" && contact != "" && address != "") {
					LoginDaoImpl daoImpl = new LoginDaoImpl();
					if (daoImpl.checkEmail(email) == false) {
						SellerBuyerModel sellerBuyerModel = new SellerBuyerModel();
						sellerBuyerModel.setName(name);
						sellerBuyerModel.setEmail(email);
						sellerBuyerModel.setPassword(pass);
						sellerBuyerModel.setAddress(address);
						sellerBuyerModel.setContact(contact);
						sellerBuyerModel.setQuestions(question);
						sellerBuyerModel.setAnswers(answer);
						daoImpl.signUp(sellerBuyerModel);
						printWriter.write("success");

					} else {

						printWriter.write("exists");
					}
				} else {
					printWriter.write("unsuccess");

				}

			} else if (request.getParameter("method").equals("verifyEmail")) {
				if (cookieName.equals("")) {
					String email = request.getParameter("email");
					LoginDaoImpl daoImpl = new LoginDaoImpl();
					boolean check = daoImpl.checkEmail(email);
					if (check) {
						Cookie cookie = new Cookie("email", email);
						response.addCookie(cookie);
						printWriter.write("success");
						count = 0;
					} else {
						count++;
						if (count == 3) {
							checkCookie.setMaxAge(60);
							response.addCookie(checkCookie);
							count = 0;
						}
						printWriter.write("unsuccess");
					}
				} else {
					printWriter.print("ban");
					count = 0;
				}

			} else if (request.getParameter("method").equals("checkAnswers")) {
				if (cookieName.equals("")) {
					Cookie[] cookies = request.getCookies();
					String email = "";
					for (Cookie cookie : cookies) {
						if (cookie.getName().equals("email")) {
							email = cookie.getValue();
						}
					}
					String answer = request.getParameter("answer");
					Integer question = Integer.parseInt(request.getParameter("question"));
					SellerBuyerModel sellerbuyerModel = new SellerBuyerModel();
					sellerbuyerModel.setEmail(email);
					sellerbuyerModel.setQuestions(question);
					sellerbuyerModel.setAnswers(answer);
					LoginDaoImpl loginDaoImpl = new LoginDaoImpl();
					Boolean check = loginDaoImpl.verifyAnswers(sellerbuyerModel);
					if (check) {
						printWriter.write("success");
						count = 0;
					} else {
						System.out.println(count);
						count++;
						if (count == 3) {
							checkCookie.setMaxAge(60);
							response.addCookie(checkCookie);
							count = 0;
						}
						printWriter.write("unsuccess");
					}

				} else {

					printWriter.write("ban");
					count = 0;
				}

			} else if (request.getParameter("method").equals("updatePassword")) {
				String password = request.getParameter("password");
				Cookie[] cookies = request.getCookies();
				String email = "";
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals("email")) {
						email = cookie.getValue();

					}
				}
				SellerBuyerModel sellerBuyerModel = new SellerBuyerModel();
				sellerBuyerModel.setPassword(PasswordEncryption.passwordEncryption(password));
				sellerBuyerModel.setEmail(email);
				LoginDaoImpl loginDaoImpl = new LoginDaoImpl();
				int row = loginDaoImpl.updatePassword(sellerBuyerModel);
				if (row > 0) {
					printWriter.write("success");
				} else {
					printWriter.write("unsuccess");
				}

			}

		}

	}

}
