package com.servlets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Hibernate;

import daoImpl.CategoryDaoImpl;
import daoImpl.PostedBidDaoImpl;

import com.dto.CategoryDTO;
import com.dto.PostedBidProductsDTO;
import com.dtotransform.CategoryDtoTransform;
import com.dtotransform.PostedBidProductsDtoTransform;
import com.google.gson.Gson;
import com.models.CategoryModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;
import com.models.ImagesModel;

@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HomeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("method") != null) {
			System.out.println("Hello");
			PrintWriter printWriter = response.getWriter();
			if (request.getParameter("method").equals("fillCategoryCombo")) {

				response.setContentType("application/json");
				CategoryDaoImpl categoryDaoImpl = new CategoryDaoImpl();
				List<CategoryModel> categories = categoryDaoImpl.getAllCategories();
				List<CategoryDTO> categoryDTOs = new ArrayList<>();
				for (int i = 0; i < categories.size(); i++) {
					CategoryDTO categoryDTO = CategoryDtoTransform.tansformCategoryDto(categories.get(i));
					categoryDTOs.add(categoryDTO);
				}
				Gson gson = new Gson();
				System.out.println(gson.toJson(categoryDTOs));
				printWriter.write(gson.toJson(categoryDTOs));
			} else if (request.getParameter("method").equals("getProductData")) {
				response.setContentType("application/json");
				PostedBidDaoImpl postedBidDaoImpl = new PostedBidDaoImpl();
				List<PostedBidProductsModel> products = postedBidDaoImpl.getPostedProducts();
				List<PostedBidProductsDTO> postedBidProductsDTOs = new ArrayList();
				for (int i = 0; i < products.size(); i++) {

					PostedBidProductsDTO postedBidProductsDTO = PostedBidProductsDtoTransform
							.transformPostedBidProducts(products.get(i));
					postedBidProductsDTOs.add(postedBidProductsDTO);

				}
				Hibernate.initialize(postedBidProductsDTOs);
				Gson gson = new Gson();
				System.out.println(gson.toJson(postedBidProductsDTOs));
				printWriter.write(gson.toJson(postedBidProductsDTOs));

			}

		}

	}
}
