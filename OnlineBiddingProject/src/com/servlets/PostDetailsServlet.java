package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dto.BidProductsDTO;
import com.dto.PostedBidProductsDTO;
import com.dtotransform.BidProductsDtoTransform;
import com.dtotransform.PostedBidProductsDtoTransform;
import com.google.gson.Gson;
import com.models.BidProducts;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;

import daoImpl.BidProductsDaoImpl;
import daoImpl.PostedBidDaoImpl;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Servlet implementation class PostDetailsServlet
 */
@WebServlet("/PostDetailsServlet")
public class PostDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PostDetailsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("method") != null) {
			if (request.getParameter("method").equals("getBidsData")) {
				response.setContentType("application/json");
				BidProductsDaoImpl bidProductsDaoImpl = new BidProductsDaoImpl();
				String decripted_id = getDecryptedId(request.getParameter("id"));
				List<BidProducts> bidProducts = bidProductsDaoImpl.getBidDetails(Integer.parseInt(decripted_id));
				List<BidProductsDTO> bidProductsDTOs = new ArrayList<BidProductsDTO>();

				for (int i = 0; i < bidProducts.size(); i++) {

					BidProductsDTO productsDTO = BidProductsDtoTransform.transform(bidProducts.get(i));
					bidProductsDTOs.add(productsDTO);

				}
				PrintWriter printWriter = response.getWriter();
				Gson gson = new Gson();
				System.out.println(gson.toJson(bidProductsDTOs));
				printWriter.print(gson.toJson(bidProductsDTOs));

			}

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("method") != null) {
			if (request.getParameter("method").equals("getPostBidProducts")) {
				response.setContentType("application/json");

				String decripted_id = getDecryptedId(request.getParameter("id"));
				// System.out.println(decripted_id);
				// Integer id = Integer.parseInt(request.getParameter("id"));

				PostedBidDaoImpl postedBidDaoImpl = new PostedBidDaoImpl();
				PostedBidProductsModel postedBidProductsModel = postedBidDaoImpl
						.getPostedBidProductsModel(Integer.parseInt(decripted_id));

				PostedBidProductsDTO postedBidProductsDTO = PostedBidProductsDtoTransform
						.transformPostedBidProducts(postedBidProductsModel);

				Gson gson = new Gson();
				PrintWriter printWriter = response.getWriter();

				printWriter.write(gson.toJson(postedBidProductsDTO));

			} else if (request.getParameter("method").equals("addBid")) {
				response.setContentType("application/json");
				String decripted_id = getDecryptedId(request.getParameter("id"));
				int userId = Integer.parseInt(request.getParameter("userId"));
				int bid = Integer.parseInt(request.getParameter("bid"));
				BidProducts bidProducts = new BidProducts();
				PostedBidProductsModel bidProductsModel = new PostedBidProductsModel();
				bidProductsModel.setId(Integer.parseInt(decripted_id));
				bidProducts.setBidProductsModel(bidProductsModel);
				SellerBuyerModel sellerBuyerModel = new SellerBuyerModel();
				sellerBuyerModel.setId(userId);
				bidProducts.setSellerBuyerModel(sellerBuyerModel);
				bidProducts.setBid(bid);
				BidProductsDaoImpl bidProductsDaoImpl = new BidProductsDaoImpl();
				int row = bidProductsDaoImpl.postBid(bidProducts);
				PrintWriter printWriter = response.getWriter();
				if (row > 0) {
					printWriter.print("success");
				} else {
					printWriter.print("unsuccess");
				}

			}

		}

	}

	public String getDecryptedId(String id) {

		Base64.Decoder decoder = Base64.getMimeDecoder();
		String dStr = new String(decoder.decode(id));
		System.out.println("Decoded message: " + dStr);

		return dStr;
	}

}
