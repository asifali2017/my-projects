package com.dto;


public class MessageDTO {

	private String id;
	private String messageArrivalTime;
	private String message;
	private String status;
	private String postedBidProductName;
	private String sender;
	private String receiver;
	public  String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMessageArrivalTime() {
		return messageArrivalTime;
	}
	public void setMessageArrivalTime(String messageArrivalTime) {
		this.messageArrivalTime = messageArrivalTime;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPostedBidProductName() {
		return postedBidProductName;
	}
	public void setPostedBidProductName(String postedBidProductName) {
		this.postedBidProductName = postedBidProductName;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	
	
	
	
	
}
