package com.dto;

import java.util.Set;

public class PostedBidProductsDTO {
    private String id;
    private String sellerBuyerId;
    private String name;
    private String contact;
    private String description;
    private String endingDate;
    private String minPrice;
    private Set<String> images;
    private String sellerBuyerName;
    private String category;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEndingDate() {
		return endingDate;
	}
	public void setEndingDate(String endingDate) {
		this.endingDate = endingDate;
	}
	public String getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	public Set<String> getImages() {
		return images;
	}
	public void setImages(Set<String> images) {
		this.images = images;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSellerBuyerName() {
		return sellerBuyerName;
	}
	public void setSellerBuyerName(String sellerBuyerName) {
		this.sellerBuyerName = sellerBuyerName;
	}
	public String getSellerBuyerId() {
		return sellerBuyerId;
	}
	public void setSellerBuyerId(String sellerBuyerId) {
		this.sellerBuyerId = sellerBuyerId;
	}
	 
	
	
	
}
