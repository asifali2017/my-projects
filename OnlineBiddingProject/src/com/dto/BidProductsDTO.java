package com.dto;

public class BidProductsDTO {

	private String bid;
	private String productName;
	private String sellerBuyerName;
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSellerBuyerName() {
		return sellerBuyerName;
	}
	public void setSellerBuyerName(String sellerBuyerName) {
		this.sellerBuyerName = sellerBuyerName;
	}
	
	
	
}
