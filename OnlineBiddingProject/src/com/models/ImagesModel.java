package com.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="images")
public class ImagesModel {
	
	@Id
	@GeneratedValue
	@Column(name="image_id")
	private Integer imageId;
	@ManyToOne
	@JoinColumn(name="pbp_id")
	private PostedBidProductsModel bidProductsModel;
	@Column(name="image_path")
	private String path;
	public Integer getId() {
		return imageId;
	}
	public void setId(Integer id) {
		imageId = id;
	}
	public PostedBidProductsModel getBidProductsModel() {
		return bidProductsModel;
	}
	public void setBidProductsModel(PostedBidProductsModel bidProductsModel) {
		this.bidProductsModel = bidProductsModel;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
	
	
	
}
