package com.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="bidding_notifications")
public class BiddingNotifications {
    @GeneratedValue
	@Id
	@Column(name="bid_notification_id")
	private Integer bidNotificationId;
	@Column(name="status")
	private Integer status;
	@ManyToOne
	@JoinColumn(name="notification_id")
	private NotificationModel notifications;
	@ManyToOne
	@JoinColumn(name="seller_buyer_id")
	private SellerBuyerModel sellerBuyerModel;
	@Column(name="notify")
	private Integer notify;
	public Integer getBidNotificationId() {
		return bidNotificationId;
	}
	public void setBidNotificationId(Integer bidNotificationId) {
		this.bidNotificationId = bidNotificationId;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public NotificationModel getNotifications() {
		return notifications;
	}
	public void setNotifications(NotificationModel notifications) {
		this.notifications = notifications;
	}
	public SellerBuyerModel getSellerBuyerModel() {
		return sellerBuyerModel;
	}
	public void setSellerBuyerModel(SellerBuyerModel sellerBuyerModel) {
		this.sellerBuyerModel = sellerBuyerModel;
	}
	public Integer getNotify() {
		return notify;
	}
	public void setNotify(Integer notify) {
		this.notify = notify;
	}
	

}
