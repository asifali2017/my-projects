package com.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="notifications")
public class NotificationModel {

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer id;
	@Column(name="message")
	private String message;
	@Column(name="time")
	private String time;
	@ManyToOne
	@JoinColumn(name="p_bid")
	private PostedBidProductsModel bidProductsModel;
	@ManyToOne
	@JoinColumn(name="s_id")
	private SellerBuyerModel sellerBuyerModel;
	@JoinColumn(name="n_id")
	@OneToMany(cascade=CascadeType.ALL)
	private Set<BidProducts> bidProducts = new HashSet<>();
	@JoinColumn(name="notification_id")
	@OneToMany(cascade=CascadeType.ALL)
	private Set<BiddingNotifications> biddingNotifications = new HashSet<>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public PostedBidProductsModel getBidProductsModel() {
		return bidProductsModel;
	}
	public void setBidProductsModel(PostedBidProductsModel bidProductsModel) {
		this.bidProductsModel = bidProductsModel;
	}
	public SellerBuyerModel getSellerBuyerModel() {
		return sellerBuyerModel;
	}
	public void setSellerBuyerModel(SellerBuyerModel sellerBuyerModel) {
		this.sellerBuyerModel = sellerBuyerModel;
	}
	public Set<BidProducts> getBidProducts() {
		return bidProducts;
	}
	public void setBidProducts(Set<BidProducts> bidProducts) {
		this.bidProducts = bidProducts;
	}
	public Set<BiddingNotifications> getBiddingNotifications() {
		return biddingNotifications;
	}
	public void setBiddingNotifications(Set<BiddingNotifications> biddingNotifications) {
		this.biddingNotifications = biddingNotifications;
	}
	
	
	
}
