package com.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
@Entity
@Table(name="seller_buyer_account")
public class SellerBuyerModel {
    
	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer Id;
	@Column(name="name")
	private String name;
	@Column(name="email")
	private String email;
	@Column(name="password")
	private String password;
	@Column(name="utility")
	private String utility;
	@Column(name="notifications")
	private String notificatios;
	@Column(name="contact")
	private String contact;
	@Column(name="address")
	private String address;
	@Column(name="questions")
	private Integer questions;
	@Column(name="answers")
	private String answers;
	@OneToMany(targetEntity=SellProducts.class,mappedBy="sellerBuyerModel",cascade=CascadeType.ALL/*,fetch=FetchType.EAGER*/)
	private Set<SellProducts> products = new HashSet<SellProducts>();
	@OneToMany(targetEntity=PostedBidProductsModel.class,mappedBy="sellerBuyerModel",cascade=CascadeType.ALL/*,fetch=FetchType.EAGER*/)
	private Set<PostedBidProductsModel> bidProductsModels = new HashSet<PostedBidProductsModel>();
	@OneToMany(targetEntity=MessagesModel.class,mappedBy="sellerBuyerModel",cascade=CascadeType.ALL)
	private Set<MessagesModel> sendByMessages = new HashSet<MessagesModel>();
	@OneToMany(targetEntity=MessagesModel.class,mappedBy="sellerBuyerModels",cascade=CascadeType.ALL)
	private Set<MessagesModel> sentToMessages = new HashSet<MessagesModel>();
	@OneToMany(targetEntity=NotificationModel.class,mappedBy="sellerBuyerModel",cascade=CascadeType.ALL)
	private Set<NotificationModel> notifications = new HashSet<NotificationModel>();
	@JoinColumn(name="seller_buyer_id")
	@OneToMany(cascade=CascadeType.ALL)
	private Set<BiddingNotifications> biddingNotifications = new HashSet<>();
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUtility() {
		return utility;
	}
	public void setUtility(String utility) {
		this.utility = utility;
	}
	public String getNotificatios() {
		return notificatios;
	}
	public void setNotificatios(String notificatios) {
		this.notificatios = notificatios;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Set<SellProducts> getProducts() {
		return products;
	}
	public void setProducts(Set<SellProducts> products) {
		this.products = products;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getAnswers() {
		return answers;
	}
	public void setAnswers(String answers) {
		this.answers = answers;
	}
	public Set<PostedBidProductsModel> getBidProductsModels() {
		return bidProductsModels;
	}
	public void setBidProductsModels(Set<PostedBidProductsModel> bidProductsModels) {
		this.bidProductsModels = bidProductsModels;
	}
	public Integer getQuestions() {
		return questions;
	}
	public void setQuestions(Integer questions) {
		this.questions = questions;
	}
	public Set<NotificationModel> getNotifications() {
		return notifications;
	}
	public void setNotifications(Set<NotificationModel> notifications) {
		this.notifications = notifications;
	}
	public Set<BiddingNotifications> getBiddingNotifications() {
		return biddingNotifications;
	}
	public void setBiddingNotifications(Set<BiddingNotifications> biddingNotifications) {
		this.biddingNotifications = biddingNotifications;
	}
	
	
	
}
