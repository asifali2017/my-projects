package com.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class CategoryModel {
	
	@Id
	@GeneratedValue
	@Column(name="category_id")
	private Integer categoryId;
	
	@Column(name="category_name")
	private String categoryName;
	@OneToMany(mappedBy="categoryModel",cascade=CascadeType.ALL)
	private Set<PostedBidProductsModel> bidProductsModels = new HashSet<>() ;
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Set<PostedBidProductsModel> getBidProductsModels() {
		return bidProductsModels;
	}
	public void setBidProductsModels(Set<PostedBidProductsModel> bidProductsModels) {
		this.bidProductsModels = bidProductsModels;
	}
	
}
