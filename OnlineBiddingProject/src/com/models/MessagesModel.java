package com.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
@Table(name="message")
public class MessagesModel {

	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer Id;
	
	@Column(name="message")
	private String message;
	
	@Column(name="message_arrival_time")
	private String messageArivalTime;
	
	@Column(name="status")
	private Integer status;
	
	@ManyToOne
	@JoinColumn(name="posted_bid_product_id")
	private PostedBidProductsModel bidProductsModel;
	
	@ManyToOne
	@JoinColumn(name="send_by")
	private SellerBuyerModel sellerBuyerModel;
	
	@ManyToOne
	@JoinColumn(name="send_to")
	private SellerBuyerModel sellerBuyerModels;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getMessageArivalTime() {
		return messageArivalTime;
	}

	public void setMessageArivalTime(String messageArivalTime) {
		this.messageArivalTime = messageArivalTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public PostedBidProductsModel getBidProductsModel() {
		return bidProductsModel;
	}

	public void setBidProductsModel(PostedBidProductsModel bidProductsModel) {
		this.bidProductsModel = bidProductsModel;
	}

	public SellerBuyerModel getSellerBuyerModel() {
		return sellerBuyerModel;
	}

	public void setSellerBuyerModel(SellerBuyerModel sellerBuyerModel) {
		this.sellerBuyerModel = sellerBuyerModel;
	}

	public SellerBuyerModel getSellerBuyerModels() {
		return sellerBuyerModels;
	}

	public void setSellerBuyerModels(SellerBuyerModel sellerBuyerModels) {
		this.sellerBuyerModels = sellerBuyerModels;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
