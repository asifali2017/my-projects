package com.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
@Table(name="sell_products")
public class SellProducts {

	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer id;
	@ManyToOne
	@JoinColumn(name="sb_id")
	private SellerBuyerModel sellerBuyerModel;
	@Column(name="final_price")
	private Double finalPrice;
	@ManyToOne
	@JoinColumn(name="pbp_id")
	private PostedBidProductsModel bidProductsModel;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public SellerBuyerModel getSellerBuyerModel() {
		return sellerBuyerModel;
	}
	public void setSellerBuyerModel(SellerBuyerModel sellerBuyerModel) {
		this.sellerBuyerModel = sellerBuyerModel;
	}
	public Double getFinalPrice() {
		return finalPrice;
	}
	public void setFinalPrice(Double finalPrice) {
		this.finalPrice = finalPrice;
	}
	/*public PostedBidProductsModel getBidProductsModel() {
		return bidProductsModel;
	}
	public void setBidProductsModel(PostedBidProductsModel bidProductsModel) {
		this.bidProductsModel = bidProductsModel;
	}
	*/
	
	
}
