package com.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="bid_products")
public class BidProducts {
   
	@Id
	@GeneratedValue
	@Column(name="Id")
	private Integer Id;
	@ManyToOne
	@JoinColumn(name="pbp_id")
	private PostedBidProductsModel bidProductsModel;
	@ManyToOne
	@JoinColumn(name="sb_id")
	private SellerBuyerModel sellerBuyerModel;
	@ManyToOne
	@JoinColumn(name="n_id")
    private NotificationModel notifications; 
	@Column(name="bid")
	private Integer bid;
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public PostedBidProductsModel getBidProductsModel() {
		return bidProductsModel;
	}
	public void setBidProductsModel(PostedBidProductsModel bidProductsModel) {
		this.bidProductsModel = bidProductsModel;
	}
	public SellerBuyerModel getSellerBuyerModel() {
		return sellerBuyerModel;
	}
	public void setSellerBuyerModel(SellerBuyerModel sellerBuyerModel) {
		this.sellerBuyerModel = sellerBuyerModel;
	}
	public Integer getBid() {
		return bid;
	}
	public void setBid(Integer bid) {
		this.bid = bid;
	}
	public NotificationModel getNotifications() {
		return notifications;
	}
	public void setNotifications(NotificationModel notifications) {
		this.notifications = notifications;
	}
	
	
	
}
