
package com.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Table(name="posted_bid_products")
public class PostedBidProductsModel {
   
	@Id
	@GeneratedValue
	@Column(name="id")
	private Integer Id;
    @ManyToOne
    @JoinColumn(name="c_id")
	private CategoryModel categoryModel;
	@Column(name="name")
	private String name;
	@Column(name="min_price")
	private Double minPrice;
	@ManyToOne
	@JoinColumn(name="sb_id")
	private SellerBuyerModel sellerBuyerModel;
	@Column(name="starting_date")
	private String startingDate;
	@Column(name="description")
	private String description;
	@Column (name="contact")
	private String contact;
	@OneToMany(targetEntity=SellProducts.class,mappedBy="bidProductsModel",cascade=CascadeType.ALL)
	private Set<SellProducts> products = new HashSet<SellProducts>();
	//@OneToMany(mappedBy="bidProductsModel",cascade=CascadeType.ALL)
	@JoinColumn(name="pbp_id")
	@OneToMany(cascade=CascadeType.ALL)
	private Set<ImagesModel> imagesModels = new HashSet<ImagesModel>();
	@OneToMany(targetEntity=BidProducts.class,mappedBy="bidProductsModel",cascade=CascadeType.ALL)
	private Set<PostedBidProductsModel> bidProductsModels = new HashSet<>();
	@OneToMany(targetEntity=BidProducts.class,mappedBy="sellerBuyerModel",cascade=CascadeType.ALL)
	private Set<SellerBuyerModel> buyerModels = new HashSet();
	@OneToMany(targetEntity=MessagesModel.class,mappedBy="bidProductsModel",cascade=CascadeType.ALL)
	private Set<MessagesModel> messagesModels = new HashSet<MessagesModel>();
	@OneToMany(targetEntity=NotificationModel.class,mappedBy="bidProductsModel",cascade=CascadeType.ALL)
	private Set<NotificationModel> notifications = new HashSet<NotificationModel>();
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public CategoryModel getCategoryModel() {
		return categoryModel;
	}
	public void setCategoryModel(CategoryModel categoryModel) {
		this.categoryModel = categoryModel;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}
	public SellerBuyerModel getSellerBuyerModel() {
		return sellerBuyerModel;
	}
	public void setSellerBuyerModel(SellerBuyerModel sellerBuyerModel) {
		this.sellerBuyerModel = sellerBuyerModel;
	}
	public String getStartingDate() {
		return startingDate;
	}
	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	/*public Set<SellProducts> getProducts() {
		return products;
	}
	public void setProducts(Set<SellProducts> products) {
		this.products = products;
	}*/
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Set<ImagesModel> getImagesModels() {
		return imagesModels;
	}
	public void setImagesModels(Set<ImagesModel> imagesModels) {
		this.imagesModels = imagesModels;
	}
	/*public Set<PostedBidProductsModel> getBidProductsModels() {
		return bidProductsModels;
	}
	public void setBidProductsModels(Set<PostedBidProductsModel> bidProductsModels) {
		this.bidProductsModels = bidProductsModels;
	}*/
	/*public Set<SellerBuyerModel> getBuyerModels() {
		return buyerModels;
	}
	public void setBuyerModels(Set<SellerBuyerModel> buyerModels) {
		this.buyerModels = buyerModels;
	}*/
	
	
}
