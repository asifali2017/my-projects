package com.dtotransform;

import com.dto.NotificationDTO;
import com.models.BiddingNotifications;

public class NotificationDtoTransform {

	public static NotificationDTO transformNotificationDto(BiddingNotifications biddingNotifications){
	
	NotificationDTO notificationDTO = new  NotificationDTO();	
	if(biddingNotifications.getSellerBuyerModel().getId()!=null){
		notificationDTO.setSellerId(biddingNotifications.getSellerBuyerModel().getId().toString());
	}
	if(biddingNotifications.getNotifications().getMessage()!=null){
		notificationDTO.setNotificationMessage(biddingNotifications.getNotifications().getMessage());
	}
	if(biddingNotifications.getNotify()!=null){
		notificationDTO.setNotify(String.valueOf(biddingNotifications.getNotify()));
	}
	if(biddingNotifications.getStatus()!=null){
		notificationDTO.setStatus(String.valueOf(biddingNotifications.getStatus()));
	}
	return notificationDTO;
	}
}	


