package com.dtotransform;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.tomcat.util.codec.binary.Base64;

import com.dto.PostedBidProductsDTO;
import com.models.ImagesModel;
import com.models.PostedBidProductsModel;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

public class PostedBidProductsDtoTransform {

	public static PostedBidProductsDTO transformPostedBidProducts(PostedBidProductsModel postedBidProductsModel){
		PostedBidProductsDTO postedBidProductsDTO = new PostedBidProductsDTO();
		if(postedBidProductsModel.getId()!=null){
			  String id = String.valueOf(postedBidProductsModel.getId());
			  byte[]   bytesEncoded = Base64.encodeBase64(id.getBytes());//encoding part
			  String encoded_id=new String(bytesEncoded);

			postedBidProductsDTO.setId(encoded_id);
		}
		if(postedBidProductsModel.getSellerBuyerModel().getId()!=null){
			postedBidProductsDTO.setSellerBuyerId(String.valueOf(postedBidProductsModel.getSellerBuyerModel().getId()));
		}
		if(postedBidProductsModel.getName()!=null){
			
			postedBidProductsDTO.setName(postedBidProductsModel.getName());
		}
        if(postedBidProductsModel.getDescription()!=null){
			
			postedBidProductsDTO.setDescription(postedBidProductsModel.getDescription());
		}
        if(postedBidProductsModel.getMinPrice()!=null){
			
			postedBidProductsDTO.setMinPrice(String.valueOf(postedBidProductsModel.getMinPrice()));
		}
        if(postedBidProductsModel.getStartingDate()!=null){
			
			postedBidProductsDTO.setEndingDate(postedBidProductsModel.getStartingDate());
		}
        if(postedBidProductsModel.getCategoryModel().getCategoryName()!=null){
			
			postedBidProductsDTO.setCategory(postedBidProductsModel.getCategoryModel().getCategoryName());
		}
       if(postedBidProductsModel.getSellerBuyerModel().getName()!=null){
			
			postedBidProductsDTO.setSellerBuyerName(postedBidProductsModel.getSellerBuyerModel().getName());
		}
       if(postedBidProductsModel.getContact()!=null){
    	   postedBidProductsDTO.setContact(postedBidProductsModel.getContact());
       }
	   if(postedBidProductsModel.getImagesModels()!=null){
		   
		   
		   Set<ImagesModel> imagesPath = postedBidProductsModel.getImagesModels() ; 
		   Set<String> images = new HashSet();
		   for(ImagesModel path:imagesPath){
			   
			   images.add(path.getPath());
			   
		   }
		   postedBidProductsDTO.setImages(images);
		   
	   }
		
		
		return postedBidProductsDTO;
		
	}
	
	
	
}
