package com.dtotransform;

import org.apache.tomcat.util.codec.binary.Base64;

import com.dto.UserMessagesDTO;
import com.models.MessagesModel;

public class UserMessagesDtoTransform {

	public static UserMessagesDTO transformUserMessages(MessagesModel messagesModel){
		UserMessagesDTO userMessagesDTO = new UserMessagesDTO();
		if(messagesModel.getSellerBuyerModel().getId()!=null){
			userMessagesDTO.setId(String.valueOf(messagesModel.getSellerBuyerModel().getId()));
		}
		if(messagesModel.getSellerBuyerModel().getName()!=null){
			userMessagesDTO.setName(messagesModel.getSellerBuyerModel().getName());
		}
		if(messagesModel.getMessageArivalTime()!=null){
			userMessagesDTO.setArrivalTime(messagesModel.getMessageArivalTime());
		}
		if(messagesModel.getBidProductsModel().getId()!=null){
			 String id = String.valueOf(messagesModel.getBidProductsModel().getId());
			  byte[]   bytesEncoded = Base64.encodeBase64(id.getBytes());//encoding part
			  String encoded_id=new String(bytesEncoded);
			  userMessagesDTO.setProductId(encoded_id);
		}
		return userMessagesDTO;
	}
	
	
	
}
