package com.dtotransform;

import com.dto.BidProductsDTO;
import com.dto.PostedBidProductsDTO;
import com.models.BidProducts;

public class BidProductsDtoTransform {

	public static BidProductsDTO transform(BidProducts bidProducts){
		
		BidProductsDTO bidProductsDTO = new BidProductsDTO();
		
		if(bidProducts.getBid()!=null){
			
			bidProductsDTO.setBid(String.valueOf(bidProducts.getBid()));
		}
		if(bidProducts.getBidProductsModel().getName()!=null){
		  	
		   bidProductsDTO.setProductName(bidProducts.getBidProductsModel().getName());
		}
		if(bidProducts.getSellerBuyerModel().getName()!=null){
			
			bidProductsDTO.setSellerBuyerName(bidProducts.getSellerBuyerModel().getName());
		}
		
		
		return bidProductsDTO;
		
	}
	
	
	
	
}
