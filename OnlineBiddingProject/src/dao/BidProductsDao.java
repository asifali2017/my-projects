package dao;

import java.util.List;

import com.models.BidProducts;

public interface BidProductsDao {

	public List<BidProducts> getBidDetails(Integer id);
	public int postBid(BidProducts bidProducts);
	
}
