package dao;

import java.util.List;

import com.models.PostedBidProductsModel;

import daoImpl.PostedBidDaoImpl;

public interface PostedBidDao {

	public void submitAdPost(PostedBidProductsModel bidProductsModel);
	public List<PostedBidProductsModel> getPostedProducts();
	public PostedBidProductsModel getPostedBidProductsModel(Integer Id);
}
