package dao;

import java.util.List;

import com.models.MessagesModel;

public interface MessagesDao {

	public int sendMessage(MessagesModel messagesModel);
	public List<MessagesModel> getMessages(MessagesModel messagesModel);
    public List<MessagesModel> getAllUserMessages(Integer id);	
	
}
