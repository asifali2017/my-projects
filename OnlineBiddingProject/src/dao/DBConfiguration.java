package dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class DBConfiguration {

	private static SessionFactory sessionFactory;
	
	private DBConfiguration(){
		
		
		
	};
	
	public static SessionFactory getSessionFactory(){
		
		if(sessionFactory == null){
		Configuration configuration = new Configuration().configure("hibernate.configuration.xml");
		 sessionFactory = configuration.buildSessionFactory();
		}
		return sessionFactory;
		
		
		
	}
	
	
	
	
	
}
