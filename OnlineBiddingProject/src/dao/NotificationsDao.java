package dao;

import java.util.List;

import com.models.BiddingNotifications;
import com.models.NotificationModel;

public interface NotificationsDao {

	public int addNotifications(NotificationModel notificationModel);
	public int addBidNotifications(NotificationModel notificationModel);
    public List<BiddingNotifications> getAllNotifications();
}
