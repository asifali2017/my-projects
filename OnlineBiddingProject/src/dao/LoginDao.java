package dao;

import java.util.List;

import com.models.SellerBuyerModel;

public interface LoginDao {

	public List login(String email, String pass);
	public void signUp(SellerBuyerModel sellerBuyerModel);
	public boolean checkEmail(String email);
    public boolean verifyAnswers(SellerBuyerModel buyerModel);	
    public int updatePassword(SellerBuyerModel sellerBuyerModel);        
}
