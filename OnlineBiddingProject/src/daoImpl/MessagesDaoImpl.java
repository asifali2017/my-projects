package daoImpl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.models.MessagesModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;
import com.sun.jmx.snmp.Timestamp;

import dao.DBConfiguration;
import dao.MessagesDao;

public class MessagesDaoImpl implements MessagesDao {

	@Override
	public int sendMessage(MessagesModel messagesModel) {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		PostedBidProductsModel postedBidProductsModel = session.get(PostedBidProductsModel.class,
				messagesModel.getBidProductsModel().getId());
		SellerBuyerModel sendBy = session.get(SellerBuyerModel.class, messagesModel.getSellerBuyerModel().getId());
		SellerBuyerModel sendTo = session.get(SellerBuyerModel.class, messagesModel.getSellerBuyerModels().getId());
		MessagesModel messageModel = new MessagesModel();
		messageModel.setBidProductsModel(postedBidProductsModel);
		messageModel.setSellerBuyerModel(sendBy);
		messageModel.setSellerBuyerModels(sendTo);
		String date = new Date().toString();
		messageModel.setMessageArivalTime(date);
		messageModel.setMessage(messagesModel.getMessage());
		messageModel.setStatus(1);

		int i = (int) session.save(messageModel);
		transaction.commit();
		session.close();
		return i;
	}

	public  List<MessagesModel> getMessages(MessagesModel messagesModel) {

		 SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		 Session session = sessionFactory.openSession();
		 Transaction transaction = session.beginTransaction();
		 SellerBuyerModel sender = new SellerBuyerModel();
		 sender.setId(messagesModel.getSellerBuyerModel().getId());
		 SellerBuyerModel receiver = new SellerBuyerModel();
		 receiver.setId(messagesModel.getSellerBuyerModels().getId());
		 Criteria criteria = session.createCriteria(MessagesModel.class);
		 Criterion sendBy     = Restrictions.eq("sellerBuyerModel", sender);
		 Criterion recievedBy = Restrictions.eq("sellerBuyerModels", receiver);
		 Criterion sender2 = Restrictions.eq("sellerBuyerModel", receiver);
		 Criterion  receiver2 = Restrictions.eq("sellerBuyerModels", sender);
		 Criterion finalCondition = Restrictions.disjunction().add(Restrictions.and(sendBy,recievedBy)).add(Restrictions.and(sender2,receiver2));
		 criteria.addOrder(Order.asc("Id"));
		 criteria.add(finalCondition);
		 List<MessagesModel> messages = criteria.list();

        session.close();
 		return messages;
	}

	@Override
	public List<MessagesModel> getAllUserMessages(Integer id) {

		 SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		 Session session = sessionFactory.openSession();
		 Transaction transaction = session.beginTransaction();
		 Criteria criteria = session.createCriteria(MessagesModel.class);
	     SellerBuyerModel sellerBuyerModel = new SellerBuyerModel();
	     sellerBuyerModel.setId(id);
		 criteria.add(Restrictions.eq("sellerBuyerModels", sellerBuyerModel));
		 List<MessagesModel> messages =  criteria.list();
	     session.close();
		 return messages;
	}

}
