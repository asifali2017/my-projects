package daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.models.CategoryModel;

import dao.CategoryDao;
import dao.DBConfiguration;

public class CategoryDaoImpl implements CategoryDao {

	@Override
	public List<CategoryModel> getAllCategories() {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From CategoryModel");
		List<CategoryModel> categories = query.list();
        session.close();
		return categories;
	}
}
