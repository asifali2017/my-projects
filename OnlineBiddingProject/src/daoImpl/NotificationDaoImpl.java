package daoImpl;



import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.models.BidProducts;
import com.models.BiddingNotifications;
import com.models.NotificationModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;
import com.servlets.LoginServlet;

import dao.DBConfiguration;
import dao.NotificationsDao;

public class NotificationDaoImpl implements NotificationsDao{

	@Override
	public int addNotifications(NotificationModel notificationModel) {
		
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		 Criteria criteria = session.createCriteria(BidProducts.class);
		 PostedBidProductsModel bidProductsModel = new PostedBidProductsModel();
		 bidProductsModel.setId(notificationModel.getBidProductsModel().getId());
		 
		 criteria.add(Restrictions.eq("bidProductsModel",bidProductsModel ));
		 
		 List<BidProducts> bidProducts = criteria.list();
		 Set<BiddingNotifications> notifications = new HashSet<>(); 
		 for(BidProducts b:bidProducts){
			 if(b.getSellerBuyerModel().getId()!= LoginServlet.id){
			 BiddingNotifications biddingNotifications = new BiddingNotifications();
			 biddingNotifications.setNotifications(notificationModel);
			 biddingNotifications.setNotify(0);
 			 biddingNotifications.setSellerBuyerModel(b.getSellerBuyerModel());
			 biddingNotifications.setStatus(0);
 		     notifications.add(biddingNotifications);
 			//session.flush(); 
			 }
		 }
		 notificationModel.setBiddingNotifications(notifications);
		
		session.save(notificationModel);
		transaction.commit();
		session.close();
 		
		return 0;
		
	}

	@Override
	public int addBidNotifications(NotificationModel notificationModel) {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
	     Session session = sessionFactory.openSession();
	     Transaction transaction = session.beginTransaction();
		 Criteria criteria = session.createCriteria(BidProducts.class);
		 PostedBidProductsModel bidProductsModel = new PostedBidProductsModel();
		 bidProductsModel.setId(notificationModel.getBidProductsModel().getId());
		 System.out.println(notificationModel.getBidProductsModel().getId());
		 criteria.add(Restrictions.eq("bidProductsModel",bidProductsModel ));
		 BiddingNotifications biddingNotifications = new BiddingNotifications();
		 List<BidProducts> bidProducts = criteria.list();
		 Set<BiddingNotifications> notifications = new HashSet<>(); 
		 for(BidProducts b:bidProducts){
			 
			 biddingNotifications.setNotifications(notificationModel);
			 biddingNotifications.setNotify(0);
 			 biddingNotifications.setSellerBuyerModel(b.getSellerBuyerModel());
			 biddingNotifications.setStatus(0);
 		     notifications.add(biddingNotifications);
 			//session.flush(); 
		 }
		 notificationModel.setBiddingNotifications(notifications);
		 session.save(notificationModel);
	   transaction.commit();
	   session.close();
	   return 0;
	}

	@Override
	public List<BiddingNotifications> getAllNotifications() {
		
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(BiddingNotifications.class);
		List<BiddingNotifications> notifications = criteria.list();
		for(BiddingNotifications biddingNotifications:notifications){
			System.out.println(biddingNotifications.getNotifications().getMessage());
			System.out.println(biddingNotifications.getSellerBuyerModel().getId());
			
		}
		return notifications;
	}

}
