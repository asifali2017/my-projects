package daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import com.models.SellerBuyerModel;

import dao.DBConfiguration;
import dao.LoginDao;

public class LoginDaoImpl implements LoginDao {

	@Override
	public List login(String email, String pass) {
		SessionFactory factory = DBConfiguration.getSessionFactory();
		Session session = factory.openSession();
		Criteria criteria = session.createCriteria(SellerBuyerModel.class);
		Criterion criterion = Restrictions.eq("email", email);
		Criterion criterion2 = Restrictions.eq("password", pass);

		LogicalExpression expression = Restrictions.and(criterion, criterion2);
		criteria.add(expression);
		List results = criteria.list();
        session.close();
		return results;
	}

	@Override
	public void signUp(SellerBuyerModel sellerBuyerModel) {

		SessionFactory factory = DBConfiguration.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		SellerBuyerModel buyerModel = new SellerBuyerModel();
		buyerModel.setEmail(sellerBuyerModel.getEmail());
		buyerModel.setName(sellerBuyerModel.getName());
		buyerModel.setPassword(sellerBuyerModel.getPassword());
		buyerModel.setAddress(sellerBuyerModel.getAddress());
		buyerModel.setContact(sellerBuyerModel.getContact());
		buyerModel.setQuestions(sellerBuyerModel.getQuestions());
		buyerModel.setAnswers(sellerBuyerModel.getAnswers());
		session.save(buyerModel);
		transaction.commit();
		session.close();
	}

	@Override
	public boolean checkEmail(String email) {
		SessionFactory factory = DBConfiguration.getSessionFactory();
		Session session = factory.openSession();
		Criteria criteria = session.createCriteria(SellerBuyerModel.class);
		criteria.add(Restrictions.eq("email", email));
		List result = criteria.list();
		if (result.isEmpty()) {
			session.close();
			return false;

		} else {
			session.close();
			return true;
		}

	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean verifyAnswers(SellerBuyerModel buyerModel) {
		SessionFactory factory = DBConfiguration.getSessionFactory();
		Session session = factory.openSession();
		Criteria criteria = session.createCriteria(SellerBuyerModel.class);
		criteria.add(Restrictions.eq("email", buyerModel.getEmail()));
		criteria.add(Restrictions.eq("questions", buyerModel.getQuestions()));
		criteria.add(Restrictions.eq("answers", buyerModel.getAnswers()));
		List list = criteria.list();

		if (list.isEmpty()) {

			return false;
		} else {

			return true;
		}

	}

	@Override
	public int updatePassword(SellerBuyerModel sellerBuyerModel) {
		SessionFactory factory = DBConfiguration.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("Update SellerBuyerModel set password=:p where email=:e ");
		query.setParameter("p", sellerBuyerModel.getPassword());
		query.setParameter("e", sellerBuyerModel.getEmail());
		int i = query.executeUpdate();
		transaction.commit();
		session.close();
		return i;
	}

}
