package daoImpl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.models.CategoryModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;

import dao.DBConfiguration;
import dao.PostedBidDao;

public class PostedBidDaoImpl implements PostedBidDao {

	@Override
	public void submitAdPost(PostedBidProductsModel bidProductsModel) {

		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		PostedBidProductsModel postedBid = new PostedBidProductsModel();
		CategoryModel categoryModel = session.get(CategoryModel.class,
				bidProductsModel.getCategoryModel().getCategoryId());
		SellerBuyerModel buyerModel = session.get(SellerBuyerModel.class,
				bidProductsModel.getSellerBuyerModel().getId());
		bidProductsModel.setName(bidProductsModel.getName());
		bidProductsModel.setContact(bidProductsModel.getContact());
		bidProductsModel.setDescription(bidProductsModel.getDescription());
		bidProductsModel.setMinPrice(bidProductsModel.getMinPrice());
		bidProductsModel.setStartingDate(bidProductsModel.getStartingDate());
		bidProductsModel.setCategoryModel(categoryModel);
		bidProductsModel.setSellerBuyerModel(buyerModel);
		session.save(bidProductsModel);
		transaction.commit();
		session.close();

	}

	@Override
	public List<PostedBidProductsModel> getPostedProducts() {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("From PostedBidProductsModel");

		List<PostedBidProductsModel> bidProductsModels = query.getResultList();

		transaction.commit();
		session.close();

		return bidProductsModels;
	}

	@Override
	public PostedBidProductsModel getPostedBidProductsModel(Integer Id) {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		PostedBidProductsModel postedBidProductsModel = session.get(PostedBidProductsModel.class, Id);
		transaction.commit();
		session.close();
		return postedBidProductsModel;

	}

}
