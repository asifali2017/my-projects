package daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.models.BidProducts;
import com.models.NotificationModel;
import com.models.PostedBidProductsModel;
import com.models.SellerBuyerModel;
import com.servlets.LoginServlet;

import dao.BidProductsDao;
import dao.DBConfiguration;

public class BidProductsDaoImpl implements BidProductsDao {

	@Override
	public List<BidProducts> getBidDetails(Integer Id) {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(BidProducts.class);
		PostedBidProductsModel postedBidProductsModel = new PostedBidProductsModel();
		postedBidProductsModel.setId(Id);
		criteria.add(Restrictions.eq("bidProductsModel", postedBidProductsModel));
		List<BidProducts> bidProducts = criteria.list();
        session.close();
   		return bidProducts;
	}

	@Override
	public int postBid(BidProducts bidProducts) {
		SessionFactory sessionFactory = DBConfiguration.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		BidProducts products = new BidProducts();
		
		PostedBidProductsModel postedBidProductsModel = session.get(PostedBidProductsModel.class,
				bidProducts.getBidProductsModel().getId());
		SellerBuyerModel sellerBuyerModel = session.get(SellerBuyerModel.class,
				bidProducts.getSellerBuyerModel().getId());
	 	NotificationModel notificationModel = new NotificationModel();
		notificationModel.setBidProductsModel(postedBidProductsModel);
		notificationModel.setSellerBuyerModel(sellerBuyerModel);
		notificationModel.setMessage(LoginServlet.name+" posted a bid");
		notificationModel.setTime("10:20");
		NotificationDaoImpl notificationDaoImpl = new NotificationDaoImpl();
		
		//notificationDaoImpl.addBidNotifications(notificationModel);
		///products.setNotifications(notificationModel);
		products.setBidProductsModel(postedBidProductsModel);
		products.setSellerBuyerModel(sellerBuyerModel);
		products.setBid(bidProducts.getBid());
		int row = (int) session.save(products);
		
		transaction.commit();
		session.close();
		notificationDaoImpl.addNotifications(notificationModel);
		return row;

	}

}
