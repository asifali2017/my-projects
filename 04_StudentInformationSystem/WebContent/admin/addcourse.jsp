<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Course</title>
</head>
<body>
<jsp:include page="header.jsp" />
		<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">
										<input type="hidden" value="" id="update-id"/>
									<div class="form-group">
										<label for="name">Course Title</label>
											<input type="text" name="title" id="title" placeholder="Enter title" class="form-control" required/><br>
									</div>	
									<div class="form-group">
											<label for="code">Course Code</label>
											<input type="text" name="code" id="code" placeholder="Enter code" class="form-control" required/><br>
									</div>
									<div class="form-group">
										<label for="credit">Credit Hours:</label>
										<select class="form-control" id="credit" name="credit">
										<option>Select CH</option>
										<option>1CH</option>
										<option>2CH</option>
										<option>3CH</option>
										<option>4CH</option>
										<option>5CH</option>
										<option>6CH</option>
										<option>7CH</option>
										<option>8CH</option>
										<option>9CH</option>
										<option>10CH</option>
										<option>11CH</option>
										<option>12CH</option>
										<option>13CH</option>
										<option>14CH</option>
										<option>15CH</option>
										<option>16CH</option>
										<option>17CH</option>
										<option>18CH</option>
										<option>19CH</option>
										<option>20CH</option>
										</select>
									</div>
									<div class="form-group">
										<label for="type">Course Type:</label>
										<select class="form-control" id="type" name="type">
										<option>Select Type</option>
										<option>TH</option>
										<option>PR</option>
										</select>
									</div>
									<div class="form-group">
										<input class="btn btn-primary form-control"  style="background-color: #00e58b; color: white" type="button" value="Add Course" name="addcourse" id="add-course" class="btn btn-default form-control btn-success" onclick="checkValidation()"/>
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/course.js"></script>

</body>
</html>