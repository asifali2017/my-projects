<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student</title>
</head>
<body>
<jsp:include page="header.jsp" />
		<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">
									<div class="form-group">
										<label for="name">First Name:</label>
											<input type="text" name="fname" id="fname" placeholder="Enter First Name" class="form-control" required/><br>
									</div>
									<div class="form-group">
										<label for="name">Middle Name:</label>
											<input type="text" name="mname" id="mname" placeholder="Enter Middle Name" class="form-control" required/><br>
									</div>	
									<div class="form-group">
											<label for="code">Last Name:</label>
											<input type="text" name="lname" id="lname" placeholder="Enter Last Name" class="form-control" required/><br>
									</div>
									<div class="form-group">
										<label for="name">Roll Number:</label>
											<input type="text" name="rollnum" id="rollnum" placeholder="Enter Roll Number" class="form-control" required/><br>
									</div>	
									<div class="form-group">
										<input class="btn btn-primary form-control"  style="background-color: #00e58b; color: white" type="button" value="Add Student" name="addstudent" id="addstudent" class="btn btn-default form-control btn-success"/>
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/student.js"></script>

</body>
</html>