<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp" />
				<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
			                  <div class="jumbotron col-sm-6" style="background-color: white;">
							
			                       <div class="form-group">
										<label for="course">Course:</label>
										<select class="form-control" id="course"  required >
											<option value='none'>Select Course</option>
										</select>
						            </div>
							        <div class="form-group">
										<label for="instructor">Instructor:</label>
										<select class="form-control" id="instructor" name="combo" required >
											<option value='none'>Select Instructor</option>
										</select>
									</div>
									<div class="form-group">
										<input type="button" id="assigncourse" value="Assign Course"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>
									<div class="form-group">
										<input type="button" value="Update"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white;display:none;" id="update">
									</div>
									</div>
			                </div>
			             </div>
			          </div>
			                
			


	<jsp:include page="footer.jsp"/>
	
</body>
<script src="js/assigncourse.js"></script>
</html>