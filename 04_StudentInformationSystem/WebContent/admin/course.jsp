<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Courses</title>
</head>
	
<body>
	<jsp:include page="header.jsp" />
	<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form" action="../CourseServlet" method="post">
									<div class="form-group">
										<label for="department">Department<span style="color: red;">*</span></label>
										<select class="form-control" id="department" required placeholder="Select Department">
											
										</select>
									</div>
									<div class="form-group">
										<input type="button" id="add" name="add" value="Add new Course"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>	
									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>				
							</form>
							</div>
						</div>
					</div>
					</div>
	<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="course-table" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>Title</th>
											<th>Code</th>
											<th>Type</th>
											<th>Credit</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='courseTableBody'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>Title</th>
											<th>Code</th>
											<th>Type</th>
											<th>Credit</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/course.js"></script>
			
</body>
</html>