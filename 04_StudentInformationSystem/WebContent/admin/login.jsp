
<!DOCTYPE html>
<html>
<head>
<title>Student Information System</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/jsp; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!--static chart-->
</head>
<style>
body{

background-image: url("images/student.jpg");

}
</style>

<body>

<div class="login-page"  style="margin-top:-100px;">
<center>
<img src="images/newlogo2.png" width="150px">	
   </center> 	 

    <div class="login-main" style="margin-top:10px;" >  	
    	 
    	 <div class="login-head">
				<h1>SALU SIS</h1>
			</div>
			<div class="login-block">
				<form>
					<input type="text" name="username" placeholder="User Name" id="username" required="">
					<input type="password" name="pass" class="lock" placeholder="Password" id="pass">
					
					<p style="color:red;display:none;" id="error">Invalid username and password.</p>
					<div class="forgot-top-grids">
						
						<div class="clearfix"> </div>
					</div>
					<input type="button" name="Sign In" value="Login" id="login"  >	
					                       <center>
					                          <div id="wait" style="width:80px;height:90px;margin-top:20px;display:none;"><img src='images/35.gif' width="70" height="70" /><br></div>
                                   </center>
									</form>
			</div>
      </div>
</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
	 <p>© 2016 Shoppy. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
</div>	
<!--COPY rights end here-->

<!--scrolling js-->
		<script src="js/jquery.nicescroll.js"></script>
		<script src="js/scripts.js"></script>
		<!--//scrolling js-->
<script src="js/bootstrap.js"> </script>
<script src="js/notify.js"></script>
<!-- mother grid end here-->
<script src="js/login.js">
</script>
</body>
</html>