<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student</title>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form" action="../StudentServlet" method="post">
									<div class="form-group">
										<label for="department">Department<span style="color: red;">*</span></label>
										<select class="form-control" id="department" required placeholder="Select Department">
											<option value='none'>Select Department</option>
										</select>
									</div>
									<div class="form-group">
										<label for="semster">Semster<span style="color: red;">*</span></label>
										<select class="form-control" id="semster" name="semster" required placeholder="Select Semster">
											<option value="">Select Semster</option>
										</select>
									</div>
									<div class="form-group">
										<label for="class">Class<span style="color: red;">*</span></label>
										<select required class="form-control" name="class" id="class"  placeholder="Select Class">
											<option value="">Select Class</option>
										</select>
									</div>
									<div class="form-group">
										<input type="submit" id="addnewstudent" name="addnewstudent" value="Add new Student"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>	
									<input id="alertfield" hidden>
									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>				
							</form>
							</div>
						</div>
					</div>
					</div>
	<div class="container" style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="studentTable" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>First Name</th>
											<th>Middle Name</th>
											<th>Last Name</th>
											<th>Roll Number</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='studentTableBody'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>First Name</th>
											<th>Middle Name</th>
											<th>Last Name</th>
											<th>Roll Number</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/student.js"></script>
</body>
</html>