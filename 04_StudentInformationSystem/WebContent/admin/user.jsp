<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp"/>
	<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">

									<div class="form-group">
										<label for="faculty">Faculty:</label>
										<select class="form-control" id="faculty" name="combo" required >
											<option value='none'>Select Faculty</option>
										</select>
									</div>
									<div class="form-group">
										<label for="department">Department:</label>
										<select class="form-control" id="department" name="combo" required >
											<option value='none'>Select Department</option>
										</select>
									</div>
									<div class="form-group">
										<label for="usertype">User Type:</label>
										<select class="form-control" id="usertype" name="combo" required >
											<option value='none'>Select User Type</option>
										</select>
									</div>
									<div class="form-group">
										<a style="background-color: #00e58b; color: white" href="adduser.jsp" 
											class="btn btn-primary form-control" >Add User</a>
									</div>
								
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="userTable" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>User Name</th>
											<th>Department Code</th>
											<th>Account Type</th>
											<th>Update</th>
											<th>Delete</th>
											
										</tr>
									</thead>
									<tbody>

									</tbody>
									<tfoot>
										<tr>
										<th>S_No</th>
											<th>User Name</th>
											<th>Department Code</th>
											<th>Account Type</th>
											<th>Update</th>
											<th>Delete</th></tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
<jsp:include page="footer.jsp"/>
<script src="js/user.js"></script>
</body>
</html>