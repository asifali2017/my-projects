<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student Information System</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/jsp; charset=utf-8" />
<meta name="keywords"
	content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 






</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all">
<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Datatable -->
<link href="css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="css/jquery.dataTables.min.css" rel="stylesheet">

<!--js-->
<script src="js/jquery-2.1.1.min.js"></script>
<!--icons-css-->
<link href="css/font-awesome.css" rel="stylesheet">
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600'
	rel='stylesheet' type='text/css'>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/style.css">
<!--static chart-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
	integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
	crossorigin="anonymous">


<script src="js/Chart.min.js"></script>
<!--//charts-->
<!-- geo chart -->
<script src="//cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"
	type="text/javascript"></script>
<!-- 
<script>
	window.modernizr
			|| document.write('<script src="lib/modernizr/modernizr-custom.js"><\/script>')
</script>

 -->

<!--<script src="lib/jsp5shiv/jsp5shiv.js"></script>-->
<!-- Chartinator  -->
<script src="js/chartinator.js"></script>
<!--skycons-icons-->
<script src="js/skycons.js"></script>
<!--//skycons-icons-->

<!-- JavaScript -->
<script
	src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.min.css" />
<!-- Default theme -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.min.css" />
<!-- Semantic UI theme -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/semantic.min.css" />

<!-- 
    RTL version
-->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/alertify.rtl.min.css" />
<!-- Default theme -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/default.rtl.min.css" />
<!-- Semantic UI theme -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/semantic.rtl.min.css" />
<!-- Bootstrap theme -->
<link rel="stylesheet"
	href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.1/build/css/themes/bootstrap.rtl.min.css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<style>
.user-container {
	position: relative;
	width: 80px;
	max-width: 300px;
}

.image {
	display: block;
	width: 100%;
	height: auto;
	border-radius: 100%;
}

.overlay {
	position: absolute;
	top: 20px;
	background: rgb(0, 0, 0);
	background: rgba(0, 0, 0, 0.5); /* Black see-through */
	color: #f1f1f1;
	width: 80px;
	transition: .5s ease;
	opacity: 0;
	color: white;
	font-size: 10px;
	padding: 10px;
	text-align: center;
	border-radius: 100px;
}

.user-container:hover .overlay {
	opacity: 1;
}
</style>
</head>
<body>
	<c:if test="${param.logout!=null}">
		<c:remove var="name" scope="session" />
		<c:redirect url="login.jsp" />
	</c:if>
	<div class="page-container">
		<div class="left-content">
			<div class="mother-grid-inner">
				<!--header start here-->
				<div class="header-main">
					<div class="header-left">
						<div class="logo-name" style="width: 300%;">
							<a href="index.jsp">
								<h1>Student Information System</h1> <!--<img id="logo" src="" alt="Logo"/>-->
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="header-right">
						<div class="profile_details_left">
							<!--notifications of menu start -->
							<div class="clearfix"></div>
						</div>
						<form id="upload-form" action="UserServlet" method="post"
							enctype="multipart/form-data">
							<input type="file" id="file" name="file1" style="display: none;" />
						</form>
						<!--notification menu end -->
						<div class="profile_details">
							<ul>

								<c:set var="profile" value="..\files${sessionScope.path}" />

								<li class="dropdown profile_details_drop"><a href="#"
									class="dropdown-toggle" data-toggle="dropdown"
									aria-expanded="false">
										<div class="profile_img">
											<span class="prfil-img"> 
											<c:choose>
											<c:when test="${sessionScope.path!=null}">
														<img id="imagePreview" src="${profile}" alt="" width=50px;
															style="border: 2px; border-radius: 100%;"></span>
											</c:when>
											<c:otherwise>
												<img id="imagePreview" src="images/profile.png" alt=""
													width=50px; style="border: 2px; border-radius: 100%;">
												</span>
											</c:otherwise>
											</c:choose>
											<div class="user-name">
												<p>${sessionScope.name}</p>
												<span>${sessionScope.usertype}</span>
											</div>
											<i class="fa fa-angle-down lnr"></i> <i
												class="fa fa-angle-up lnr"></i>
											<div class="clearfix"></div>
										</div>
								</a>
									<ul class="dropdown-menu drp-mnu">
										<!--      <li><a href="#"><img id="profileimage" src="images/profile.png" alt="" width=50px; style="border:2px;border-radius:100%;">Change Image</a></li>
										 -->
										<li>
											<div class="user-container">
											<c:choose>
											<c:when test="${sessionScope.path!=null}">
												<img src="${profile}" alt="Avatar" class="image">
											</c:when>
											<c:otherwise>
												<img src="images/profile.png" alt="Avatar" class="image">
											</c:otherwise>
											</c:choose>
												<div class="overlay">Change</div>
											</div>
										</li>
										<li><a href="cng-password.jsp"><i class="fa fa-cog"></i>
												Change Password</a></li>
										<li><a href="header.jsp?logout=1"><i
												class="fa fa-sign-out-alt"></i> Logout</a></li>
									</ul></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
				<!--heder end here-->
				<!-- script-for sticky-nav -->
				<script src="js/userimage.js"></script>
				<script>
					$(document).ready(function() {

						var navoffeset = $(".header-main").offset().top;
						$(window).scroll(function() {
							var scrollpos = $(window).scrollTop();
							if (scrollpos >= navoffeset) {
								$(".header-main").addClass("fixed");
							} else {
								$(".header-main").removeClass("fixed");
							}
						});

					});
				</script>
</body>
</html>