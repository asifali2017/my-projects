<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<!--slider menu-->
    <div class="sidebar-menu">
		  	<div class="logo"> <a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> <a href="#"> <span id="logo" ></span> 
			      <!--<img id="logo" src="" alt="Logo"/>--> 
			  </a> </div>		  
		    <div class="menu">
		      <ul id="menu" >
		          
		        <li><a href="#"><img src="images/newlogo2.png" width=80px;></a></li>   
		        <li><a href="department.jsp"><i class="fa fa-building"></i><span>Department</span></a></li>
		        <li><a href="faculty.jsp"><i class="fa fa-university "></i><span>Faculty</span></a></li>
		        <li><a href="year.jsp"><i class="fa fa-calendar"></i><span>Years</span></a></li>
		        <li><a href="semester.jsp"><i class="fa fa-book"></i><span>Semester</span></a></li>
		        <li><a href="user.jsp"><i class="fa fa-user"></i><span>User</span></a></li>
		        
		        <li><a href="program.jsp"><i class="fa fa-user-tie"></i><span>Program</span></a></li>
		        
		        <li><a href="teacher.jsp"><i class="fa fa-user-tie"></i><span>Teacher</span></a></li>
		        <li><a href="student.jsp"><i class="fa fa-user-graduate"></i><span>Enroll Student</span></a></li>
		        
		        <li><a href="course.jsp"><i class="fa fa-book-open"></i><span>Course</span></a></li>
		        <li><a href="classes.jsp"><i class="fa fa-chalkboard"></i><span>Class</span></a></li>
		        <li><a href="courseassignment.jsp"><i class="fa fa-clipboard-list"></i><span>Course Assignment</span></a></li>
		        <li><a href="insertattendance.jsp"><i class="fa fa-user-edit"></i><span>Insert Attendance</span></a></li>
		        <li><a href="attendance-report.jsp"><i class="fa fa-address-card"></i><span>Course-Wise Student Report</span></a></li>
		        <li><a href="labreport.jsp"><i class="fa fa-newspaper"></i><span>Class Lactures/Lab Reports</span></a></li>
		      
		      </ul>
		    </div>
	 </div>
</body>
</html>