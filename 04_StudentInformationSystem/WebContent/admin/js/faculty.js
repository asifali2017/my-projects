$(document).ready(function(){
	
	alertify.set('notifier','position', 'bottom-left');
	$('#facultyTable').DataTable();
	$("#faculty-name").alpha();
	getAllRecords();
	var facultyList=[];
		
	
	
	//add faculty
		$("#btn").click(function(){
			if($("#faculty-name").val()==""){
				$("#faculty-name").notify("Please fill out this field.",{position:"right"});
			}else if($("#faculty-name").val().trim()==""){
				$("#faculty-name").notify("Please enter valid name.",{position:"right"});
			}else if($("#faculty-name").val().length<2){
				$("#faculty-name").notify("Atleast 2 characters required",{position:"right"});
			}else{
				
				if($("#btn").val()=="Add Faculty"){
					if(checkFaculty($("#faculty-name").val())==true){
						$("#faculty-name").notify("Already exist.",{position:"right"});
					}
					else{
						$.ajax({
						url:"../FacultyServlet",
						type:"post",
						data:{
							facultyName:$("#faculty-name").val(),
							method:"addRecord",
						},
						success:function(row){
							if(row>0){
								$("#faculty-name").val("");
								getAllRecords();
								alertify.success('Record Added');
							}
							else{
								$("#faculty-name").val("");
								getAllRecords();
								alertify.error('Record Not Added');
							}
						}
						
					});
					}
				}
			//update faculty
			else if($("#btn").val()=="Update Faculty"){
				$.ajax({
					url:"../FacultyServlet",
					type:"post",
					data:{
						facultyId:$("#faculty-id").val(),
						facultyName:$("#faculty-name").val(),
						method:"updateRecord",
					},
					success:function(row){
						if(row>0){
							$("#faculty-name").val("");
							$("#btn").val("Add Faculty");
							getAllRecords();
							alertify.success("Record Updated");
						}else{
							$("#faculty-name").val("");
							$("#btn").val("Add Faculty");
							getAllRecords();
							alertify.error("Record Not Updated");
						}
					}
				});
			}
			}
		});
		
		$("#clear").click(function(){
			clearFields();
		});
		
		function clearFields(){
			$("#faculty-name").val("");
		} 
		
		//check exist before or not
		function checkFaculty(){
			if(facultyList.includes($("#faculty-name").val().toLowerCase())==true){
				return true;
			}else{
				return false;
			}
		}
		
		//get list of faculties
		function getAllRecords(){
			$.ajax({
				url:"../FacultyServlet",
				type:"post",
				data:{
					method:"getAllRecords",
				},
				success:function(object){
					var i = 1;
		    		 var t =$('#facultyTable').DataTable();
		    		 t.clear().draw();
		    		 for(x in object){
		    		 facultyList.push(object[x].facultyName.toLowerCase());
		    		 var updateRow = "<td><Button class='btn btn-primary edit' value='"+object[x].facultyId+"' id='updatebtn'>Update</Button></td>";
		    		 var deleteRow = "<td><Button class='btn btn-danger delete' value='"+object[x].facultyId+"' id='deletebtn'>Delete</Button></tr>";
		    	     var r = [i,object[x].facultyName,updateRow,deleteRow];
		    	     t.row.add(r).draw();
		    	     i++;
		    		 }
				}
			});
		}
		
		
		$("#btnexport").click(function () {
            $("#facultyTable").table2excel();
        });

		
		//get faculty by faculty id
		$(document).on("click",".edit",function(e){
			$.ajax({
				url:"../FacultyServlet",
				type:"post",
				data:{
					editId:$(this).val(),
					method:"getRecordById",
				},
				success:function(object){
					$("#faculty-id").val(object.facultyId);
					$("#faculty-name").val(object.facultyName);
					$("#btn").val("Update Faculty");
				}
			});
		});
		
		//delete faculty
		$(document).on("click",".delete",function(e){
			$.ajax({
				url:"../FacultyServlet",
				type:"post",
				data:{
					deleteId :$(this).val(),
					method:"deleteRecord",
				},
				success:function(row){
					if(row>0){
						getAllRecords();
						alertify.success("Record Deleted");
					}else{
						getAllRecords();
						alertify.error("Record Not Deleted");
					}
					
				}
			});
		});
	});