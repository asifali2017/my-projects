$(document).ready(function(){
	alertify.set('notifier','position', 'bottom-left');
	$('#program-table').DataTable();
	getDepartments();
	getAllRecords();
	
	var okToSubmit=false;
	$("#upload-form").submit(function(e){
		if(checkValidation()==false){
			e.preventDefault();
		}else{
			$("#upload-form").submit();
		}
	});
	
	
	if(getUrlParameter("added")=="success"){
		alertify.success('Record Added');
	}else if(getUrlParameter("added")=="error"){
		alertify.error('Record Not Added');
	}else if(getUrlParameter("updated")=="success"){
		alertify.success('Record Updated');
	}
	else if(getUrlParameter("updated")=="error"){
		alertify.error('Record Not Updated');
	}
	
	
	function checkValidation(){
		 if($("#department").prop('selectedIndex')==0){
			 $("#department").notify("Select department.",{position:"right"});
			 okToSubmit=false;
		 }else{
			 okToSubmit=true;
		 }
		 return okToSubmit;
	 }
	
	//get all programs
	function getAllRecords(){
		$.ajax({
			url:"../ProgramServlet",
			type:"post",
			data:{
				method:"getAllRecords",
			},
			success:function(object){
				var i=1;
				var table=$("#program-table").DataTable();
				table.clear().draw();
				var departmentId=$("#department").val();
				for (x in object){
					var updateRow = "<td><a href='programentry.jsp?updateId="+object[x].programId+"&department="+departmentId+"'><Button class='btn btn-primary edit' id='update-btn'>Update</Button></td>";
		    		var deleteRow = "<td><Button class='btn btn-danger delete' value='"+object[x].programId+"' id='delete-btn'>Delete</Button></tr>";
		    		var r=[i,object[x].programTitle,object[x].programCode,object[x].days,updateRow,deleteRow];
		    		table.row.add(r).draw();
		    		i++;
				}
			}
		});
	}
	
	//get all records departmentwise
	$('#department').change(function() {
	    	alert("hello");
		   if($(this).val()=='none'){
	    		getAllRecords();
	    	}else{
	    		$.ajax({
					url:"../ProgramServlet",
					type:"post",
					data:{
						departmentId:$(this).val(),
		    			method:"getAllRecordsDepartmentWise",
					},
					success:function(object){
						var i=1;
						var table=$("#program-table").DataTable();
						table.clear().draw();
						for (x in object){
							var updateRow = "<td><a href='programentry.jsp?updateId="+object[x].programId+"'><Button class='btn btn-primary edit' id='update-btn'>Update</Button></td>";
				    		var deleteRow = "<td><Button class='btn btn-danger delete' value='"+object[x].programId+"' id='delete-btn'>Delete</Button></tr>";
				    		var r=[i,object[x].programTitle,object[x].programCode,object[x].days,updateRow,deleteRow];
				    		table.row.add(r).draw();
				    		i++;
						}
					}
				});
	    	}
	    
	});
	
	//fill department combo
	function getDepartments(){
		$.ajax({
			url:"../DepartmentServlet",
			type:"post",
			data:{
				method:"getDepartment",
			},
			success:function(data){
				console.log(data.length);
				if(data.length>=2){
					for(x in data){
						$("#department").append("<option value="+data[x].departmentId+">"+data[x].departmentName+"</option>");
					}
					
				}else{
					$("#department").append("<option value="+data.departmentId+">"+data.departmentName+"</option>");
					$("#department").val(data.departmentId);
					//$("select option[value="+data.departmentId+"]").attr("selected","selected");
					$("#department").css('pointer-events','none');
				}
				
			}
		});
	}

	
	//delete program
	$(document).on("click",".delete",function(e){
		$.ajax({
			url:"../ProgramServlet",
			type:"post",
			data:{
				deleteId :$(this).val(),
				method:"deleteRecord",
			},
			success:function(row){
				if(row>0){
					getAllRecords();
					alertify.success("Record Deleted");
				}else{
					getAllRecords();
					alertify.error("Record Not Deleted");
				}
				
			}
		});
	});
	
	
	//Get URl parameter
	function getUrlParameter(name) {
	    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	    var results = regex.exec(location.search);
	    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	}
	
});