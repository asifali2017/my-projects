$(document).ready(function (){
        function readURL(input) {
				    if (input.files && input.files[0]) {
				        var reader = new FileReader();
				        reader.onload = function(e) {
				        	
				            $('#imagePreview').attr('src', e.target.result);
				            $('#imagePreview').hide();
				            $('#imagePreview').fadeIn(650);
				            $('.image').attr('src', e.target.result);
				            $('.image').hide();
				            $('.image').fadeIn(650);
				        }
				        reader.readAsDataURL(input.files[0]);
				    }
				}

				$("#file").change(function() {
                    uploadUserImage();
					readURL(this);
				});
			 $(".image").click(function (){
			
				  $("#file").click();
			 });
			 $(".overlay").click(function (){
					
				  $("#file").click();
			 });
             function uploadUserImage(){
	    	       
            	 // Get form
	    	        var form = $('#upload-form')[0];

	    			// Create an FormData object
	    	        var data = new FormData(form);

	    		// disabled the submit button
	    	        $("#submit").prop("disabled", true);

	    	        $.ajax({
	    	            type: "POST",
	    	            enctype: 'multipart/form-data',
	    	            url: "../UserServlet",
	    	            data:data,
	    	            processData: false,
	    	            contentType: false,
	    	            cache: false,
	    	            timeout: 600000,
	    	            success: function (data) {
	    	            	
	    	    		    console.log("SUCCESS : ", data);
	    	                $("#submit").prop("disabled", false);

	    	            },
	    	            error: function (e) {
	    	                console.log("ERROR : ", e);
	    	                $("#submit").prop("disabled", false);

	    	            }
	    	        });
	    		
	               	 } 
});       