$("doucment").ready(function(){
	$("#teacherTable").DataTable();
	getDepartments();
	fillPrefixCombo();
	
	//  add new teacher click
	var dId="";
	$("#addnewteacher").click(function(){
		window.location= "./addteacher.jsp?departmentId="+dId+"&dept="+departmentId+"";
	});
	// validation
	var validate=true;
	function checkValidation(){
		var firstName = $("#fname").val();
		var lastName = $("#lname").val();
		var prefix = $("#prefix").val();
		//Check input Fields Should not be blanks.
		if (prefix == 'Select Prefix') {
			$("#prefix").notify("Please select prefix.",{position:"right"});
			validate=false;	
		}
		else if (firstName == '') {
			$("#fname").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else if (lastName == '') {
			$("#lname").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else{
			validate=true;
		}
		return validate;
	}
	// get values from url
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	// update click
	var id= getParameterByName('teacherId');
	var add=getParameterByName('add');
	var update=getParameterByName('update');
	var addDepartmentId="";
	var dept=getParameterByName('departmentId');
	var deptID=getParameterByName('dept');
	if(dept!=null && dept!=""){
		addDepartmentId=dept;
	}
	if(deptID!=null && deptID!=""){
		addDepartmentId=deptID;
	}
	var updateDepartmentId=getParameterByName('department');
	if(id!=null){
		updateClick();
	}
	// alertify
	if(add=="success"){
		alertify.success('Teacher Added.');
	    }
	if(update=="success"){
		alertify.success('Teacher Updated.');
	    }
	var departmentId="";
	$("#department").change(function() {
		departmentId=this.value;
		getAllTeachers(departmentId);
	    
	});
	// prefix combo on change
	var selectedFaculty="";
	$("select#prefix").change(function() {
		selectedPrefix = $("#prefix option:selected").val();
		
	});
	//get list of prefixes
	function fillPrefixCombo(){
		$.ajax({
			url:"../TeacherServlet",
			type:"POST",
			data:{
				method:"getAllPrefixes",
			},
			success:function(result){
				for(x in result){
					$("#prefix").append("<option value="+result[x].prefixId+">"+result[x].prefix+"</option>");
				}
			}
			
		});
	}
	// add teacher
	$("#addteacher").click(function(){
		if(checkValidation()==true){
			if($("#addteacher").val()=="Add Teacher"){
				$.ajax({
					url:"../TeacherServlet",
					type:"POST",
					data:{
						firstName:$("#fname").val(),
						lastName:$("#lname").val(),
						prefix:selectedPrefix,
						deptId:addDepartmentId,
						method:"addTeacher",	
					},
					success:function(result){
						clearFields();
					    window.location = "../admin/teacher.jsp?add=success";
					    
					}	
				});
			}
			// update teacher
			else if($("#addteacher").val()=="Update Teacher"){
				$.ajax({
					url:"../TeacherServlet",
					type:"POST",
					data:{
						teacherId:$("#addteacher").text(),
						firstName:$("#fname").val(),
						lastName:$("#lname").val(),
						prefix:$("#prefixField").val(),
						deptId:updateDepartmentId,
						method:"updateTeacher",	
					},
					success:function(result){
						clearFields();
						$("#addteacher").val("Add Teacher");
						window.location = "../admin/teacher.jsp?update=success";
					}	
				});
			}
			
		}
		else{
			checkValidation();
		}
		
	});
	// get teacher Model
	function updateClick(){
		$.ajax({
			url:"../TeacherServlet",
			type:"POST",
			data:{
				teacherId:id,
				method:"getTeacherModelById",
			},
			success:function(data){
				$("#fname").val(data.firstName);
				$("#lname").val(data.lastName);
				$("#prefixField").val(data.prefixId);
				$("#prefix option").text(data.prefix);
				$("#addteacher").val("Update Teacher");
				$("#addteacher").html(data.teacherId);		
			}
		});		
		
	}
	// add delete teacher
	$(document).on("click","#deletebtn", function(e){
		$.ajax({
			url:"../TeacherServlet",
			type:"POST",
			data:{
				teacherId:e.target.value,
				method:"deleteTeacher",
			},
			success:function(data){
				alertify.success('Record Deleted.');
				getAllTeachers(departmentId);
			}
		});
	});
	// clear fields
	$("#clear").click(function (){  
 	   clearFields();
	});
	
	//fill department combo
	function getDepartments(){
		$.ajax({
			url:"../DepartmentServlet",
			type:"POST",
			data:{
				method:"getDepartment",
			},
			success:function(data){
				if(data.length>=2){
					for(x in data){
						$("#department").append("<option value="+data[x].departmentId+">"+data[x].departmentName+"</option>");
					}
					
				}else{
					$("#department").append("<option value="+data.departmentId+">"+data.departmentName+"</option>");
					$("#department").val(data.departmentId);
					$("#departmentId").val(data.departmentId);
					dId=data.departmentId;
					getAllTeachers(data.departmentId);
					//$("select option[value="+data.departmentId+"]").attr("selected","selected");
					$("#department").css('pointer-events','none');
				}
				
			}
		});
	}
	
	// get all departments
	function getAllTeachers(deptId){
		var i = 1;
		$.ajax({
			url:"../TeacherServlet",
			type:"POST",
			data:{
				dId:deptId,
				method:"getAllTeachers",
			},
			success:function(result){
				if(result=="none"){
					var table = $('#teacherTable').DataTable();
					table.clear().draw();
				}
				else{
					var table = $('#teacherTable').DataTable();
					var departmentId=$("#department").val();
		    		 table.clear().draw();
		    		 for(x in result){
		    		 var updateRow = "<td><a href='./addteacher.jsp?teacherId="+result[x].teacherId+"&department="+departmentId+"'><Button value='"+result[x].teacherId+"' id='updatebtn' class='btn btn-primary'>Update</Button></a></td>";
		    		 var deleteRow = "<td><Button value='"+result[x].teacherId+"' id='deletebtn' class='btn btn-danger'>Delete</Button></td>";
		    	     var r = [i,result[x].prefix,result[x].firstName,result[x].lastName,updateRow,deleteRow];
		    	     table.row.add(r).draw();
		    	     i++;
		    		 }
				}
	    		 
			} 
			
		});
	}
	function clearFields(){
		departmentName:$("#fname").val("");
		departmentCode:$("#lname").val("");
		$("#prefix").val("Select Prefix");
	}
	
});