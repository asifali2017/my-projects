$("doucment").ready(function(){
	$('#departmentTable').DataTable();
	fillFacultyCombo();
	getAllDepartments();
	
	//........................ validation .....................//
	var validate=true;
	function checkValidation(){
		var departmentName = $("#dname").val();
		var departmentCode = $("#dcode").val();
		var faculty = $("#faculty").val();
		//Check input Fields Should not be blanks.
		if (departmentName == '') {
			$("#dname").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else if (departmentCode == '') {
			$("#dcode").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else if (faculty == 'Select Faculty') {
			$("#faculty").notify("Please select faculty.",{position:"right"});
			validate=false;		
		}
		else{
			validate=true;
		}
		return validate;
	}
	//........................ fill faculty combo .....................//
	function fillFacultyCombo(){
		$.ajax({
			url:"../FacultyServlet",
			type:"POST",
			data:{
				method:"getAllRecords",
			},
			success:function(result){
				for(x in result){
					
					$("#faculty").append("<option  value='"+result[x].facultyId+"'>"+result[x].facultyName+"</option>");
				}
			}
			
		});
	}
	// faculty combo on change
	var selectedFaculty="";
	$("select#faculty").change(function() {
		selectedFaculty = $("#faculty option:selected").val();
	});
	$("#adddepartment").click(function(){
		//........................ add department .....................//
		if(checkValidation()==true){
			if($("#adddepartment").val()=="Add Department"){
				$.ajax({
					url:"../DepartmentServlet",
					type:"POST",
					data:{
						departmentName:$("#dname").val(),
						departmentCode:$("#dcode").val(),
						faculty:$("#faculty").val(),
						method:"addDepartment",	
					},
					success:function(result){
						if(result=="code exists"){  
							$("#dcode").notify("Code already exits try another.",{position:"right"});
						}
						if(result=="name exists"){
							$("#dname").notify("Name already exits try another.",{position:"right"});	
						}
						if(result=="success"){
							clearFields();
							getAllDepartments();
							alertify.success('Record added.');
						
						}else{
							alertify.error('Can not add record.');
						}
		
					}	
				});
			}
			//........................ update department .....................//
			else if($("#adddepartment").val()=="Update Department"){
				
				$.ajax({
					url:"../DepartmentServlet",
					type:"POST",
					data:{
						departmentId:$("#adddepartment").text(),
						departmentName:$("#dname").val(),
						departmentCode:$("#dcode").val(),
						faculty:$("#faculty").val(),
						method:"updateDepartment",	
					},
					success:function(result){
						if(result=="code exists"){  
							$("#dcode").notify("Code already exits try another.",{position:"right"});
						}
						if(result=="name exists"){
							$("#dname").notify("Name already exits try another.",{position:"right"});	
						}
						if(result=="success"){
							clearFields();
							getAllDepartments();
							fillFacultyCombo();
							$("#adddepartment").val("Add Department");
							alertify.success('Record updated.');
						}else{
							alertify.error('Can not update record.');
						}
					}	
				});
			}
		}
		else{
			checkValidation();
		}
		
	});
	//........................ get department by id .....................//
	$(document).on("click",".update", function(e){
		$.ajax({
			url:"../DepartmentServlet",
			type:"POST",
			data:{
				departmentId:e.target.value,
				method:"getDepartmentById",
			},
			success:function(data){
				window.scrollTo(0, 0);
				$("#dname").val(data.departmentName);
				$("#dcode").val(data.departmentCode);
				$("#facultyField").val(data.facultyId);
				$('#faculty option').text(data.facultyName);
				$("#adddepartment").val("Update Department");
				$("#adddepartment").html(data.departmentId);		
			}
		});		
	});
	//........................ delete department .....................//
	$(document).on("click",".delete", function(e){
		$.ajax({
			url:"../DepartmentServlet",
			type:"POST",
			data:{
				departmentId:e.target.value,
				method:"deleteDepartment",
			},
			success:function(result){
				if(result=="success"){
					getAllDepartments();
					alertify.success('Record deleted.');
				}else{
					alertify.error('Can not delete record.');
				}
			}
		});
	});
	//........................ view departments .....................//
	function getAllDepartments(){
		$.ajax({
			url:"../DepartmentServlet",
			type:"POST",
			data:{
				method:"getAllDepartments",
			},
			success:function(result){
				 var i = 1;
	    		 var t = $('#departmentTable').DataTable();
	    		 t.clear().draw();
	    		 for(v in result){
	    		 var updateRow = "<td><button value='"+result[v].departmentId+"' class='btn btn-primary update' >Update</button></td>";
	    		 var deleteRow = "<td><button value='"+result[v].departmentId+"' class='btn btn-danger delete'>Delete</button></td>";
	    	     var r = [i,result[v].departmentName,result[v].departmentCode,result[v].facultyName,updateRow,deleteRow];
	    	     t.row.add(r).draw();
	    	     i++;
				
			}
			}
		});
	}
	//........................ clear  .....................//
	$(document).on("click","#clear", function(e){
		clearFields();
	});
	function clearFields(){
		departmentName:$("#dname").val("");
		departmentCode:$("#dcode").val("");
		$("#faculty option").text("Select Faculty");
		$('#faculty').children('option:not(:first)').remove();
		
	}
	
});