	

         $(document).ready(function() {
        	 $.notify.defaults({ className: "success" });
			alertify.set('notifier','position', 'bottom-left');
			$("#startdate").datepicker();
			$("#enddate").datepicker();
		     $("#cityTable").DataTable();
			 
		    
			//.......................get list of years..........................//
			$.ajax({
				url:"../SemesterServlet",
				type:"post",
				data:{
					method:"getAllYears"
				},
				success:function(a){
               
					for(x in a){
						
						$("#year").append("<option>"+a[x].yearName+"</option>");
					}
				}
				
			});
		
			//.......................add new semester..........................//
		  $("#addsemester").click(function (){
			  
			 if($("#year").prop('selectedIndex')==0){
			
				 $("#year").notify("Select Year",{position:"right"});
			  
			 }else if($("#startdate").val()==""){
			
				 $("#startdate").notify("Select Start Date",{position:"right"});
			  
			 }else if($("#enddate").val()==""){
			
				 $("#enddate").notify("Select End Date",{position:"right"});
			 
			 }else{
		  $.ajax({
			 url:"../SemesterServlet",
			 type:"post",
			 data:{
			    method:"addNewSemester",
                year:$("#year").val(),
                type:$("input[name='optradio']:checked").val(),
                startdate:$("#startdate").val(),
                enddate:$("#enddate").val() 
			 },
			 success:function(a){
				 
				 if(a>0){
					viewSemesterTable();
					alertify.success('Record added.');
				    clearFields();
				 }
				 
			 }
			 
		  });
			  }
		  });
		//.......................get semester model..........................//	 
       $(document).on('click','#updatebtn',function(){
			  
  			  var semesterId = $(this).val();
  			  $.ajax({
				 url:"../SemesterServlet",
				 type:"post",
				 data:{
					 method:"getSemesterModel",
					 id:semesterId
				 },
				 success:function(a){
					 window.scrollTo(0, 0);
					 $("#year").val(a.year);
					 $("#startdate").val(a.startDate);
					 $("#enddate").val(a.endDate);
					 $("#semesterid").val(semesterId);
					 $("input[name='optradio'][value=" + a.type + "]").prop('checked', true);
					 $("#addsemester").hide();
					 $("#update").show();
				 }
				  
			  });
			  
			  
		  });
     //.......................update semester data..........................//
       $(document).on('click','#update',function(){
    	   if($("#year").prop('selectedIndex')==0){
    		   
				  $("#year").notify("Select Year",{position:"right"});
			  
    	   }else if($("#startdate").val()==""){
				 
    		   $("#startdate").notify("Select Start Date",{position:"right"});
			
    	   }else if($("#enddate").val()==""){
			
    		   $("#enddate").notify("Select End Date",{position:"right"});
			  
    	   }else{
    	 $.ajax({
    		url:"../SemesterServlet",
    		type:"post",
    		data:{
    			method:"updateSemester",
                year:$("#year").val(),
                type:$("input[name='optradio']:checked").val(),
                startdate:$("#startdate").val(),
                enddate:$("#enddate").val(),
                semesterid:$("#semesterid").val()
    			
    		},
    		success:function(a){
    			if(a>0)
    			{
    				alertify.success('Record updated.');
        			viewSemesterTable();
        		    clearFields();
    			}else{
    				alertify.error('Can not update.');
    			}
    		}
    	 
    		 
    	 });
			  }
    	   
       });
     //.......................delete semester data..........................//
       $(document).on('click','#deletebtn',function(){
    	   var semesterId = $(this).val();
 			$.ajax({
 				url:"../SemesterServlet",
 	    		type:"post",
 	    		data:{
 	    			id:semesterId,
 	    			method:"deleteSemester"
 	    		},
 	    		success:function(a){
 	    			if(a>0)
 	    			{
 	    				viewSemesterTable();
 	 	    			alertify.success('Record deleted');
 	    			}else{
 	 	    			alertify.error('Could not delete.');
 	    			}
 	    			
 	    		}
 				
 			});
       });	
       $("#clear").click(function (){
			  
    	   clearFields();
		
		 }); 
		 
     //.......................get semester data..........................//
          viewSemesterTable();
		  function viewSemesterTable(){
		    	var i = 1;
		    	
		    	$.ajax({
		    	 url:"../SemesterServlet",
		    	 type:"post",
		    	 data:{
		    		 method:"viewSemesterData",
		    	 },
		    	 success:function(a){
		    		 
		    		 var t = $('#cityTable').DataTable();
		    		 t.clear().draw();
		    		 for(x in a){
		    	    
		    		 var updateRow = "<td><Button class='btn btn-primary' value="+a[x].semesterId+" id='updatebtn'>Update</Button></td>";
		    		 var deleteRow = "<td><Button class='btn btn-danger' value="+a[x].semesterId+" id='deletebtn'>Delete</Button></tr>";
		    	     var r = [i,a[x].type,a[x].year,a[x].startDate,a[x].endDate,updateRow,deleteRow];
		    	     t.row.add(r).draw();
		    	     i++;
		    		 }		    		 
		    	  }
		    	
		    	});
		    	
		    }
		//.......................clear fields..........................//
		  function clearFields(){
			  $("#year").val("");
			  $("input[name='optradio'][value='First']").prop('checked', true);
			  $("#startdate").val("");
              $("#enddate").val("");
              $("#semesterid").val("");
              $("#addsemester").show();
			  $("#update").hide();
			  
		  }
		  
		  
		
		  });
