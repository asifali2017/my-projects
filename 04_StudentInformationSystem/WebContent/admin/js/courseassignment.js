$(document).ready(function (){

	fillProgramCombo();
	fillAllSemesters();


	var semesterId;
	var classId;
	var programId;

	$('select[name="combo"]').change(function() {

		semesterId = $("#semester").val();
		classId = $("#class").val();
		programId = $("#program").val();
		fillClassCombo();
		if(semesterId!="none"&&classId!="none"&&programId!="none"){
			getAllAssignedCourses();
		   $("#assigncoursebtn").show();	
		}else{
			$("#assigncoursebtn").hide();
		}
	});
	//........................fill department combo.....................//
	$.ajax({
		url:"../DepartmentServlet",
		type:"post",
		data:{
		   method:"getAllDepartments"	
		},
		success:function(a){
			
			for(x in a){
			$("#department").append("<option>"+a[x].departmentName+"</option>");
			}
		}
		
	});
	//........................fill semester combo.....................//
	function fillAllSemesters()
	{
		$.ajax({
			url:"../SemesterServlet",
			type:"Post",
			data:{
			   method:"viewSemesterData"	
			},
			success:function(a){
				
				for(x in a){
				$("#semester").append("<option value="+a[x].semesterId+">"+a[x].type+" Of "+a[x].year+"</option>");
				}
			}
		});
	}
	//...............................fill program combo...........................\\\\\
	function fillProgramCombo()
	{
		$.ajax({
			url:"../ProgramServlet",
			type:"post",
			data:{
			   method:"getAllRecords"	
			},
			success:function(a){
				
				for(x in a){
					$("#program").append("<option value='"+a[x].programId+"'>"+a[x].programTitle+"</option>");
				}
			}
		});
	}
	//.............................fillClassCombo.............................\\\
	function fillClassCombo()
	{
		$.ajax({
			url:"../CourseAssignmentServlet",
			type:"post",
			data:{
			   method:"getClassWithSemesterAndProgram",
			   semesterId:semesterId,
			   programId:programId
			},
			success:function(a){
				
				for(x in a){
					$("#class").append("<option value='"+a[x].classId+"'>"+a[x].programDTO.programTitle+" "+"P - "+a[x].partDTO.part+"</option>");
				} 
			}
		});
	}
	//.............................getAllAssignedCourses.....................\\\
	function getAllAssignedCourses(){
		var i = 1;
		$.ajax({
			url:"../CourseAssignmentServlet",
			method:"post",
			data:{
				method:"getAllAssignedCourses",
				classId:classId
			},
			success:function(a){
				 var t = $('#courseassignmenttable').DataTable();
	    		 t.clear().draw();
	    		 for(x in a){
	    	    
	    		 var updateRow = "<td><a class='btn btn-primary' href='assigncourse.jsp?id="+a[x].courseAssignmentId+"' '>Update</a></td>";
	    		 var deleteRow = "<td><Button class='btn btn-danger' value="+a[x].courseAssignmentId+" id='deletebtn'>Delete</Button></tr>";
	    	     var r = [i,a[x].courseAssigned,a[x].teacherFirstName,updateRow,deleteRow];
	    	     t.row.add(r).draw();
	    	     i++;
	    		 }
			
			
			}
			
			
		});
		
		
	}
	//.......................delete courseassignment data..........................//
	$(document).on('click','#deletebtn',function(){
		  
		var courseAssignmentId = $(this).val();
		 	
		$.ajax({
				url:"../CourseAssignmentServlet",
	  		type:"post",
	  		data:{
	  			id:courseAssignmentId,
	  			method:"deleteCourseAssignment"
	  		},
	  		success:function(a){
	  		
	  			if(a>0)
	  			{
	  				getAllAssignedCourses();
		    			alertify.success('Record deleted');
	  			}else{
		    			alertify.error('Could not delete.');
	  			}
	  			
	  		}
				
			});
	});
	$("#assigncoursebtn").click(function(){
		window.location = "assigncourse.jsp?classId="+classId+"";
	});
	
	
	
	
});