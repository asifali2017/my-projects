$("doucment").ready(function(){
	alertify.set('notifier','position', 'bottom-left');
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	$('#course-table').DataTable();
	//on change
   
	getDepartments();
	getCourses();
    
	$("#add").click(function (){
		  var department=$("#department").val();

		 window.location = "addcourse.jsp?depart="+department+"";
		
	});
	//validation
	var okToSubmit=false;
	//validation
	function checkValidation(){
		 if($("#title").val()==""){
			 $("#title").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#title").val().trim()==""){
			 $("#title").notify("Please enter valid title.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#code").val()==""){
			 $("#code").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#code").val().trim()==""){
			 $("#code").notify("Please enter valid code.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#credit").val()=="" ||$("#credit").val()=="Select CH"){
			 $("#credit").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#type").val()=="" ||$("#type").val()=="Select Type"){
			 $("#type").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else{
			 okToSubmit=true;
		 }
		 return okToSubmit;
	 }

	var urlParams = new URLSearchParams(location.search);
	var courseId=urlParams.get('courseId');
	if(courseId!=null){
		$.ajax({
			url:"../CourseServlet",
			type:"post",
			data:{
				updateId:courseId,
				method:"getDataById"
				},
				success:function(data1){
					var data=JSON.parse(data1);
						$("#title").val(data.courseTitle);
						$("#code").val(data.courseCode);
						$("#type").val(data.courseType);
						$("#credit").val(data.courseCredit);
						$("#update-id").val(data.courseId);
						$("#add-course").val("Update Course");
				}
			});
	}
	function getAllRecords(){
		var i = 1;
		var department = $("#department").val();
		alert(department);
		$.ajax({
			url:"../CourseServlet",
			type:"POST",
			data:{
				department:department,
				method:"getAllRecords",
			},
			success:function(result){
	    		 var table = $('#course-table').DataTable();
	    		 table.clear().draw();
	    		 var object=result;
	    		 for(x in object){
	    		 var updateRow = "<td><a href='./addcourse.jsp?courseId="+object[x].courseId+"'><Button value='"+object[x].courseId+"' id='updatebtn' class='btn btn-primary'>Update</Button></a></td>";
	    		 var deleteRow = "<td><Button value='"+object[x].courseId+"' id='deletebtn' class='btn btn-danger'>Delete</Button></td>";
	    	     var r = [i,object[x].courseTitle,object[x].courseCode,object[x].courseType,object[x].courseCredit,updateRow,deleteRow];
	    	     table.row.add(r).draw();
	    	     i++;
	    		 }
			} 
			
		});
	}
	
	
	function getCourses(){
		   if($("#department").val()=='none'){
	    		getAllRecords();
	    	}else{
	    		alert($("#department").val());
	    		$.ajax({
					url:"../CourseServlet",
					type:"post",
					data:{
						departmentId:$("#department").val(),
		    			method:"getAllRecords",
					},
					success:function(object){
						 var table = $('#course-table').DataTable();
			    		 table.clear().draw();
			    		 var object=result;
			    		 for(x in object){
			    		 var updateRow = "<td><a href='./addcourse.jsp?courseId="+object[x].courseId+"'><Button value='"+object[x].courseId+"' id='updatebtn' class='btn btn-primary'>Update</Button></a></td>";
			    		 var deleteRow = "<td><Button value='"+object[x].courseId+"' id='deletebtn' class='btn btn-danger'>Delete</Button></td>";
			    	     var r = [i,object[x].courseTitle,object[x].courseCode,object[x].courseType,object[x].courseCredit,updateRow,deleteRow];
			    	     table.row.add(r).draw();
			    	     i++;
			    		 }
					}
				});
	    	}
	    
	
	}
	
	//fill department combo
	function getDepartments(){
		$.ajax({
			url:"../DepartmentServlet",
			type:"post",
			data:{
				method:"getDepartment",
			},
			success:function(data){
				console.log(data.length);
				if(data.length>=2){
					for(x in data){
						$("#department").append("<option value="+data[x].departmentId+">"+data[x].departmentName+"</option>");
					}
					
				}else{
					$("#department").append("<option value="+data.departmentId+">"+data.departmentName+"</option>");
					$("#department").val(data.departmentId).change();
					//$("select option[value="+data.departmentId+"]").attr("selected","selected");
					$("#department").css('pointer-events','none');
				}
				
			}
		});
	}

	
	$("#add-course").click(function(){
		if(checkValidation()==true){
		if($("#add-course").val()=="Add Course"){
			alert(getParameterByName("depart"));
			$.ajax({
				url:"../CourseServlet",
				type:"POST",
				data:{
					title:$("#title").val(),
					code:$("#code").val(),
					type:$("#type").val(),
					credit:$("#credit").val(),
					department:getParameterByName("depart"),
					method:"addCourse",	
				},
				success:function(result){
					if(result>0){
						alertify.success('Record added.');
					}else{
						alertify.error('Record added  fail.');
					}
					location.replace("../admin/course.jsp");
				}	
			});
			
		}else if($("#add-course").val()=="Update Course"){
			$.ajax({
				url:"../CourseServlet",
				type:"POST",
				data:{
					title:$("#title").val(),
					code:$("#code").val(),
					type:$("#type").val(),
					credit:$("#credit").val(),
					updateId:courseId,
					method:"updateCourse",	
				},
				success:function(result){
					if(result>0){
						alertify.success('record successfully updated');
						location.replace("../admin/course.jsp");
					}else{
						alertify.error('Record not updated.');
					}
				}	
			});
		}
		}
	});
	//delete record
	$(document).on("click","#deletebtn",function(e){
		$.ajax({
			url:"../CourseServlet",
			type:"post",
			data:{
				deleteId:e.target.value,
				method:"deleteRecord"
				},
				success:function(data){
					alert(data);
					var deptName=$("#department").val();
					getAllRecords(deptName);
					if(data>0){
						alertify.success('record successfully deleted');
					}else{
						alertify.error('Record not deleted.');
					}
				}
			});
			});
	//clear fields
	$("#clear").click(function(){
		$("#title").val("");
		$("#code").val("");
		$("#type").val("");
		$("#credit").val("");
	});
	
});