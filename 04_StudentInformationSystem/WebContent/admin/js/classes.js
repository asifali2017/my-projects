$(document).ready(function()
{
	$('#classesTable').DataTable();
	getDepartments();
	fillAllSemesters();
	
	$('#department').change(function()
	{
		searchClass();
	});
	
	$('#semester').change(function()
	{
		searchClass();
	});
	
	function searchClass()
	{
		$.ajax({
			url:"../ClassesServlet",
			type:"Post",
			data:{
				   method:"searchClasses",
				   semesterId : $('#semester').val(),
				   departmentId : $('#department').val()
				},
				success:function(result){
					var i = 1;
			    	var t = $('#classesTable').DataTable();
			    	t.clear().draw();
			    	for(v in result)
			    	{
				    	var deleteRow = "<td><button value='"+result[v].classId+"' class='btn btn-danger delete'>Delete</button></td>";
				    	var r = [i,result[v].programDTO.programTitle,result[v].partDTO.part,result[v].semesterDTO.type+" OF "+result[v].semesterDTO.year,deleteRow];
				    	t.row.add(r).draw();
				    	i++;
			    	}	
				}
			});
	}
	
	//fill department combo
	function getDepartments(){
		$.ajax({
			url:"../DepartmentServlet",
			type:"POST",
			data:{
				method:"getDepartment",
			},
			success:function(data){
				if(data.length>=2){
					for(x in data){
						$("#department").append("<option value="+data[x].departmentId+">"+data[x].departmentName+"</option>");
					}
					
				}else{
					$("#department").append("<option value="+data.departmentId+">"+data.departmentName+"</option>");
					$("#department").val(data.departmentId);
					$("#departmentId").val(data.departmentId);
					fillSemesterCombo();
					//$("select option[value="+data.departmentId+"]").attr("selected","selected");
					$("#department").css('pointer-events','none');
				}
				
			}
		});
	}
	function fillAllSemesters()
	{
		$.ajax({
			url:"../SemesterServlet",
			type:"Post",
			data:{
			   method:"viewSemesterData"	
			},
			success:function(a){
				$('#semester').children('option:not(:first)').remove();
				var v = "";
				for(x in a){
					v+="<option value='"+a[x].semesterId+"'>"+a[x].type+" Of "+a[x].year+"</option>";
				}
				$('#semester').append(v);
			}
		});
	}
	
	$('#add-class').click(function()
	{
		event.preventDefault();
		var department = $('#department').val();
		var semesterId = $('#semester').val();
		if(department=="none")
		{
			alert("Please select department to add record.");
		}else if(semesterId=="none" || semesterId==undefined || semesterId=="")
		{
			alert("Please select semester to add record.");
		}else{
			window.location = "new-class.jsp?departmentId="+department+"&semesterId="+semesterId;
		}
	});

	$(document).on("click",".delete", function(e){
		var deleteId = e.target.value;
		if(deleteId==undefined || deleteId=="")
		{
			alert("Can not delete record.");
		}else{
			$.ajax({
				url:"../ClassesServlet",
				type:"POST",
				data:{
					classId:deleteId,
					method:"deleteClass",
				},
				success:function(result){
					if(result>0)
					{
						alert("Record has been deleted.");
				    	clearFields();
					}else{
						alert("Could not delete record.");
					}
				}
			});
		}
	});
	
	function clearFields()
	{
		var t = $('#classesTable').DataTable();
    	t.clear().draw();
    	$('#semester').prop("selectedIndex",0);
    	$('#department').prop("selectedIndex",0);
	}

});