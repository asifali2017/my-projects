$(document).ready(function (){

	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}


	var classId = getParameterByName("classId");
	var courseAssignmentId = getParameterByName("id");
	if(courseAssignmentId!=null){
	  
		$("#assigncourse").hide();
		$("#update").show();
	}
	getAllCourses(6);
	//getAllTeachers(6);

	////////////Fill All Courses\\\\\\\\\\\\\\\\\\\\
	function getAllCourses(department){
		var i = 1;
		$.ajax({
			url:"../CourseServlet",
			type:"POST",
			data:{
				department:department,
				method:"getAllRecords",
			},
			success:function(a){
				for(x in a){
	    		$("#course").append("<option value="+a[x].courseId+">"+a[x].courseTitle+","+a[x].courseCode+"</option>");
				}
				} 
			
		});
	}
	////////////////Fill All Instructors\\\\\\\\\\\\\\\\

		function getAllTeachers(department){
			var i = 1;
			$.ajax({
				url:"../TeacherServlet",
				type:"POST",
				data:{
					dId:department,
					method:"getAllTeachers",
				},
				success:function(result){
		    		
					for(x in result){
		    		$("#instructor").append("<option value="+result[x].teacherId+">"+result[x].firstName+" "+result[x].lastName+"</option>");
					}
				} 
				
			});
		}

	//////////////Assign Course\\\\\\\\\\\\\\\\\\\\\\\\\\\
	     $("#assigncourse").click(function (){
	    	 var course = $('#course').val();
	 		var teacher = $('#instructor').val();
	 		if(course=="none")
	 		{
	 			$('#course').notify("Select course to add");
	 		}else if(teacher=="none")
	 		{
	 			$('#instructor').notify("Select instrutor to add");
	 		}else{
	     	$.ajax({
	     	url:"../CourseAssignmentServlet",
	     	type:"post",
	     	data:{
	     		method:"assignCourses",
	     		classId:classId,
	     		courseId:$("#course").val(),
	     		teacherId:$("#instructor").val(),
	     		
	     	},
	     	success:function(a){
	     		if(a=="success"){
	     			
	     			alertify.success('Course Assigned .');
	     			window.location = "courseassignment.jsp";
	     		}else{
	     			
	     			alertify.success('Course Not Assigned .');
	     		}
	     		
	     		
	     	}
	     	
	     	});
	     	
	 		}
	     	
	     });
	     
	//........................updateassigncourses...........\\\\\\\\\\\\\\\\\\\\\\\\\\    
	     $("#update").click(function (){
	    	 var course = $('#course').val();
		 		var teacher = $('#instructor').val();
		 		if(course=="none")
		 		{
		 			$('#course').notify("Select course to add");
		 		}else if(teacher=="none")
		 		{
		 			$('#instructor').notify("Select instrutor to add");
		 		}else{
	    	 $.ajax({
	    	     	url:"../CourseAssignmentServlet",
	    	     	type:"post",
	    	     	data:{
	    	     		method:"updateCourseAssignment",
	    	     		courseId:$("#course").val(),
	    	     		teacherId:$("#instructor").val(),
	    	     		courseAssingmentId:courseAssignmentId
	    	     	},
	    	     	success:function(a){
	    	     		if(a=="success"){
	    	     			
	    	     			alertify.success('Course Updated .');
	    	     			 window.location = "courseassignment.jsp";
	    	     		}else{
	    	     			
	    	     			alertify.success('Course Not Updated .');
	    	     		}
	    	     		
	    	     		
	    	     	}
	    	 
	    	     	});
	    	     	
		 		}
	    	 
	     });

	
	
});