$(document)
		.ready(
				function() {
					alertify.set('notifier', 'position', 'bottom-left');
					$('#year-table').DataTable();
					getAllRecords();
					
					// add year
					$("#btn")
							.click(
									function() {
										if ($("#year-name").val() == ""
												|| $("#year-name").val() == null) {
											$("#year-name")
													.notify(
															"Please fill out this field.",
															{
																position : "right"
															});
											alertify.error("fill");
										} else {
											if ($("#btn").val() == "Add Year") {
												$
														.ajax({
															url : "../YearServlet",
															type : "post",
															data : {
																yearName : $(
																		"#year-name")
																		.val(),
																method : "addRecord",
															},
															success : function(
																	row) {
																if (row > 0) {
																	$(
																			"#year-name")
																			.val(
																					"");
																	getAllRecords();
																	alertify
																			.success('Record Added');
																} else {
																	$(
																			"#year-name")
																			.val(
																					"");
																	getAllRecords();
																	alertify
																			.error('Record Not Added');
																}
															}

														});
											}

											// update year
											else if ($("#btn").val() == "Update Year") {
												$
														.ajax({
															url : "../YearServlet",
															type : "post",
															data : {
																yearId : $(
																		"#year-id")
																		.val(),
																yearName : $(
																		"#year-name")
																		.val(),
																method : "updateRecord",
															},
															success : function(
																	row) {
																if (row > 0) {
																	$(
																			"#year-name")
																			.val(
																					"");
																	$("#btn")
																			.val(
																					"Add Year");
																	getAllRecords();
																	alertify
																			.success("Record Updated");
																} else {
																	$(
																			"#year-name")
																			.val(
																					"");
																	$("#btn")
																			.val(
																					"Add Year");
																	getAllRecords();
																	alertify
																			.error("Record Not Updated");
																}

															}
														});
											}
										}
									});

					// get list of faculties
					function getAllRecords() {
						$
								.ajax({
									url : "../YearServlet",
									type : "post",
									data : {
										method : "getAllRecords",
									},
									success : function(data) {
										var object = JSON.parse(data);
										var i = 1;
										var t = $('#year-table').DataTable();
										t.clear().draw();
										for (x in object) {

											var updateRow = "<td><Button class='btn btn-primary edit' value='"
													+ object[x].yearId
													+ "' id='updatebtn'>Update</Button></td>";
											var deleteRow = "<td><Button class='btn btn-danger delete' value='"
													+ object[x].yearId
													+ "' id='deletebtn'>Delete</Button></tr>";
											var r = [ i, object[x].yearName,
													updateRow, deleteRow ];
											t.row.add(r).draw();
											i++;
										}
									}
								});
					}

					// get year by year id
					$(document).on("click", ".edit", function(e) {
						$.ajax({
							url : "../YearServlet",
							type : "post",
							data : {
								editId : $(this).val(),
								method : "getRecordById",
							},
							success : function(data) {
								var object = JSON.parse(data);
								$("#year-id").val(object.yearId);
								$("#year-name").val(object.yearName);
								$("#btn").val("Update Year");
							}
						});
					});

					// delete year
					$(document).on("click", ".delete", function(e) {
						$.ajax({
							url : "../YearServlet",
							type : "post",
							data : {
								deleteId : $(this).val(),
								method : "deleteRecord",
							},
							success : function(row) {
								if (row > 0) {
									getAllRecords();
									alertify.success("Record Deleted");
								} else {
									getAllRecords();
									alertify.error("Record Not Deleted");
								}

							}
						});
					});
				});