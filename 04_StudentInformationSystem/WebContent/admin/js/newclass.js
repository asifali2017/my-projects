$(document).ready(function()
{
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};
	
	var departmentId = getUrlParameter('departmentId');
	var semesterId = getUrlParameter('semesterId');
	$('#department-id').val(departmentId);
	$('#semester-id').val(semesterId);
	
	fillProgramCombo();
	function fillProgramCombo()
	{
		$.ajax({
			url:"../ProgramServlet",
			type:"post",
			data:{
			   method:"getAllRecords"	
			},
			success:function(a){
				$('#program').children('option:not(:first)').remove();
				var v = "";
				for(x in a){
					v+="<option value='"+a[x].programId+"'>"+a[x].programTitle+"</option>";
				}
				$('#program').append(v);
			}
		});
	}
	$('#add-new-class').click(function()
	{
		event.preventDefault();
		var programId = $('#program').val();
		var part = $('#part').val();
		if(programId=="" || programId=="none" || programId==undefined)
		{
			alert("Please select a program to add record.");
		}else if(part=="" || part=="none" || part==undefined)
		{
			alert("Please select part to add record.");
		}else if(departmentId=="" || departmentId==undefined)
		{
			alert("No department selected.");
		}else if(semesterId=="" || semesterId==undefined)
		{
			alert("No semester selected.");
		}else{
			$.ajax(
			{
				url : "../ClassesServlet",
				type : "POST",
				data : {
					method : "addClass",
					programId : programId,
					part : part,
					departmentId : departmentId,
					semesterId : semesterId
				},
				success : function(result)
				{
					if(result>0)
					{
						alert("Record has been added successfully.");
						window.location="classes.jsp";
					}else if(result=="error"){
						alert("Record may already exist.");
						window.location="classes.jsp";
					}else{
						alert("Could not add record,try again.");
						window.location="classes.jsp";
					}
				}
			});
		}
	});
});