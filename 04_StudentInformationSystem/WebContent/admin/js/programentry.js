$(document).ready(function(){
	alertify.set('notifier','position', 'bottom-left');
	getAllDurations();
	getAllRecords();
	var programCodes=[];
	var programTitles=[];
	
	
	var okToSubmit=false;
	$("#add-program").click(function(e){
		if(checkValidationForAdd()==false){
			e.preventDefault();
		}else{
			$("#programentry-form").submit();
		}
	});
	$("#update-program").click(function(e){
		if(checkValidationForUpdate()==false){
			e.preventDefault();
		}else{
			$("#programentry-form").submit();
		}
	});
	
	
	 function checkValidationForAdd(){
		 if($("#program-title").val()==""){
			 $("#program-title").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#program-title").val().trim()==""){
			 $("#program-title").notify("Please enter valid title.",{position:"right"});
			 okToSubmit=false;
		 }else if(checkProgramTitle($("#program-title").val())==true){
			$("#program-title").notify("Already exist.",{position:"right"});
		 }else if($("#program-code").val()==""){
			 $("#program-code").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#program-code").val().trim()==""){
			 $("#program-code").notify("Please enter valid code.",{position:"right"});
			 okToSubmit=false;
		 }else if(checkProgramCode($("#program-code").val())==true){
			$("#program-code").notify("Already exist.",{position:"right"});
		 }else if($("#duration").prop('selectedIndex')==0){
			 $("#duration").notify("Select duration.",{position:"right"});
		 }else{
			okToSubmit=true;
		 }
		 return okToSubmit;
	 }
	 function checkValidationForUpdate(){
		 if($("#program-title").val()==""){
			 $("#program-title").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#program-title").val().trim()==""){
			 $("#program-title").notify("Please enter valid title.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#program-code").val()==""){
			 $("#program-code").notify("Please fill out this field.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#program-code").val().trim()==""){
			 $("#program-code").notify("Please enter valid code.",{position:"right"});
			 okToSubmit=false;
		 }else if($("#duration").prop('selectedIndex')==0){
			 $("#duration").notify("Select duration.",{position:"right"});
		 }else{
			okToSubmit=true;
		 }
		 return okToSubmit;
	 }
	 
	 function checkProgramCode(){
		if(programCodes.includes($("#program-code").val().toLowerCase())==true){
			return true;
		}else{
			return false;
		}
	 }
	 
	 function checkProgramTitle(){
		if(programTitles.includes($("#program-title").val().toLowerCase())==true){
			return true;
		}else{
			return false;
		}
	 }
	 
	 
	 function getAllRecords(){
			$.ajax({
				url:"../ProgramServlet",
				type:"post",
				data:{
					method:"getAllRecords",
				},
				success:function(object){
					for (x in object){
						programCodes.push(object[x].programCode.toLowerCase());
						programTitles.push(object[x].programTitle.toLowerCase());
					}
				}
			});
	 }
	// Fill duration combo
	function getAllDurations(){
		$.ajax({
			url:"../DurationServlet",
			type:"post",
			data:{
				method:"getAllRecords",
			},
			success:function(data){
				var object=JSON.parse(data);
				for(x in object){
					$("#duration").append("<option value="+object[x].durationId+">"+object[x].days+"</option>");
				}
			}
		});
	}
	
	$("#clear").click(function(){
		clearFields();
	});
	
	//Get URl parameter
	function getUrlParameter(name) {
	    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	    var results = regex.exec(location.search);
	    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
	}
	
	function clearFields(){
		$("#program-title").val("");
		$("#program-code").val("");
		$("#duration").val("none");
	}
	
	//edit
	if(getUrlParameter("updateId")!=""){
		var updateId=getUrlParameter("updateId");	
		$.ajax({
			url:"../ProgramServlet",
			type:"post",
			data:{
				editId:updateId,
				method:"getRecordById",
			},
			success:function(object){
				$("#program-id").val(object.programId);
				$("#program-title").val(object.programTitle);
				$("#program-code").val(object.programCode);
				$("#duration").val(object.durationId);
				$("#add-program").hide();
				$("#update-program").show();
			}
		});
	}
	
	if(getUrlParameter("department")!=""){
		var departmentId=getUrlParameter("department");
		$("#department-id").val(departmentId);
		console.log(departmentId);
	}
});