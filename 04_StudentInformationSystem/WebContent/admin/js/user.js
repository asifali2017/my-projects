$(document).ready(function (){
	 var faculty = "";
	 var department = "";
	 var usertype = "";
	 var id="${param.id}";

	 	$('select[name="combo"]').change(function() {
	   
		faculty = $("#faculty").val();
		department = $("#department").val();
		usertype = $("#usertype").val();
		viewUserTable();
	});
	 	//........................fill user type combo.....................//
	$.ajax({
		url:"../UserServlet",
		type:"post",
		data:{
		   method:"getAllUserTypes"	
		},
		success:function(a){
			
			for(x in a){
			$("#usertype").append("<option>"+a[x].userType+"</option>");
			}
		}
		
	});
 	
	//........................fill department combo.....................//
	$.ajax({
		url:"../DepartmentServlet",
		type:"post",
		data:{
		   method:"getAllDepartments"	
		},
		success:function(a){
			
			for(x in a){
			$("#department").append("<option>"+a[x].departmentName+"</option>");
			}
		}
		
	});
 	//........................fill faculty  combo.....................//

	$.ajax({
		url:"../FacultyServlet",
		type:"post",
		data:{
		   method:"getAllRecords"	
		},
		success:function(a){
			for(x in a){
			$("#faculty").append("<option>"+a[x].facultyName+"</option>");
			}
		}
		
	});
 	//........................delete users .....................//

	$(document).on('click','#delete',function(){
		   var userid = $(this).val();
		   
			$.ajax({
				url:"../UserServlet",
	  		type:"post",
	  		data:{
	  			id:userid,
	  			method:"deleteUser"
	  		},
	  		success:function(a){
	  		
	  			if(a=="success")
	  			{
	  				viewUserTable();
		    			alertify.success('Record deleted');
	  			}else{
		    			alertify.error('Could not delete.');
	  			}
	  			
	  		}
				
			});
	});	
 
	//........................ view users .....................//

	viewUserTable();
	function viewUserTable(){
	  	var i = 1;
	  	
	  	$.ajax({
	  	 url:"../UserServlet",
	  	 type:"post",
	  	 data:{
	  		 method:"getAllUsers",
	  		 faculty:$("#faculty").val(),
	  		 department: $("#department").val(),
	  		 usertype:$("#usertype").val()
	  	 },
	  	 success:function(a){
	  		 
	  		 
	  		  var t = $('#userTable').DataTable();
	  		 t.clear().draw();
	  		 for(x in a){
	  	    
	  		 var updateRow = "<td><a class='btn btn-primary' href='adduser.jsp?id="+a[x].userId+"' '>Update</a></td>";
	  		 var deleteRow = "<td><Button class='btn btn-danger' value="+a[x].userId+" id='delete'>Delete</Button></tr>";
	  	     var r = [i,a[x].userName,a[x].departmentCode,a[x].accountType,updateRow,deleteRow];
	  	     t.row.add(r).draw();
	  	     i++;
	  		 } 		    		 
	  	  }
	  	
	  	});
	  	
	  }
  
	
	
});

