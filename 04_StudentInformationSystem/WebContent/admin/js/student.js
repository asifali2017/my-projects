$("document").ready(function(){
	$("#studentTable").DataTable();
	getDepartments();
	
	// department combo
	var departmentId="";
	$("select#department").change(function() {
		departmentId=this.value;
        if(departmentId!="none"){
        	fillSemesterCombo();
        }
        else{
        	$('#semster').children('option:not(:first)').remove();
        }
	});
	//  validation
	
	var validate=true;
	function checkValidation(){
		var firstName = $("#fname").val();
		var middleName = $("#mname").val();
		var lastName = $("#lname").val();
		var rollNum =$("#rollnum").val();
		//Check input Fields Should not be blanks.
		if (firstName == '') {
			$("#fname").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else if (middleName == '') {
			$("#mname").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else if (lastName == '') {
			$("#lname").notify("Please fill out this field.",{position:"right"});
			validate=false;		
		}
		else if (rollNum == '') {
			$("#rollnum").notify("Please fill out this field.",{position:"right"});
			validate=false;	
		}
		else{
			validate=true;
		}
		return validate;
	}
	// url parameters get
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	// update click
	var id= getParameterByName('studentId');
	var urlClassId=getParameterByName('classId');
	var add=getParameterByName('add');
	var update=getParameterByName('update');
	var addDepartmentId=getParameterByName('departmentId');
	var updateDepartmentId=getParameterByName('department');
	if(id!=null){
		updateClick();
	}
	// alertify
	if(add=="success"){
		alertify.success('Student Added');
	}
	if(update=="success"){
		alertify.success('Student Deleted');
	}
	//fill department combo
	function getDepartments(){
		$.ajax({
			url:"../DepartmentServlet",
			type:"POST",
			data:{
				method:"getDepartment",
			},
			success:function(data){
				if(data.length>=2){
					for(x in data){
						$("#department").append("<option value="+data[x].departmentId+">"+data[x].departmentName+"</option>");
					}
					
				}else{
					$("#department").append("<option value="+data.departmentId+">"+data.departmentName+"</option>");
					$("#department").val(data.departmentId);
					$("#departmentId").val(data.departmentId);
					fillSemesterCombo();
					//$("select option[value="+data.departmentId+"]").attr("selected","selected");
					$("#department").css('pointer-events','none');
				}
				
			}
		});
	}
	// semster combo on change
	$("select#semster").change(function() {
		var selectedSemster = $("#semster option:selected").val();
        if(selectedSemster==""){
        	$('#class').children('option:not(:first)').remove();
        	var table = $('#studentTable').DataTable();
			table.clear().draw();
        }
        else{
        	fillClassCombo(selectedSemster);
        }       
	});
	// class combo on change
	var selectedclass="";
	$("select#class").change(function() {
		selectedclass = $("#class option:selected").val();
		if(selectedclass==""){
			var table = $('#studentTable').DataTable();
			table.clear().draw();
		}
		else{
			getAllStudents(selectedclass);
		}      
	});
	
	//get list of semster
	function fillSemesterCombo(){
		$.ajax({
			url:"../StudentServlet",
			type:"POST",
			data:{
				method:"getAllSemsters",
			},
			success:function(result){
				for(x in result){
					var value=result[x].type+" of "+result[x].year;
					$("#semster").append("<option value='"+result[x].semesterId+"'>"+value+"</option>");
				}
			},
			error:function (error) {
			    alert('error sems : ' + eval(error));
			}
			
		});
	}
	
	
	//get list of clases
	function fillClassCombo(semster){
		$.ajax({
			url:"../ClassesServlet",
			type:"POST",
			data:{
				semsterId:semster,
				method:"getClasessBySemester",
			},
			success:function(result){
				$('#class').children('option:not(:first)').remove();
				for(x in result){
					var value=result[x].programDTO.programCode+" Part : "+result[x].partDTO.part
					$("#class").append("<option value='"+result[x].classId+"'>"+value+"</option>");
				}
			},
			error:function (error) {
			    alert('error class ' + eval(error));
			}
			
		});
		
	}
	
	// get all students
	function getAllStudents(selectedClass){
		var i = 1;
		$.ajax({
			url:"../StudentServlet",
			type:"POST",
			data:{
				classId:selectedClass,
				method:"getAllStudents",
			},
			success:function(result){
	    		 var table = $('#studentTable').DataTable();
	    		 table.clear().draw();
	    		 for(x in result){
	    		 var updateRow = "<td><a href='./addstudent.jsp?studentId="+result[x].studentId+"&department="+departmentId+"'><Button value='"+result[x].studentId+"' id='updatebtn' class='btn btn-primary'>Update</Button></a></td>";
	    		 var deleteRow = "<td><Button value='"+result[x].studentId+"' id='deletebtn' class='btn btn-danger'>Delete</Button></td>";
	    	     var r = [i,result[x].firstName,result[x].middleName,result[x].lastName,result[x].rollNumber,updateRow,deleteRow];
	    	     table.row.add(r).draw();
	    	     i++;
	    		 }
			} 
			
		});
	}
	// add student
	$("#addstudent").click(function(){
		if(checkValidation()==true){
			if($("#addstudent").val()=="Add Student"){
				$.ajax({
					url:"../StudentServlet",
					type:"POST",
					data:{
						firstName:$("#fname").val(),
						middleName:$("#mname").val(),
						lastName:$("#lname").val(),
						rollNumber:$("#rollnum").val(),
						classId:urlClassId,
						method:"addStudent",	
					},
					success:function(result){
						if(result=="rollNumber exists"){  
							$("#rollnum").notify("Roll Number already exits try another.",{position:"right"});
						}else{
							clearFields();
						    window.location = "../admin/student.jsp?add=success";
						}
					}	
				});
			}
			// update teacher
			else if($("#addstudent").val()=="Update Student"){
				$.ajax({
					url:"../StudentServlet",
					type:"POST",
					data:{
						studentId:$("#addstudent").text(),
						firstName:$("#fname").val(),
						middleName:$("#mname").val(),
						lastName:$("#lname").val(),
						rollNumber:$("#rollnum").val(),
						method:"updateStudent",	
					},
					success:function(result){
						if(result=="rollNumber exists"){  
							$("#rollnum").notify("Roll Number already exits try another.",{position:"right"});
						}
						else{
							clearFields();
							$("#addstudent").val("Add Student");
							window.location = "../admin/student.jsp?update=success";
						}
					}	
				});
			}
			
		}
		else{
			checkValidation();
		}
		
	});
	// get student Model
	function updateClick(){
		$.ajax({
			url:"../StudentServlet",
			type:"POST",
			data:{
				studentId:id,
				method:"getStudentModelById",
			},
			success:function(data){
				$("#fname").val(data.firstName);
				$("#mname").val(data.middleName);
				$("#lname").val(data.lastName);
				$("#rollnum").val(data.rollNumber);
				$("#addstudent").val("Update Student");
				$("#addstudent").html(data.studentId);		
			}
		});				
	}
	// add delete teacher
	$(document).on("click","#deletebtn", function(e){
		$.ajax({
			url:"../StudentServlet",
			type:"POST",
			data:{
				studentId:e.target.value,
				method:"deleteStudent",
			},
			success:function(data){
				alertify.success('Record deleted');
			    getAllStudents(selectedclass);
			}
		});
	});
	// clear fields
	$("#clear").click(function (){  
 	   clearFields();
	});
	function clearFields(){
		$("#fname").val("");
		$("#mname").val("");
		$("#lname").val("");
		$("#rollnum").val("");
	}
	
});