<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
			<jsp:include page="header.jsp" />
				<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">

									<div class="form-group">
										<label for="year">Year<span style="color: red;">*<span></label>
										<select class="form-control" id="year" required placeholder="Select Year">
											<option value='none'>Select Year</option>
										</select>
									</div>
									<div class="form-group">
										<label class="radio-inline"><input type="radio"
											name="optradio" value="First">First</label> <label class="radio-inline"><input
											type="radio" value="Second" name="optradio">Second</label>
									</div>
									<div class="form-group">
										<label for="startdate">Start Date:</label> <input type="text"
											class="form-control" id="startdate"  placeholder="01/05/2018">
									</div>
									<div class="form-group">
										<label for="enddate">End Date:</label> <input type="text"
											class="form-control" id="enddate" placeholder="01/05/2018">
									</div>
									<div class="form-group">
										<input type="button" id="addsemester" value="Add Semester"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>
									<div class="form-group">
										<input style="display:none;background-color: #00e58b; color: white" type="button" id="update" value="Update"
											class="btn btn-primary form-control" >
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
									<input type="hidden" id="semesterid">


									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					<div class="container" style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="cityTable" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>Type</th>
											<th>Year</th>
											<th>Start Date</th>
											<th>End Date</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='cityTableBody'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>Type</th>
											<th>Year</th>
											<th>Start Date</th>
											<th>End Date</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="footer.jsp" />
			
				<script type="text/javascript" src="js/semester.js"></script>

</body>
</html>