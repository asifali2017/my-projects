<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
			<jsp:include page="header.jsp" />
				<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">
								<input type="hidden" id="faculty-id"/>
									<div class="form-group">
										<label for="startdate">Faculty Name:</label> 
										<input type="text" id="faculty-name" class="form-control" placeholder="Enter faculty Name">
									</div>
									<div class="form-group">
										<input type="button" id="btn" value="Add Faculty"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>
									<div class="form-group">
										<input style="display:none;background-color: #00e58b; color: white" type="button" id="btn" value="Update"
											class="btn btn-primary form-control" >
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="facultyTable" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>Faculty Name</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='facultyTableBody'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>Faculty Name</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/faculty.js"></script>
</body>
</html>