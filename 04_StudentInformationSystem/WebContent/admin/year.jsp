<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
			<jsp:include page="header.jsp" />
				<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">
								<input type="hidden" id="year-id"/>
									<div class="form-group">
										<label for="startdate">Year Name:</label> 
										<input type="text" id="year-name" class="form-control" minlength="4"  pattern="[0-9]+" placeholder="Enter year">
									</div>
									<div class="form-group">
										<input type="button" id="btn" value="Add Year"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>
									<div class="form-group">
										<input style="display:none;background-color: #00e58b; color: white" type="button" id="btn" value="Update"
											class="btn btn-primary form-control" >
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="year-table" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>Year Name</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='facultyTableBody'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>Year Name</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/year.js"></script>
</body>
</html>