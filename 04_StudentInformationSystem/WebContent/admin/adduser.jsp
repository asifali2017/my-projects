<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<jsp:include page="header.jsp"/>
<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">
                                    <div class="form-group">
										<label for="usertype" id="usertypelabel">User Type:</label>
										<select class="form-control" id="usertype"  required >
											<option value='none'>Select User Type</option>
										</select>
									</div>
									<div class="form-group">
										<label for="faculty" id="facultylabel">Faculty:</label>
										<select class="form-control" id="faculty" name="combo" required >
											<option value='none'>Select Faculty</option>
										</select>
									</div>
									<div class="form-group">
										<label for="department" id="departmentlabel">Department:</label>
										<select class="form-control" id="department"  required >
											<option value='none'>Select Department</option>
										</select>
									</div>
									<div class="form-group">
                                      <label for="usr">User Name:</label>
                                      <input type="text" class="form-control" id="user">
                                    </div>
                                    <div class="form-group">
                                      <label for="pwd">Password:</label>
                                       <input type="password" class="form-control" id="pass">
                                      </div>
									 <div class="form-group">
                                      <label for="cnfpass" id="confirmlabel">Confirm Password:</label>
                                       <input type="password" class="form-control" id="cnfpass">
                                      </div>
                                      	<div class="form-group">
										<input style="background-color: #00e58b; color: white" type="button" id="addbutton" value="Add "
											class="btn btn-primary form-control" >
									</div>
									
								          	<div class="form-group">
										<input style="background-color: #00e58b; color: white;display:none;" type="button" id="updatebutton" value="Update"
											class="btn btn-primary form-control" >
									</div>
								    <input type="hidden" id="userid">
									
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>

				</div>

<jsp:include page="footer.jsp"/>
<script>
var id="${param.id}";

$.ajax({
	url:"../UserServlet",
	type:"post",
	data:{
	   method:"getAllUserTypes"	
	},
	success:function(a){
		
		for(x in a){
		$("#usertype").append("<option id='utvalue' value="+a[x].userId+">"+a[x].userType+"</option>");
		}
	}
	
});

$.ajax({
	url:"../FacultyServlet",
	type:"post",
	data:{
	   method:"getAllRecords"	
	},
	success:function(a){
		for(x in a){
		$("#faculty").append("<option id='facultyvalue' value="+a[x].facultyId+">"+a[x].facultyName+"</option>");
		}
	}
	
});
$('select[name="combo"]').change(function() {

	var faculty = $("#faculty").val();
$.ajax({
	url:"../UserServlet",
	type:"post",
	data:{
	   method:"getAllDepartmentsWithFaculty",
	   faculty:faculty
	   
	},
	success:function(a){
		
		$("#department").empty();
		for(x in a){
			
		$("#department").append("<option id='deptvalue' value="+a[x].departmentId+" >"+a[x].departmentName+"</option>");
		}
	}
	
});
});
$("#cnfpass").keyup(function (){
	var pass = $("#pass").val();
	var cnfpass = $("#cnfpass").val();
	if(pass == cnfpass){
		$("#cnfpass").notify("Password matched",{ position:"right" , type:"info"});
	}else{
		
		$("#cnfpass").notify("Password not match",{ position:"right" , type:"error" });
	}
	
});
$("#addbutton").click(function (){
    if($("#usertype").prop('selectedIndex')==0){
    	$("#usertype").notify("Select user type",{position:"right"});
    }else if($("#faculty").prop('selectedIndex')==0){
      	$("#faculty").notify("Select faculty",{position:"right"});
    }else if($("#user").val()==""){
    	$("#user").notify("Enter Username",{position:"right"});
    }else if($("#pass").val()==""&&("#cnfpass").val()==""||$("#pass").val()!=$("#cnfpass").val()){
    	$("#cnfpass").notify("Password Matched","success");
    }else{
	$.ajax({
	url:"../UserServlet",
	method:"post",
	data:{
		method : "addUsers",
        usertype:$("#utvalue").val(),
        faculty:$("#facultyvalue").val(),
        department:$("#deptvalue").val(),
        username:$("#user").val(),
        pass:$("#pass").val()
		
		
	},
	success:function(a){
	if(a=='success'){
		window.location = "user.jsp";	
	}else if(a=='exists'){
		alertify.error('Username already exists!.');
			
	}else{
		alertify.error('Could not add.')
	}
    		
    		
	}
	
	});
    }
});
if(id!=0){

	$.ajax({
		url:"../UserServlet",
		method:"post",
		data:{
			userid:id,
			method:"getUserById"
		},
		success:function(a){
		
			$("#usertype").hide()
			$("#usertypelabel").hide()
			
			$("#faculty").hide()
			$("#facultylabel").hide()
			
			$("#department").hide()
			$("#departmentlabel").hide()
			
			$("#cnfpass").hide()
			$("#confirmlabel").hide()
			
		    $("#user").val(a.userName)
			$("#userid").val(a.userId)
		    
		    $("#addbutton").hide();
			$("#updatebutton").show();
		}
		
	});
	$("#updatebutton").click(function (){
		$.ajax({
			url:"../UserServlet",
			method:"post",
			data:{
				method:"updateUserData",
				userid:$("#userid").val(),
				username:$("#user").val(),
				password:$("#pass").val()
				
			},
			success:function(a){
				
				alertify.success('Updated Successfully.');
					
			}
			
		});
		
		
	});
	
	
}	

</script>
</body>
</html>