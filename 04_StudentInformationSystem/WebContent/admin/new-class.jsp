
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Class</title>
</head>
<body>
<jsp:include page="header.jsp"/>
	<!--inner block end here-->
			<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">
									<div class="form-group">
										<label for="program">Program:</label>
										<select class="form-control" id="program" name="combo" required >
											<option value='none'>Select Program</option>
										</select>
									</div>
									<div class="form-group">
										<label for="part">Part:</label>
										<select class="form-control" id="part" name="combo" required >
											<option value='none'>Select Part</option>
											<option value='1'>1</option>
											<option value='2'>2</option>
											<option value='3'>3</option>
											<option value='4'>4</option>
											<option value='5'>5</option>
										</select>
									</div>
									<div class="form-group">
										<button  class="btn btn-primary form-control"  style="background-color: #00e58b; color: white" id="add-new-class">Add Class</button>
									</div>
								</form>
							</div>
							<div id="semester-id" style="display:none"></div>
							<div id="department-id" style="display:none"></div>
							<div class="col-sm-3">
							</div>
						</div>
					</div>
				</div>
			<jsp:include page="footer.jsp"></jsp:include>
			<script src="js/newclass.js"></script>
</body>
</html>