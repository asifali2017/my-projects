<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
			<jsp:include page="header.jsp" />
				<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form method="get" action="programentry.jsp" id="upload-form">

									<div class="form-group">
										<label for="department">Department<span style="color: red;">*<span></label>
										<select class="form-control" name="department" id="department" required placeholder="Select Department">
											<option value='none'>Select Department</option>
										</select>
									</div>
									<div class="form-group">
										<input type="submit" id="add-program" value="Add Program"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>
									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="program-table" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>Program Title</th>
											<th>Code</th>
											<th>Duration Of Semester</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='program-table-body'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>Program Title</th>
											<th>Code</th>
											<th>Duration Of Semester</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="footer.jsp" />
				<script type="text/javascript" src="js/programs.js"></script>

</body>
</html>