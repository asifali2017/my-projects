<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Department</title>
</head>
<body>
		<jsp:include page="header.jsp" />
		<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="upload-form">

									<div class="form-group">
									 	<label for="name">Name:</label>
											<input type="text" name="dname" id="dname" placeholder="Enter Department Name" class="form-control" required/><br>
									</div>	
									<div class="form-group">
											<label for="code">Department Code:</label>
											<input type="text" name="dcode" id="dcode" placeholder="Enter Department Code" class="form-control" required/><br>
									</div>
									<div class="form-group">
										<label for="code">Faculty:</label>
										<select class="form-control" id="faculty" name="faculty">
										<option>Select Faculty</option>
										</select>
									</div>
									<div class="form-group">
										<input class="btn btn-primary form-control"  style="background-color: #00e58b; color: white" type="button" value="Add Department" name="adddepartment" id="adddepartment" class="btn btn-default form-control btn-success"/>
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
									<input type="text" id="facultyField" hidden>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="departmentTable" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
											<th>S_No</th>
											<th>Department Name</th>
											<th>Department Code</th>
											<th>Faculty Name</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody id='departmentTableBody'>

									</tbody>
									<tfoot>
										<tr>
											<th>S_No</th>
											<th>Department Name</th>
											<th>Department Code</th>
											<th>Faculty Name</th>
											<th>Update</th>
											<th>Delete</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="footer.jsp"></jsp:include>
				<script type="text/javascript" src="js/department.js"></script>
</body>
</html>