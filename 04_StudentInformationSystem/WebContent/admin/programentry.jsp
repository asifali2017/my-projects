<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
			<jsp:include page="header.jsp" />
				<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
								<form id="programentry-form" method="post" action="../ProgramServlet">
									<div class="form-group">
										<label for="programtitle">Program Title:</label> 
										<input type="text" name="program-title" id="program-title" class="form-control" minlength="2" placeholder="Enter Program Title">
									</div>
										<label for="programcode">Program Code:</label> 
									<div class="form-group">
										<input type="text" id="program-code" name="program-code" class="form-control" minlength="2" placeholder="Enter Program Code">
									</div>
									<div class="form-group">
										<label for="duration">Duration Semester<span style="color: red;">*<span></label>
										<select name="duration" class="form-control" id="duration" required placeholder="Select Duration Semester">
											<option value='none'>Select Duration Semester</option>
										</select>
									</div>
									<div class="form-group">
										<input type="submit" id="add-program" value="Add Program"
											name="addRecord"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white">
									</div>
									<div class="form-group">
										<input style="display:none;background-color: #00e58b; color: white" type="submit" id="update-program" value="Update Program"
											name="updateRecord"
											class="btn btn-primary form-control" >
									</div>
									<div class="form-group">
										<input type="button" id="clear" value="Clear"
											class="btn btn-danger form-control">
									</div>
									<input type="hidden" id="program-id" name="program-id">
									<input type="hidden" id="department-id" name="department-id">

									<div class="alert alert-success" role="alert"
										style="display: none" id="alert">
										<strong>Success</strong>
									</div>
								</form>
							</div>
							<div class="col-sm-3"></div>
						</div>
					</div>
					
				</div>
				<jsp:include page="footer.jsp" />
				<script type="text/javascript" src="js/programentry.js"></script>

</body>
</html>