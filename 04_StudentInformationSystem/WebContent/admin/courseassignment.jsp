<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:include page="header.jsp" />
		<!--inner block end here-->
				<div class="inner-block" style="background-color: #fff;">
					<div class="container">
						<div class="row">
							<div class="col-sm-2"></div>
							<div class="jumbotron col-sm-6" style="background-color: white;">
							      <div class="form-group">
										<label for="department">Department:</label>
										<select class="form-control" id="department"  required >
											<option value='none'>Select Department</option>
										</select>
						            </div>
							        <div class="form-group">
										<label for="semester">Semester:</label>
										<select class="form-control" id="semester" name="combo" required >
											<option value='none'>Select Semester</option>
										</select>
									</div>
									<div class="form-group">
										<label for="program">Program:</label>
										<select class="form-control" id="program" name="combo" required >
											<option value='none'>Select Program</option>
										</select>
									</div>
									<div class="form-group">
										<label for="class">Classes:</label>
										<select class="form-control" id="class" name="combo" required >
											<option value='none'>Select Class</option>
										</select>
									</div>	
									<div class="form-group">
										<input type="button" id="assigncoursebtn" value="Assign Course"
											class="btn btn-primary form-control"
											style="background-color: #00e58b; color: white;display:none;">
									</div>	
							
							</div>
					    </div>
					</div>
										<div class="container"
						style="margin-bottom: 40px; margin-top: 40px">
						<div class="row">
							<div class="col-sm-11">
								<table id="courseassignmenttable" class="table table-striped table-bordered"
									style="width: 100%">
									<thead>
										<tr>
										    <th>S_No</th>
											<th>Course Title</th>
											<th>Instructor</th>
											<th>Update</th>
											<th>Delete</th>
											
										</tr>
									</thead>
									<tbody id='courseassignmenttable'>

									</tbody>
									<tfoot>
										<tr><th>S_No</th>
										    <th>Course Title</th>
											<th>Instructor</th>
											<th>Update</th>
											<th>Delete</th>
											
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					
			    </div>
			    
	<jsp:include page="footer.jsp"/>		    
</body>
<script src="js/courseassignment.js"></script>
</html>