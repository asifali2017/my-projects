package salu.sis.daoimpl;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.YearDAO;
import salu.sis.models.YearModel;

public class YearDAOImpl implements YearDAO{

	@SuppressWarnings("null")
	@Override
	public Integer addYear(YearModel yearModel) {
		int row=0;
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			
			session.save(yearModel);
			transaction.commit();
			row++;
		}catch(Exception e){
			e.printStackTrace();
			if(transaction!=null){
				transaction.rollback();
			}
		}finally{
			session.close();
		}
		return row;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public List<YearModel> getAllYears() {
		
		List<YearModel> list=new ArrayList<>();
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session =sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			list=session.createQuery("from YearModel where active=1").list();
		}catch(Exception e){
			if(transaction!=null){
				transaction.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}

	@Override
	public Integer updateYear(YearModel yearModel) {
		int row=0;
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			session.update(yearModel);
			transaction.commit();
			row=1;
		}catch(Exception e){
			if (transaction!=null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return row;
		
	}

	@SuppressWarnings("unused")
	@Override
	public YearModel getYearById(int id) {
		
		YearModel yearModel=null;
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.getTransaction();
		try{
			 yearModel=session.get(YearModel.class,id);
		}catch(Exception e){
			if (transaction!=null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return yearModel;
	}

	@Override
	public Integer deleteYear(int deleteId) {
		
		int row=0;
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		try{
			YearModel yearModel=session.get(YearModel.class,deleteId);
			yearModel.setActive((byte) 0);
			session.update(yearModel);
			transaction.commit();
			row=1;
		}catch(Exception e){
			if (transaction!=null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}finally{
			session.close();
		}
		return row;
		
		
	}

	@Override
	public YearModel getYearByName(String year) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		YearModel yearsModel = (YearModel) session.createQuery("From YearModel y where y.yearName = "+year+"").getSingleResult();
	    session.close();
	    return yearsModel;}

}
