package salu.sis.daoimpl;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.DurationDAO;
import salu.sis.models.DurationModel;

public class DurationDAOImpl implements DurationDAO{

	@Override
	public DurationModel getDurationById(Integer durationId) {
		DurationModel durationModel=new DurationModel();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			durationModel=session.get(DurationModel.class, durationId);
		}catch(Exception e){
			durationModel=null;
		}finally{
			if(durationModel!=null){
				transaction.commit();
				session.close();
			}
		}
		return durationModel;
	}

	@Override
	public List<DurationModel> getAllDurations() {
		List<DurationModel> list=new ArrayList<DurationModel>();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			list=session.createQuery("FROM DurationModel where active=1").list();
		}catch(Exception ex){
			list=null;
		}
		finally{
			if(!list.isEmpty()){
				transaction.commit();
				session.close();
			}
		}
		return list;
	}
}
