package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.DepartmentDAO;
import salu.sis.models.DepartmentModel;

@SuppressWarnings("deprecation")
public class DepartmentDAOImpl implements DepartmentDAO{
	
	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	public Integer addDepartment(DepartmentModel departmentModel) {
		Integer success=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		success=(Integer)session.save(departmentModel);
		success=1;
		transaction.commit();
		session.close();
		return success;
	}

	@Override
	public Integer deleteDepartment(DepartmentModel departmentModel) {
		Integer success=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(departmentModel);
		success=1;
		transaction.commit();
		session.close();
		return success;
	}

	@Override
	public Integer updateDepartment(DepartmentModel departmentModel) {
		Integer success=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(departmentModel);
		success=1;
		transaction.commit();
		session.close();
		return success;
	}

	@Override
	public List<DepartmentModel> getAllDepartments() {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<DepartmentModel> departmentModels=session.createQuery("From DepartmentModel d where d.active=1").list();
		transaction.commit();
		session.close();
		return departmentModels;
	}

	@Override
	public DepartmentModel getDepartmentModelById(Integer departmentId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		DepartmentModel departmentModel=session.get(DepartmentModel.class, departmentId); 
		transaction.commit();
		session.close();
		return departmentModel;
	}

	
	@Override
	public DepartmentModel getDepartmentModelByName(String departmentName) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		DepartmentModel departmentModel=(DepartmentModel)session.createQuery("From DepartmentModel d where d.departmentName='"+departmentName+"' and d.active=1").uniqueResult(); 
		transaction.commit();
		session.close();
		return departmentModel;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<DepartmentModel> getDepartmentWithFacultyId(Integer facultyId) {

 		Session session = sessionFactory.openSession();
        Query query = session.createQuery("From DepartmentModel d where d.facultyModel.facultyId =:id");	
        query.setParameter("id", facultyId);
        @SuppressWarnings("unchecked")
		List<DepartmentModel> departments = query.getResultList();
        session.close();
  	    return departments;
	}

	@SuppressWarnings({ "rawtypes"})
	@Override
	public boolean checkDepartmentCode(String departmentCode) {
		boolean found=false;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From DepartmentModel where departmentCode='"+departmentCode+"' and active=1";
		Query query=session.createQuery(hql);
		DepartmentModel departmentModel=(DepartmentModel)query.uniqueResult();
		transaction.commit();
		session.close();
		if(departmentModel!=null){
			System.out.println("departemant model "+departmentModel); 
			found=true;
		}
		return found;
	}

	@Override
	public boolean checkDepartmentName(String departmentName) {
		boolean found=false;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From DepartmentModel where departmentName='"+departmentName+"' and active=1";
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery(hql);
		DepartmentModel departmentModel=(DepartmentModel)query.uniqueResult();
		transaction.commit();
		session.close();
		if(departmentModel!=null){ 
			found=true;
		}
		return found;
	}

	@Override
	public boolean checkCode(String code, Integer id) {
		boolean found=false;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From DepartmentModel where departmentCode='"+code+"' and departmentId<>'"+id+"' and active=1";
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery(hql);
		DepartmentModel departmentModel=(DepartmentModel)query.uniqueResult();
		transaction.commit();
		session.close();
		if(departmentModel!=null){
			found=true;
		}
		return found;
	}

	@Override
	public boolean checkName(String name, Integer id) {
		boolean found=false;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From DepartmentModel where departmentName='"+name+"' and departmentId<>'"+id+"' and active=1";
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery(hql);
		DepartmentModel departmentModel=(DepartmentModel)query.uniqueResult();
		transaction.commit();
		session.close();
		if(departmentModel!=null){
			System.out.println("departemant model "+departmentModel); 
			found=true;
		}
		return found;
	}
	
	


}
