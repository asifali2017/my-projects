package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.UserDAO;
import salu.sis.models.DepartmentModel;
import salu.sis.models.FacultyModel;
import salu.sis.models.UserModel;
import salu.sis.models.UserTypeModel;

public class UserDAOImpl implements UserDAO {

	@Override
	public List<UserModel> getAllUsers(UserModel userModel) {

		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		String hql = "from UserModel where departmentModel.departmentName like :department and facultyModel.facultyName like :faculty and userTypeModel.userType like :user and active = 1 ";
		Query query = session.createQuery(hql);
		query.setParameter("department", "%" + userModel.getDepartmentModel().getDepartmentName() + "%");
		query.setParameter("faculty", "%" + userModel.getFacultyModel().getFacultyName() + "%");
		query.setParameter("user", "%" + userModel.getUserTypeModel().getUserType() + "%");
		List<UserModel> users = query.getResultList();
		return users;
	}

	@Override
	public List<UserTypeModel> getAllUsersTypes() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("From UserTypeModel");
		List<UserTypeModel> userTypes = query.getResultList();
		return userTypes;
	}

	@Override
	public int insertUserData(UserModel userModel) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		UserModel model = new UserModel();
		DepartmentModel departmentModel = session.get(DepartmentModel.class,
				userModel.getDepartmentModel().getDepartmentId());
		FacultyModel facultyModel = session.get(FacultyModel.class, userModel.getFacultyModel().getFacultyId());
		UserTypeModel userTypeModel = session.get(UserTypeModel.class, userModel.getUserTypeModel().getUserId());
		model.setUserName(userModel.getUserName());
		model.setDepartmentModel(departmentModel);
		model.setFacultyModel(facultyModel);
		model.setUserTypeModel(userTypeModel);
		model.setPassword(userModel.getPassword());
		model.setActive((byte) 1);
		int i = (int) session.save(model);
		transaction.commit();
		session.close();
		return i;
	}

	@Override
	public int updateUserData(UserModel userModel) {
		int i = 0;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		UserModel model = session.get(UserModel.class, userModel.getUserId());
		model.setPassword(userModel.getPassword());
		model.setUserName(userModel.getUserName());
		session.update(model);
		i = 1;
		transaction.commit();
		session.close();
		return i;
	}

	@Override
	public int deleteUserData(int id) {
		int i = 0;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		UserModel userModel = session.get(UserModel.class, id);
		userModel.setActive((byte) 0);
		i = 1;
		transaction.commit();
		session.close();
		return i;
	}

	@Override
	public UserModel getUserById(int userId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		UserModel userModel = session.get(UserModel.class, userId);
		session.close();
		return userModel;
	}

	@Override
	public boolean checkUser(String userName) {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.openSession();
		Criteria criteria = session.createCriteria(UserModel.class);
		criteria.add(Restrictions.eq("userName", userName));
		List result = criteria.list();
		if (result.isEmpty()) {
			session.close();
			return false;

		} else {
			session.close();
			return true;
		}

	}

	@Override
	public int setUserImage(UserModel userModel) {
		int row = 0;
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		UserModel user = session.get(UserModel.class, userModel.getUserId());
		user.setImagePath(userModel.getImagePath());
		session.update(user);
		row = 1;
		transaction.commit();
		session.close();
		return row;
	}

}
