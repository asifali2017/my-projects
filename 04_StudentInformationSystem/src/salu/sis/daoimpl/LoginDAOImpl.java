package salu.sis.daoimpl;

import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.LoginDAO;
import salu.sis.models.UserModel;

public class LoginDAOImpl implements LoginDAO {

	@Override
	public List<UserModel> login(String userName, String password) {
		UserModel userModel = new UserModel();
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(UserModel.class);
		Criterion userCriterion = Restrictions.eq("userName", userName);
		Criterion passCriterion = Restrictions.eq("password", password);
		LogicalExpression checkUser = Restrictions.and(userCriterion, passCriterion);
		criteria.add(checkUser);
		List<UserModel> users = criteria.list();
		session.close();
		return users;
	}

	@Override
	public List<UserModel> checkOldPassword(Integer userId, String oldPassword) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
	    Criteria criteria = session.createCriteria(UserModel.class);
	    Criterion userCriterion = Restrictions.eq("userId", userId);
		Criterion passCriterion = Restrictions.eq("password", oldPassword);
		LogicalExpression checkPassword = Restrictions.and(userCriterion, passCriterion);
		criteria.add(checkPassword);
		List<UserModel> users = criteria.list();
		session.close();
		return users;
	}

	@Override
	public int updateUserPassword(Integer userId, String newPassword) {
		int i = 0;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
	    Transaction transaction = session.beginTransaction();
	    UserModel userModel = session.get(UserModel.class, userId);
	    userModel.setPassword(newPassword);
	    session.update(userModel);
        i = 1;
        transaction.commit();
        session.close();
        return i;
	}

}
