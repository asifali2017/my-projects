package salu.sis.daoimpl;



import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.CourseAssignmentDAO;
import salu.sis.dto.CourseDTO;
import salu.sis.models.ClassModel;
import salu.sis.models.CourseAssignmentModel;
import salu.sis.models.CourseModel;
import salu.sis.models.TeacherModel;

public class CourseAssignmentDAOImpl implements CourseAssignmentDAO{

	@Override
	public int assignCourse(CourseAssignmentModel courseAssignmentModel) {
    
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		ClassModel classModel = session.get(ClassModel.class, courseAssignmentModel.getClassModel().getClassId());
        TeacherModel teacherModel = session.get(TeacherModel.class,courseAssignmentModel.getTeacherModel().getTeacherId());
        CourseModel courseModel = session.get(CourseModel.class, courseAssignmentModel.getCourseModel().getCourseId());
	    courseAssignmentModel.setClassModel(classModel);
	    courseAssignmentModel.setTeacherModel(teacherModel);
	    courseAssignmentModel.setCourseModel(courseModel);
	    courseAssignmentModel.setActive((byte)1);
	    int i = (int) session.save(courseAssignmentModel);
        transaction.commit();
        session.close();
        return i;
	}

	@Override
	public List<CourseAssignmentModel> getAllAssignedCoursesWithClass(Integer classId) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Criteria criteria = session.createCriteria(CourseAssignmentModel.class);
		ClassModel classModel = new ClassModel();
		classModel.setClassId(classId);
		criteria.add(Restrictions.eq("classModel", classModel));
		criteria.add(Restrictions.eq("active", (byte)1));
	    List<CourseAssignmentModel> coursesAssigned = criteria.list();
	    session.close();
	    return coursesAssigned;
	
	}

	@Override
	public CourseAssignmentModel getCousreAssignmentModel(Integer courseId) {
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		CourseAssignmentModel courseAssignmentModel = session.get(CourseAssignmentModel.class, courseId);
		return courseAssignmentModel;
		
	}

	@Override
	public int updateCourseAssignment(CourseAssignmentModel courseAssignmentModel) {
        
		int i =0;
	 	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		CourseAssignmentModel assignmentModel =  session.get(CourseAssignmentModel.class, courseAssignmentModel.getCourseAssigmentId());
	    assignmentModel.setTeacherModel(courseAssignmentModel.getTeacherModel());
        assignmentModel.setCourseModel(courseAssignmentModel.getCourseModel());
	     session.update(assignmentModel);
	     i=1;
        transaction.commit();
        session.close();
        return i;
	}

	@Override
	public int deleteCourseAssignment(int courseAssignmentId) {
		int i  = 0;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		CourseAssignmentModel assignmentModel =  session.get(CourseAssignmentModel.class,courseAssignmentId);
	    assignmentModel.setActive((byte)0);
	    session.update(assignmentModel);
	    i=1;
	    transaction.commit();
	    session.close();
	    return i;
	}

	@Override
	public List<CourseAssignmentModel> getCourseAssignments(Integer classId,Integer teacherId) {
		SessionFactory  sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Query query=session.createQuery("From CourseAssignmentModel where classModel.classId=:class_id and teacherModel.teacherId=:teacher_id and active=1");
		query.setParameter("class_id", classId);
		query.setParameter("teacher_id",teacherId);
		List<CourseAssignmentModel> assignmentModels=query.list();
		return assignmentModels;
	}
	
	
}
