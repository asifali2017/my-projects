package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.StudentDAO;
import salu.sis.models.StudentModel;


@SuppressWarnings("deprecation")
public class StudentDAOImpl implements StudentDAO{

	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	public Integer addStudent(StudentModel studentModel) {
		Integer add=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(studentModel);
		add=1;
		transaction.commit();
		session.close();
		return add;
	}

	@Override
	public Integer deleteStudent(StudentModel studentModel) {
		Integer delete=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(studentModel);
		delete=1;
		transaction.commit();
		session.close();
		return delete;
	}

	@Override
	public Integer updateStudent(StudentModel studentModel) {
		Integer update=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(studentModel);
		update=1;
		transaction.commit();
		session.close();
		return update;
	}

	@Override
	public List<StudentModel> getAllStudents() {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<StudentModel> studentModels=session.createQuery("From StudentModel where active=1").list();
		transaction.commit();
		session.close();
		return studentModels;
	}

	@Override
	public StudentModel getStudentById(Integer studentId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		StudentModel studentModel=session.get(StudentModel.class, studentId);
		transaction.commit();
		session.close();
		return studentModel;
	}

	@SuppressWarnings({"rawtypes" })
	@Override
	public List<StudentModel> getAllStudentsByClass(Integer classId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		Query query=session.createQuery("From StudentModel where classModel.classId=:class_id and active=1");
		query.setParameter("class_id", classId);
		@SuppressWarnings("unchecked")
		List<StudentModel> studentModels=query.list();
		transaction.commit();
		session.close();
		return studentModels;
	}

	@Override
	public boolean checkRollNumber(String rollNumber) {
		boolean found=false;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From StudentModel where rollNumber='"+rollNumber+"' and active=1";
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery(hql);
		StudentModel studentModel=(StudentModel)query.uniqueResult();
		transaction.commit();
		session.close();
		if(studentModel!=null){ 
			found=true;
		}
		return found;
	}

	@Override
	public boolean checkRollNumberWithId(String rollNumber, Integer studentId) {
		boolean found=false;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From StudentModel where rollNumber='"+rollNumber+"' and studentId<>'"+studentId+"' and active=1";
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery(hql);
		StudentModel studentModel=(StudentModel)query.uniqueResult();
		transaction.commit();
		session.close();
		if(studentModel!=null){
			found=true;
		}
		return found;
	}
}
