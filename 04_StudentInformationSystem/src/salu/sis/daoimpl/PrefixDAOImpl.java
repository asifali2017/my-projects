package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.PrefixDAO;
import salu.sis.models.PrefixModel;

public class PrefixDAOImpl implements PrefixDAO{
	
	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	public List<PrefixModel> getAllPrefixes() {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<PrefixModel> prefixModels=session.createQuery("From PrefixModel p where p.active=1").list();
		transaction.commit();
		session.close();
		return prefixModels;
	}

}
