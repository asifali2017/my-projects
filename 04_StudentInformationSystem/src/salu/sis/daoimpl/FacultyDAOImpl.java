package salu.sis.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.FacultyDAO;
import salu.sis.models.FacultyModel;

public class FacultyDAOImpl implements FacultyDAO {

	@Override
	public Integer addFaculty(FacultyModel facultyModel) {
		Integer row=0;
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transcation=session.beginTransaction();
		try{
			session.save(facultyModel);
			row++;
			
		}catch(Exception e){
			row=0;
		}
		finally{
			if(row>0){
				transcation.commit();
				session.close();
			}
		}
		return row;
	}

	@Override
	public Integer updateFaculty(FacultyModel facultyModel) {
		Integer row=0;
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transcation=session.beginTransaction();
		try{
			session.update(facultyModel);
			row++;
			
		}catch(Exception e){
			row=0;
		}
		finally{
			if(row>0){
				transcation.commit();
				session.close();
			}
		}
		return row;
	}

	@Override
	public Integer deleteFaculty(Integer facultyId) {
		Integer row=0;
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transcation=session.beginTransaction();
		try{
			FacultyModel facultyModel=getFacultyById(facultyId);
			facultyModel.setActive((byte)0);
			session.update(facultyModel);
			row++;
			
		}catch(Exception e){
			row=0;
		}
		finally{
			if(row>0){
				transcation.commit();
				session.close();
			}
		}
		return row;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FacultyModel> getAllFaculties() {
		List<FacultyModel> list=new ArrayList<>();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			list=session.createQuery("FROM FacultyModel where active=1").list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(!list.isEmpty()){
				transaction.commit();
				session.close();
			}
		}
		return list;
	}

	@Override
	public FacultyModel getFacultyById(Integer facultyId) {
		FacultyModel facultyModel=new FacultyModel();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			facultyModel=session.get(FacultyModel.class, facultyId);
		}catch(Exception e){
			facultyModel=null;
		}finally{
			if(facultyModel!=null){
				transaction.commit();
				session.close();
			}
		}
		return facultyModel;
	}
	@SuppressWarnings({ "deprecation", "rawtypes" })
	public Integer getFacultyIdByName(String facultyName) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		Query query=session.createQuery("From FacultyModel where facultyName=:faculty");
		query.setParameter("faculty", facultyName);
		FacultyModel facultyModel=(FacultyModel)query.uniqueResult();
		transaction.commit();
		session.close();
		return facultyModel.getFacultyId();
	}
}
