package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.CourseDAO;
import salu.sis.models.CourseModel;
import salu.sis.models.TeacherModel;

public class CourseDAOImpl implements CourseDAO{

	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	public Integer addCourse(CourseModel courseModel) {
		Integer row=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(courseModel);
		row=1;
		transaction.commit();
		session.close();
		return row;
	}

	@Override
	public Integer deleteCourse(Integer courseId) {
		Integer row=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		CourseModel courseModel=session.get(CourseModel.class, courseId);
		courseModel.setActive(0);
		session.update(courseModel);
		row=1;
		transaction.commit();
		session.close();
		return row;
	}

	@Override
	public Integer updateCourse(CourseModel courseModel) {
		Integer row=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(courseModel);
		row=1;
		transaction.commit();
		session.close();
		return row;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CourseModel> getAllCourses() {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		List<CourseModel> courseModels=session.createQuery("From CourseModel c where c.active=1").list();
		transaction.commit();
		session.close();
		return courseModels;
	}

	@Override
	public CourseModel getCourseById(Integer courseId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		CourseModel courseModel=session.get(CourseModel.class, courseId);
		transaction.commit();
		session.close();
		return courseModel;
	}

	public List<CourseModel> getCoursesByDepartment(Integer departmentId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery("From CourseModel where departmentModel.departmentId=:department_id and active=1");
		query.setParameter("department_id", departmentId);
		List<CourseModel> courses=query.list();
		transaction.commit();
		session.close();
		return courses;
		
	}

}
