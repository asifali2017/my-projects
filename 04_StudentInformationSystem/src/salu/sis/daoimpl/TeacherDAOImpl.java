package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.TeacherDAO;
import salu.sis.models.PrefixModel;
import salu.sis.models.TeacherModel;

@SuppressWarnings("deprecation")
public class TeacherDAOImpl implements TeacherDAO{
	
	SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
	@Override
	public Integer addTeacher(TeacherModel teacherModel) {
		Integer success=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(teacherModel);
		success=1;
		transaction.commit();
		session.close();
		return success;
	}

	@Override
	public Integer updateTeacher(TeacherModel teacherModel) {
		Integer success=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(teacherModel);
		success=1;
		transaction.commit();
		session.close();
		return success;
	}

	@Override
	public Integer deleteTeacher(TeacherModel teacherModel) {
		Integer success=0;
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.update(teacherModel);
		success=1;
		transaction.commit();
		session.close();
		return success;
	}

	@Override
	public TeacherModel getTeacherModelById(Integer teacherId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		TeacherModel teacherModel=session.get(TeacherModel.class,teacherId);
		transaction.commit();
		session.close();
		return teacherModel;
	}

	@Override
	public List<TeacherModel> getAllTeachers() {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<TeacherModel> teacherModels=session.createQuery("From TeacherModel t where t.active=1").list();
		transaction.commit();
		session.close();
		return teacherModels;
	}
	public Integer getPrefixIdByName(String prefix) {
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings({ "rawtypes" })
		Query query=session.createQuery("From PrefixModel where prefix=:prefix and active=1");
		query.setParameter("prefix",prefix);
		PrefixModel prefixModel=(PrefixModel)query.uniqueResult();
		transaction.commit();
		session.close();
		return prefixModel.getPrefixId();
	}

	@SuppressWarnings({"unchecked" })
	@Override
	public List<TeacherModel> getTeachersByDepartment(Integer departmentId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		@SuppressWarnings("rawtypes")
		Query query=session.createQuery("From TeacherModel where departmentModel.departmentId=:department_id and active=1");
		query.setParameter("department_id", departmentId);
		List<TeacherModel> teacherModels=query.list();
		transaction.commit();
		session.close();
		return teacherModels;
	}
}
