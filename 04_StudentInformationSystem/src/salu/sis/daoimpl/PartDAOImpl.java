package salu.sis.daoimpl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.PartDAO;
import salu.sis.models.ClassModel;
import salu.sis.models.PartModel;

public class PartDAOImpl implements PartDAO{

	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	Session session = null;
	Transaction transaction = null;
	Integer result = 0;
	
	@Override
	public PartModel getPartModelByPartName(String partName) {
		PartModel partModel = null;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			partModel = (PartModel)session.createQuery("From PartModel p where p.part='"+partName+"' and p.active=1").uniqueResult();
			result = 1;
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return partModel;
	}

}
