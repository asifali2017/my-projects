package salu.sis.daoimpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.ClassesDAO;
import salu.sis.models.ClassModel;
import salu.sis.models.DepartmentModel;
import salu.sis.models.ProgramModel;
import salu.sis.models.SemesterModel;
import salu.sis.models.UserModel;
import salu.sis.models.YearModel;

public class ClassesDAOImpl implements ClassesDAO{

	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	Session session = null;
	Transaction transaction = null;
	Integer result = 0;
	
	@Override
	public Integer addNewClass(ClassModel classModel) {
		result = 0;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			result = (Integer) session.save(classModel);
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Integer updateClass(ClassModel classModel) {
		result = 0;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			session.update(classModel);
			result = 1;
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public Integer deleteClass(ClassModel classModel) {
		result = 0;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			ClassModel c = session.get(ClassModel.class, classModel.getClassId());
			c.setActive(0);
			session.update(c);
			result = 1;
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public ClassModel getClassById(Integer classId) {
		ClassModel classModel = null;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			classModel = (ClassModel)session.createQuery("From ClassModel c where c.classId='"+classId+"' and c.active=1").list();
			result = 1;
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return classModel;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClassModel> getAllClasses() {
		List<ClassModel> classesList = null;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			classesList = (List<ClassModel>)session.createQuery("From ClassModel c where c.active=1").list();
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return classesList;
	}

	@Override
	public List<ClassModel> searchClasses(ProgramModel programModel,SemesterModel semesterModel) {
		List<ClassModel> classesList = null;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			String hql = "from ClassModel c where c.semesterModel.semesterId like :semesterId and programsModel.departmentModel.departmentId like :departmentId and active=1";
			Query query = session.createQuery(hql);
			
			if(semesterModel.getSemesterId()!=null && programModel.getDepartmentModel().getDepartmentId()!=null)
			{
				classesList = (List<ClassModel>) session.createQuery("FROM ClassModel c where c.semesterModel.semesterId='"+semesterModel.getSemesterId()+"' and c.programsModel.departmentModel.departmentId='"+programModel.getDepartmentModel().getDepartmentId()+"' and c.active=1").list();
			}
			else if(semesterModel.getSemesterId()!=null)
			{
				classesList = (List<ClassModel>) session.createQuery("FROM ClassModel c where c.semesterModel.semesterId='"+semesterModel.getSemesterId()+"' and c.active=1").list();
			}
			else if(programModel.getDepartmentModel()!=null)
			{
				classesList = (List<ClassModel>) session.createQuery("FROM ClassModel c where c.programsModel.departmentModel.departmentId='"+programModel.getDepartmentModel().getDepartmentId()+"' and c.active=1").list();
			}
	
			//classesList = (List<ClassModel>)query.getResultList();
			transaction.commit();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return classesList;
	}

	@Override
	public boolean validateClasses(ClassModel classModel) {
		boolean r = false;
		ClassModel c = null;
		try{
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();
			c = (ClassModel)session.createQuery("From ClassModel c where c.partModel.partId='"+classModel.getPartModel().getPartId()+"' and c.programsModel.programId='"+classModel.getProgramsModel().getProgramId()+"' and c.semesterModel.semesterId='"+classModel.getSemesterModel().getSemesterId()+"' and c.active=1").uniqueResult();
			if(c!=null)
			{
				r = true;
				System.out.println(r);
			}
			transaction.commit();
			session.close();
		}catch(Exception e)
		{
			if(transaction!=null)
			{
				transaction.rollback();
			}
			e.printStackTrace();
		}
		return r;
	}

	@Override
	public List<ClassModel> getClassesWithSemesterAndProgram(ClassModel classModel) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	    Session session = sessionFactory.openSession();	
         	
	    Criteria criteria = session.createCriteria(ClassModel.class);
	    ProgramModel programModel = new ProgramModel();
	    programModel.setProgramId(classModel.getProgramsModel().getProgramId());
	    SemesterModel semesterModel = new SemesterModel();
	    semesterModel.setSemesterId(classModel.getSemesterModel().getSemesterId());
	    criteria.add(Restrictions.eq("programsModel", programModel));
	    criteria.add(Restrictions.eq("semesterModel", semesterModel));
	    List<ClassModel> classes = criteria.list();
	    
		return classes;
	}

	@Override
	public List<ClassModel> getClassesBySemster(Integer semsterId) {
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		String hql="From ClassModel where semesterModel.semesterId='"+semsterId+"' and active=1";
		Query query=session.createQuery(hql);
		List<ClassModel> classModels=query.list();
		return classModels;
		}
}
