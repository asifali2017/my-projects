package salu.sis.daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.ProgramDAO;
import salu.sis.models.DepartmentModel;
import salu.sis.models.ProgramModel;

public class ProgramDAOImpl implements ProgramDAO{

	@Override
	public Integer addProgram(ProgramModel programModel) {
		Integer row=0;
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			session.save(programModel);
			row++;
		}catch(Exception ex){
			row=0;
		}finally{
			if(row>0){
				transaction.commit();
				session.close();
			}
		}
		return row;
	}

	@Override
	public Integer updateProgram(ProgramModel programModel) {
		Integer row=0;
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			session.update(programModel);
			row++;
		}catch(Exception ex){
			row=0;
		}finally{
			if(row>0){
				transaction.commit();
				session.close();
			}
		}
		return row;
	}

	@Override
	public Integer deleteProgram(Integer programId) {
		Integer row=0;
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			ProgramModel programModel=session.get(ProgramModel.class, programId);
			programModel.setActive((byte)0);
			session.save(programModel);
			row++;
		}catch(Exception ex){
			row=0;
		}finally{
			if(row>0){
				transaction.commit();
				session.close();
			}
		}
		return row;
	}

	@Override
	public List<ProgramModel> getAllPrograms() {
		List<ProgramModel> list=new ArrayList<ProgramModel>();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			list=session.createQuery("FROM ProgramModel where active=1").list();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			if(!list.isEmpty()){
				transaction.commit();
				session.close();
			}
		}
		return list;
	}

	@Override
	public ProgramModel getProgramById(Integer programId) {
		ProgramModel programModel=new ProgramModel();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			programModel=session.get(ProgramModel.class, programId);
		}catch(Exception ex){
			programModel=null;
		}finally{
			if(programModel!=null){
				transaction.commit();
				session.close();
			}
		}
		return programModel;
	}

	@Override
	public List<ProgramModel> getAllProgramsDepartmentWise(Integer departmentId) {
		List<ProgramModel> list=new ArrayList<ProgramModel>();
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction transaction=session.beginTransaction();
		try{
			Criteria c = session.createCriteria(ProgramModel.class);
					DepartmentModel d = new DepartmentModel();
					d.setDepartmentId(departmentId);
					c.add(Restrictions.eq("departmentModel",d));
					c.add(Restrictions.eq("active", (byte)1));
					list = c.list();			
		}catch(Exception ex){
			list=null;
		}finally{
			if(!list.isEmpty()){
				transaction.commit();
				session.close();
			}
		}
		return list;
	}

	@Override
	public ProgramModel getProgramByName(String programName) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
