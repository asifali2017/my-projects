package salu.sis.daoimpl;

import java.util.List;

import javax.persistence.TypedQuery;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import salu.sis.connection.HibernateUtil;
import salu.sis.dao.SemesterDao;
import salu.sis.models.SemesterModel;
import salu.sis.models.YearModel;

public class SemesterDAOImpl implements SemesterDao {


	@Override
	public int addNewSemester(SemesterModel semesterModel) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
	    Transaction transaction = session.beginTransaction();
	    int i = (int) session.save(semesterModel);
	    transaction.commit();
	    session.close();
	    return i;
	}


	@Override
	public List<SemesterModel> viewSemesterData() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<SemesterModel> semesters = session.createQuery("From SemesterModel s where s.active = 1").list();
	    return semesters;	
	}

	@Override
	public SemesterModel getSemesterModelById(Integer id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
	    SemesterModel semesterModel = session.get(SemesterModel.class,id );
	    return semesterModel;
	}

	@Override
	public int updateSemester(SemesterModel semester) {
		int i = 0;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
	    SemesterModel semesterModel = session.get(SemesterModel.class, semester.getSemesterId());	
		int type = semester.getType();
		String startDate = semester.getStartDate();
		String endDate = semester.getEndDate();
		semesterModel.setType(type);
		semesterModel.setYearModel((semester.getYearModel()));
		semesterModel.setStartDate(startDate);
		semesterModel.setEndDate(endDate);
		semesterModel.setActive((byte) 1);
	
	    session.update(semesterModel);
		i = 1;
		transaction.commit();
		session.close();
		return i;
	}

	@Override
	public int deleteSemester(int id) {
		int i = 0;
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		SemesterModel semesterModel = session.get(SemesterModel.class, id);	
		semesterModel.setActive((byte)0);
		session.update(semesterModel);
	    i = 1;
		transaction.commit();
	    session.close();
        return i;		
 	}

	@Override
	public SemesterModel getSemesterModel(SemesterModel semesterModel) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
	    SemesterModel semesterResult = (SemesterModel)session.createQuery("FROM SemesterModel s where s.type='"+semesterModel.getType()+"' and s.yearModel.yearName='"+semesterModel.getYearModel().getYearName()+"' and s.active=1").uniqueResult();
	    return semesterResult;
	}
	

	@Override
	public List<SemesterModel> getAllSemester() {
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		List<SemesterModel> semesterModels=session.createQuery("From SemesterModel where active=1").list();
		session.clear();
		return semesterModels;
	}
	
	
	
	
	
}
