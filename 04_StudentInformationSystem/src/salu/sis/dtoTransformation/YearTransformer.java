package salu.sis.dtoTransformation;

import salu.sis.dto.YearDto;
import salu.sis.models.YearModel;

public class YearTransformer {

	public static YearDto transform(YearModel yearModel){
		YearDto yearDto = new YearDto();
		if(yearModel.getYearId()!=null){
			yearDto.setYearId(String.valueOf(yearModel.getYearId()));	
		}
		if(yearModel.getYearName()!=null){
			yearDto.setYear(yearModel.getYearName());
		}
		
		return yearDto;
		
	}
	
	
	
}
