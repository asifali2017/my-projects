package salu.sis.dtoTransformation;

import salu.sis.dto.ClassesDTO;
import salu.sis.dto.PartDTO;
import salu.sis.dto.ProgramDTO;
import salu.sis.dto.SemesterDto;
import salu.sis.models.ClassModel;

public class ClassesDTOTransform {
	
	public static ClassesDTO transform(ClassModel classModel)
	{
		ClassesDTO classesDTO = new ClassesDTO();
		if(classModel.getClassId()!=null)
		{
			classesDTO.setClassId(String.valueOf(classModel.getClassId()));
		}
		if(classModel.getPartModel()!=null)
		{
			PartDTO partDTO = PartDTOTransform.transform(classModel.getPartModel());
			classesDTO.setPartDTO(partDTO);
		}
		if(classModel.getProgramsModel()!=null)
		{
			ProgramDTO programDTO = ProgramTransformer.transform(classModel.getProgramsModel());
			classesDTO.setProgramDTO(programDTO);
		}
		if(classModel.getSemesterModel()!=null)
		{
			SemesterDto semesterDTO = SemesterDtoTransformation.transform(classModel.getSemesterModel());
			classesDTO.setSemesterDTO(semesterDTO);
		}
		return classesDTO;
	}
	
}
