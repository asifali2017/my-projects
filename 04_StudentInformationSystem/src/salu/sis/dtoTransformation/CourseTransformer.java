package salu.sis.dtoTransformation;

import salu.sis.dto.CourseDTO;
import salu.sis.models.CourseModel;

public class CourseTransformer {
	
	public static CourseDTO transformer(CourseModel courseModel){
		CourseDTO courseDTO=new CourseDTO();
		if(courseModel.getCourseCode()!=null){
			courseDTO.setCourseCode(courseModel.getCourseCode());
		}
		if (courseModel.getCourseId()!=null) {
			courseDTO.setCourseId(courseModel.getCourseId().toString());
		}
		if (courseModel.getCourseTitle()!=null) {
			courseDTO.setCourseTitle(courseModel.getCourseTitle());
		}
		if (courseModel.getCourseCredit()!=null) {
			courseDTO.setCourseCredit(courseModel.getCourseCredit());
		}
		if (courseModel.getCourseCredit()!=null) {
			courseDTO.setCourseCredit(courseModel.getCourseCredit());
		}
		if(courseModel.getCourseType()!=null){
			courseDTO.setCourseType(courseModel.getCourseType());
		}
		if (courseModel.getDepartmentModel().getDepartmentName()!=null) {
			courseDTO.setDepartmentName(courseModel.getDepartmentModel().getDepartmentName());
		}
		return courseDTO;
	}
}
