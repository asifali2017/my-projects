package salu.sis.dtoTransformation;

import salu.sis.dto.StudentDTO;
import salu.sis.models.StudentModel;

public class StudentTransform {
	public static StudentDTO transform(StudentModel studentModel){
		StudentDTO studentDTO=new StudentDTO();
		if(studentModel.getStudentId()!=null){
			studentDTO.setStudentId(studentModel.getStudentId().toString());
		}
		if(studentModel.getFirstName()!=null){
			studentDTO.setFirstName(studentModel.getFirstName());
		}
		if(studentModel.getMiddleName()!=null){
			studentDTO.setMiddleName(studentModel.getMiddleName());
		}
		if(studentModel.getLastName()!=null){
			studentDTO.setLastName(studentModel.getLastName());
		}
		if(studentModel.getRollNumber()!=null){
			studentDTO.setRollNumber(studentModel.getRollNumber());	
		}
		return studentDTO;
	}

}
