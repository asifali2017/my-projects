package salu.sis.dtoTransformation;

import salu.sis.dto.PartDTO;
import salu.sis.models.PartModel;

public class PartDTOTransform {

	public static PartDTO transform(PartModel partModel)
	{
		PartDTO partDTO = new PartDTO();
		if(partModel.getPartId()!=null)
		{
			partDTO.setPartId(String.valueOf(partModel.getPartId()));
		}
		if(partModel.getPart()!=null)
		{
			partDTO.setPart(String.valueOf(partModel.getPart()));
		}
		return partDTO;
	}
}
