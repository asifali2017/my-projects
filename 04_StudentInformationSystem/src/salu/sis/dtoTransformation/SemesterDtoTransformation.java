package salu.sis.dtoTransformation;

import salu.sis.dto.SemesterDto;
import salu.sis.models.SemesterModel;

public class SemesterDtoTransformation {

	public static SemesterDto transform(SemesterModel semesterModel) {
		SemesterDto semesterDto = new SemesterDto();
		if (semesterModel.getSemesterId() != null) {
			semesterDto.setSemesterId(String.valueOf(semesterModel.getSemesterId()));
		}
		if (semesterModel.getType() != null) {
			if(semesterModel.getType()==1){
				semesterDto.setType("First");	
			}else{
				semesterDto.setType("Second");
			}
			
		}
		if (semesterModel.getStartDate() != null) {
			semesterDto.setStartDate(semesterModel.getStartDate());
		}
		if (semesterModel.getEndDate() != null) {
			semesterDto.setEndDate(semesterModel.getEndDate());
		}
		if (semesterModel.getYearModel().getYearName() != null) {
			semesterDto.setYear(semesterModel.getYearModel().getYearName());
		}

		return semesterDto;
	}

}
