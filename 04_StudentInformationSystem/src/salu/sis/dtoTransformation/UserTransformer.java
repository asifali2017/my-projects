package salu.sis.dtoTransformation;

import salu.sis.dto.UserDTO;
import salu.sis.models.UserModel;

public class UserTransformer {

	public static UserDTO transform(UserModel userModel) {

		UserDTO userDTO = new UserDTO();

		if (userModel.getUserId() != null) {
			userDTO.setUserId(String.valueOf(userModel.getUserId()));
		}
		if (userModel.getUserName() != null) {
			userDTO.setUserName(userModel.getUserName());
		}
		if (userModel.getDepartmentModel().getDepartmentCode() != null) {
			userDTO.setDepartmentCode(userModel.getDepartmentModel().getDepartmentCode());
		}
		if (userModel.getUserTypeModel().getUserType() != null) {

			userDTO.setAccountType(userModel.getUserTypeModel().getUserType());
		}

		return userDTO;
	}

}
