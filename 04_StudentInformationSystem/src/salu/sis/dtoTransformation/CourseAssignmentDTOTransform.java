package salu.sis.dtoTransformation;

import salu.sis.dto.CourseAssignmentDTO;
import salu.sis.models.CourseAssignmentModel;

public class CourseAssignmentDTOTransform {

	 public CourseAssignmentDTO transform(CourseAssignmentModel courseAssignmentModel){
		 CourseAssignmentDTO courseAssignmentDTO = new CourseAssignmentDTO();
		 if(courseAssignmentModel.getCourseAssigmentId()!=null)
		 {
			 courseAssignmentDTO.setCourseAssignmentId(String.valueOf(courseAssignmentModel.getCourseAssigmentId()));
			 
		 }
		 if(courseAssignmentModel.getTeacherModel().getFirstName()!=null){
			 
			 courseAssignmentDTO.setTeacherFirstName(courseAssignmentModel.getTeacherModel().getFirstName());
		 }
		 if(courseAssignmentModel.getTeacherModel().getLastName()!=null)
		 {
			 
			 courseAssignmentDTO.setTeacherLastName(courseAssignmentModel.getTeacherModel().getLastName());
		 }
		 if(courseAssignmentModel.getCourseModel().getCourseTitle()!=null){
			 
			 courseAssignmentDTO.setCourseAssigned(courseAssignmentModel.getCourseModel().getCourseTitle());
		 }
		 if(courseAssignmentModel.getCourseModel().getCourseCode()!=null){
			 
			 courseAssignmentDTO.setCourseCode(courseAssignmentModel.getCourseModel().getCourseCode());
		 }
		 return courseAssignmentDTO;
	 }
	 
	
	
}
