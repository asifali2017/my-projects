package salu.sis.dtoTransformation;

import salu.sis.dto.DurationDTO;
import salu.sis.models.DurationModel;

public class DurationTransformer {
	public static DurationDTO transform(DurationModel durationModel){
		DurationDTO durationDTO=new DurationDTO();
		if(durationModel.getDurationId()!=null){
			durationDTO.setDurationId(durationModel.getDurationId().toString());
		}
		
		if(durationModel.getDays()!=null){
			durationDTO.setDays(durationModel.getDays().toString());
		}
		
		if(durationModel.getActive()!=null){
			durationDTO.setActive(durationModel.getActive().toString());
		}
		
		return durationDTO;
	}
}
