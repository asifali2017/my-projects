package salu.sis.dtoTransformation;

import salu.sis.dto.UserTypeDTO;
import salu.sis.models.UserTypeModel;

public class UserTypeTransformer {

	public UserTypeDTO transform(UserTypeModel userTypeModel) {

		UserTypeDTO userTypeDTO = new UserTypeDTO();
		if (userTypeModel.getUserId() != null) {

			userTypeDTO.setUserId(String.valueOf(userTypeModel.getUserId()));
		}
		if (userTypeModel.getUserType() != null) {

			userTypeDTO.setUserType(userTypeModel.getUserType());

		}

		return userTypeDTO;

	}

}
