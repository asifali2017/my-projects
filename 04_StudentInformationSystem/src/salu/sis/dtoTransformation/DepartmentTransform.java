package salu.sis.dtoTransformation;

import salu.sis.dto.DepartmentDTO;
import salu.sis.models.DepartmentModel;

public class DepartmentTransform {
	
	public static DepartmentDTO transformDepartment(DepartmentModel departmentModel){
		DepartmentDTO departmentDTO=new DepartmentDTO();
		if(departmentModel.getDepartmentId()!=null){
			departmentDTO.setDepartmentId(departmentModel.getDepartmentId().toString());
		}
		if(departmentModel.getDepartmentCode()!=null){
			departmentDTO.setDepartmentCode(departmentModel.getDepartmentCode());
		}
		if(departmentModel.getDepartmentName()!=null){
			departmentDTO.setDepartmentName(departmentModel.getDepartmentName());
		}
		if(departmentModel.getFacultyModel().getFacultyName()!=null){
			departmentDTO.setFacultyName(departmentModel.getFacultyModel().getFacultyName().toString()); 
		}
		if(departmentModel.getFacultyModel().getFacultyId()!=null){
			departmentDTO.setFacultyId(departmentModel.getFacultyModel().getFacultyId().toString());
		}
		return departmentDTO;
	}

}
