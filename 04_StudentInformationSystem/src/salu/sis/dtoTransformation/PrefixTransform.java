package salu.sis.dtoTransformation;

import salu.sis.dto.PrefixDTO;
import salu.sis.models.PrefixModel;

public class PrefixTransform {
	public static PrefixDTO transformDepartment(PrefixModel prefixModel){
		PrefixDTO prefixDTO=new PrefixDTO();
		if(prefixModel.getPrefixId()!=null){
			prefixDTO.setPrefixId(prefixModel.getPrefixId().toString());
		}
		if(prefixModel.getPrefix()!=null){
			prefixDTO.setPrefix(prefixModel.getPrefix());
		}
		return prefixDTO;
	}
}
