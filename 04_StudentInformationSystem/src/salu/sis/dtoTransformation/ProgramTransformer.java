package salu.sis.dtoTransformation;

import salu.sis.dto.ProgramDTO;
import salu.sis.models.ProgramModel;

public class ProgramTransformer {
	public static ProgramDTO transform(ProgramModel programModel){
		ProgramDTO programDTO=new ProgramDTO();
		if(programModel.getProgramId()!=null){
			programDTO.setProgramId(programModel.getProgramId().toString());
		}
		
		if(programModel.getProgramTitle()!=null){
			programDTO.setProgramTitle(programModel.getProgramTitle());
		}
		
		if(programModel.getProgramCode()!=null){
			programDTO.setProgramCode(programModel.getProgramCode());
		}
		
		if(programModel.getDurationModel().getDurationId()!=null){
			programDTO.setDurationId(programModel.getDurationModel().getDurationId().toString());
		}
		
		if(programModel.getDurationModel().getDays()!=null){
			programDTO.setDays(programModel.getDurationModel().getDays().toString());
		}
		
		if(programModel.getDepartmentModel().getDepartmentId()!=null){
			programDTO.setDepartmentId(programModel.getDepartmentModel().getDepartmentId().toString());
		}
		
		if(programModel.getDepartmentModel().getDepartmentName()!=null){
			programDTO.setDepartmentName(programModel.getDepartmentModel().getDepartmentName());
		}
		return programDTO;
	}
}
