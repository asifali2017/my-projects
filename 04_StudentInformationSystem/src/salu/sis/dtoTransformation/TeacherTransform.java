package salu.sis.dtoTransformation;

import salu.sis.dto.TeacherDTO;
import salu.sis.models.TeacherModel;

public class TeacherTransform {
	public static TeacherDTO transformTeacher(TeacherModel teacherModel){
		TeacherDTO teacherDTO=new TeacherDTO();
		if(teacherModel.getTeacherId()!=null){
			teacherDTO.setTeacherId(teacherModel.getTeacherId().toString());
		}
		if(teacherModel.getFirstName()!=null){
			teacherDTO.setFirstName(teacherModel.getFirstName());
		}
		if(teacherModel.getLastName()!=null){
			teacherDTO.setLastName(teacherModel.getLastName());
		}
		if(teacherModel.getPrefixModel().getPrefix()!=null){
			teacherDTO.setPrefix(teacherModel.getPrefixModel().getPrefix());
		}
		if(teacherModel.getPrefixModel().getPrefixId()!=null){
			teacherDTO.setPrefixId(teacherModel.getPrefixModel().getPrefixId().toString());
		}
		return teacherDTO;
	}

}
