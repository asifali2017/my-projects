package salu.sis.dtoTransformation;

import salu.sis.dto.FacultyDTO;
import salu.sis.models.FacultyModel;

public class FacultyTransformer {
	public static FacultyDTO transform(FacultyModel facultyModel){
		FacultyDTO facultyDTO=new FacultyDTO();
		if(facultyModel.getFacultyId()!=null){
			facultyDTO.setFacultyId(facultyModel.getFacultyId().toString());
		}
		if(facultyModel.getFacultyName()!=null){
			facultyDTO.setFacultyName(facultyModel.getFacultyName());
		}
		return facultyDTO;
	}
}
