package salu.sis.dto;

public class DurationDTO {
	private String durationId;
	private String days;
	private String active;
	public String getDurationId() {
		return durationId;
	}
	public void setDurationId(String durationId) {
		this.durationId = durationId;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
}
