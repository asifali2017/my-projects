package salu.sis.dto;

public class ClassesDTO {

	private String classId;
	private SemesterDto semesterDTO;
	private PartDTO partDTO;
	private ProgramDTO programDTO;
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public SemesterDto getSemesterDTO() {
		return semesterDTO;
	}
	public void setSemesterDTO(SemesterDto semesterDTO) {
		this.semesterDTO = semesterDTO;
	}
	public PartDTO getPartDTO() {
		return partDTO;
	}
	public void setPartDTO(PartDTO partDTO) {
		this.partDTO = partDTO;
	}
	public ProgramDTO getProgramDTO() {
		return programDTO;
	}
	public void setProgramDTO(ProgramDTO programDTO) {
		this.programDTO = programDTO;
	}
	
}
