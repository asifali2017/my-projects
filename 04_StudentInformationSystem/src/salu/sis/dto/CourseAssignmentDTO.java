package salu.sis.dto;

public class CourseAssignmentDTO {
    
	private String courseAssignmentId;
	private String teacherFirstName;
	private String teacherLastName;
	private String courseAssigned;
	private String courseCode;
	public String getCourseCode() {
		return courseCode;
	}
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}
	public String getCourseAssignmentId() {
		return courseAssignmentId;
	}
	public void setCourseAssignmentId(String courseAssignmentId) {
		this.courseAssignmentId = courseAssignmentId;
	}
	public String getTeacherFirstName() {
		return teacherFirstName;
	}
	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}
	public String getTeacherLastName() {
		return teacherLastName;
	}
	public void setTeacherLastName(String teacherLastName) {
		this.teacherLastName = teacherLastName;
	}
	public String getCourseAssigned() {
		return courseAssigned;
	}
	public void setCourseAssigned(String courseAssigned) {
		this.courseAssigned = courseAssigned;
	}
	
	
}
