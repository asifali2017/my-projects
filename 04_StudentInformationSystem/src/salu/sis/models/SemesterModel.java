package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "semester")
public class SemesterModel {

	@Id
	@GeneratedValue
	@Column(name = "semester_id")
	private Integer semesterId;
	@ManyToOne
	@JoinColumn(name = "year_id")
	private YearModel yearModel;
	@Column(name = "type")
	private Integer type;
	@Column(name = "start_date")
	private String startDate;
	@Column(name = "end_date")
	private String endDate;
	@Column(name = "active")
	private byte active;

	@OneToMany(targetEntity = ClassModel.class, mappedBy = "semesterModel", cascade = CascadeType.ALL)
	Set<ClassModel> classes = new HashSet<>();

	public Set<ClassModel> getClasses() {
		return classes;
	}

	public void setClasses(Set<ClassModel> classes) {
		this.classes = classes;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}



	public YearModel getYearModel() {
		return yearModel;
	}

	public void setYearModel(YearModel yearModel) {
		this.yearModel = yearModel;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public byte getActive() {
		return active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

}
