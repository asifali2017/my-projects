package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "part")
public class PartModel {

	@Id
	@GeneratedValue
	@Column(name = "part_id")
	private Integer partId;

	@Column(name = "part")
	private String part;
	@Column(name = "active")
	private Integer active;

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@OneToMany(targetEntity = ClassModel.class, mappedBy = "partModel", cascade = CascadeType.ALL)
	Set<ClassModel> classes = new HashSet<>();

	public Set<ClassModel> getClasses() {
		return classes;
	}

	public void setClasses(Set<ClassModel> classes) {
		this.classes = classes;
	}

	public Integer getPartId() {
		return partId;
	}

	public void setPartId(Integer partId) {
		this.partId = partId;
	}

	public String getPart() {
		return part;
	}

	public void setPart(String part) {
		this.part = part;
	}


}
