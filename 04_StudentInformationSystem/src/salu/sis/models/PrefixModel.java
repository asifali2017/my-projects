package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "prefix")
public class PrefixModel {

	@Id
	@GeneratedValue
	@Column(name = "prefix_id")
	private Integer prefixId;

	@Column(name = "prefix")
	private String prefix;

	@Column(name = "active")
	private Integer active;

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@OneToMany(targetEntity = TeacherModel.class, mappedBy = "prefixModel", cascade = CascadeType.ALL)
	Set<TeacherModel> teachers = new HashSet<>();

	public Integer getPrefixId() {
		return prefixId;
	}

	public Set<TeacherModel> getTeachers() {
		return teachers;
	}

	public void setTeachers(Set<TeacherModel> teachers) {
		this.teachers = teachers;
	}

	public void setPrefixId(Integer prefixId) {
		this.prefixId = prefixId;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

}
