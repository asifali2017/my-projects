package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "duration")
public class DurationModel {

	@Id
	@GeneratedValue
	@Column(name = "duration_id")
	private Integer durationId;

	@Column(name = "days")
	private Integer days;

	@Column(name = "active")
	private Integer active;

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Set<ProgramModel> getPrograms() {
		return programs;
	}

	public void setPrograms(Set<ProgramModel> programs) {
		this.programs = programs;
	}

	@OneToMany(targetEntity = ProgramModel.class, mappedBy = "durationModel", cascade = CascadeType.ALL)
	Set<ProgramModel> programs = new HashSet<>();

	public Integer getDurationId() {
		return durationId;
	}

	public void setDurationId(Integer durationId) {
		this.durationId = durationId;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

}
