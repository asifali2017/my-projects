package salu.sis.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class UserModel {

	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private Integer userId;
	@Column(name = "user_name")
	private String userName;
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Column(name = "password")
	private String password;
	@Column(name="image_path")
	private String imagePath;
	@ManyToOne
	@JoinColumn(name = "user_type_id")
	private UserTypeModel userTypeModel;
	@ManyToOne
	@JoinColumn(name = "dept_id")
	private DepartmentModel departmentModel;
	@ManyToOne
	@JoinColumn(name = "teacher_id")
	private TeacherModel teacherModel;
	@ManyToOne
	@JoinColumn(name = "faculty_id")
	private FacultyModel facultyModel;
	@Column(name = "active")
	private Byte active;

	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserTypeModel getUserTypeModel() {
		return userTypeModel;
	}

	public void setUserTypeModel(UserTypeModel userTypeModel) {
		this.userTypeModel = userTypeModel;
	}

	public DepartmentModel getDepartmentModel() {
		return departmentModel;
	}

	public void setDepartmentModel(DepartmentModel departmentModel) {
		this.departmentModel = departmentModel;
	}

	public TeacherModel getTeacherModel() {
		return teacherModel;
	}

	public void setTeacherModel(TeacherModel teacherModel) {
		this.teacherModel = teacherModel;
	}

	public FacultyModel getFacultyModel() {
		return facultyModel;
	}

	public void setFacultyModel(FacultyModel facultyModel) {
		this.facultyModel = facultyModel;
	}

}
