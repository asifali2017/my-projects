package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "programs")
public class ProgramModel {

	@Id
	@GeneratedValue
	@Column(name = "program_id")
	private Integer programId;

	@Column(name = "title")
	private String programTitle;

	@Column(name = "code")
	private String programCode;

	@ManyToOne
	@JoinColumn(name = "duration_id")
	private DurationModel durationModel;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private DepartmentModel departmentModel;

	@Column(name = "active")
	private byte active;

	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	@OneToMany(targetEntity = ClassModel.class, mappedBy = "programsModel", cascade = CascadeType.ALL)
	Set<ClassModel> classes = new HashSet<>();

	public Set<ClassModel> getClasses() {
		return classes;
	}

	public void setClasses(Set<ClassModel> classes) {
		this.classes = classes;
	}

	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

	public String getProgramTitle() {
		return programTitle;
	}

	public void setProgramTitle(String programTitle) {
		this.programTitle = programTitle;
	}

	public String getProgramCode() {
		return programCode;
	}

	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}

	public DurationModel getDurationModel() {
		return durationModel;
	}

	public void setDurationModel(DurationModel durationModel) {
		this.durationModel = durationModel;
	}

	public DepartmentModel getDepartmentModel() {
		return departmentModel;
	}

	public void setDepartmentModel(DepartmentModel departmentModel) {
		this.departmentModel = departmentModel;
	}

}
