package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "faculty")
public class FacultyModel {

	@Id
	@GeneratedValue
	@Column(name = "faculty_id")
	private Integer facultyId;
	@Column(name = "faculty")
	private String facultyName;
	@Column(name = "active")
	private Byte active;
    
	@OneToMany(targetEntity=UserModel.class,mappedBy="facultyModel",cascade=CascadeType.ALL)
	Set<UserModel> users = new HashSet<>();
	
 	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Set<DepartmentModel> getDepartments() {
		return departments;
	}

	public void setDepartments(Set<DepartmentModel> departments) {
		this.departments = departments;
	}

	@OneToMany(targetEntity = DepartmentModel.class, mappedBy = "facultyModel", cascade = CascadeType.ALL)
	Set<DepartmentModel> departments = new HashSet<>();

	public Integer getFacultyId() {
		return facultyId;
	}

	public void setFacultyId(Integer facultyId) {
		this.facultyId = facultyId;
	}

	public String getFacultyName() {
		return facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

}
