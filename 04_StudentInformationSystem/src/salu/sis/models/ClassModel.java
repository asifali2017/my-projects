package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "class")
public class ClassModel {

	@Id
	@GeneratedValue
	@Column(name = "class_id")
	private Integer classId;

	@ManyToOne
	@JoinColumn(name = "semester_id")
	private SemesterModel semesterModel;

	@ManyToOne
	@JoinColumn(name = "part_id")
	private PartModel partModel;

	@ManyToOne
	@JoinColumn(name = "program_id")
	private ProgramModel programsModel;
	
	@Column(name = "active")
	private Integer active;

	@OneToMany(targetEntity=StudentModel.class,mappedBy="classModel",cascade=CascadeType.ALL)
	Set<StudentModel> students = new HashSet<>();
	
	@OneToMany(targetEntity=CourseAssignmentModel.class,mappedBy="classModel",cascade=CascadeType.ALL)
	Set<CourseAssignmentModel> courseAssigmnets = new HashSet<>();

	public Set<StudentModel> getStudents() {
		return students;
	}

	public void setStudents(Set<StudentModel> students) {
		this.students = students;
	}

	public Integer getActive() {
		return active;
	}
	
	public void setActive(Integer active) {
		this.active = active;
	}
    
	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public SemesterModel getSemesterModel() {
		return semesterModel;
	}

	public void setSemesterModel(SemesterModel semesterModel) {
		this.semesterModel = semesterModel;
	}

	public PartModel getPartModel() {
		return partModel;
	}

	public void setPartModel(PartModel partModel) {
		this.partModel = partModel;
	}

	public ProgramModel getProgramsModel() {
		return programsModel;
	}

	public void setProgramsModel(ProgramModel programsModel) {
		this.programsModel = programsModel;
	}

}
