package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name="course_assignment")
public class CourseAssignmentModel {
    
	@Id
	@GeneratedValue
	@Column(name="course_assignment_id")
	private Integer courseAssigmentId;
	
	@ManyToOne
	@JoinColumn(name="course_id")
	private CourseModel courseModel;
	
	@OneToMany(targetEntity=InsertAttendenceModel.class,mappedBy="courseAssignmentModel",cascade=CascadeType.ALL)
	Set<InsertAttendenceModel> getAttendese = new HashSet<>();
	
	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	@ManyToOne
	@JoinColumn(name="teacher_id")
	private TeacherModel teacherModel;
	
	@ManyToOne
	@JoinColumn(name="class_id")
	private ClassModel classModel;
	
	@Column(name="active")
	private Byte active;

	public Integer getCourseAssigmentId() {
		return courseAssigmentId;
	}

	public void setCourseAssigmentId(Integer courseAssigmentId) {
		this.courseAssigmentId = courseAssigmentId;
	}

	public CourseModel getCourseModel() {
		return courseModel;
	}

	public void setCourseModel(CourseModel courseModel) {
		this.courseModel = courseModel;
	}

	public TeacherModel getTeacherModel() {
		return teacherModel;
	}

	public void setTeacherModel(TeacherModel teacherModel) {
		this.teacherModel = teacherModel;
	}

	public ClassModel getClassModel() {
		return classModel;
	}

	public void setClassModel(ClassModel classModel) {
		this.classModel = classModel;
	}
	
	
}
