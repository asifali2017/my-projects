package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "teacher")
public class TeacherModel {

	@Id
	@GeneratedValue
	@Column(name = "teacher_id")
	private Integer teacherId;

	@ManyToOne
	@JoinColumn(name = "prefix_id")
	private PrefixModel prefixModel;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "active")
	private Integer active;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private DepartmentModel departmentModel;
	
	@OneToMany(targetEntity=UserModel.class,mappedBy="teacherModel",cascade=CascadeType.ALL)
	Set<UserModel> users = new HashSet<>();
	 
	@OneToMany(targetEntity=CourseAssignmentModel.class,mappedBy="teacherModel",cascade=CascadeType.ALL)
	Set<CourseAssignmentModel> courseAssigmnets = new HashSet<>();

    
	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public PrefixModel getPrefixModel() {
		return prefixModel;
	}

	public void setPrefixModel(PrefixModel prefixModel) {
		this.prefixModel = prefixModel;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public DepartmentModel getDepartmentModel() {
		return departmentModel;
	}

	public void setDepartmentModel(DepartmentModel departmentModel) {
		this.departmentModel = departmentModel;
	}

}
