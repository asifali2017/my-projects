package salu.sis.models;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "years")
public class YearModel {

	@Id
	@GeneratedValue
	@Column(name = "year_id")
	private Integer yearId;
	@Column(name = "year_name")
	private String yearName;
	@Column(name = "active")
	private Byte active;

	public Byte getActive() {
		return active;
	}

	public void setActive(Byte active) {
		this.active = active;
	}

	public Set<SemesterModel> getSemesters() {
		return semesters;
	}

	public void setSemesters(Set<SemesterModel> semesters) {
		this.semesters = semesters;
	}

	@OneToMany(targetEntity = SemesterModel.class, mappedBy = "yearModel", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	Set<SemesterModel> semesters = new HashSet<>();

	public Integer getYearId() {
		return yearId;
	}

	public void setYearId(Integer yearId) {
		this.yearId = yearId;
	}

	public String getYearName() {
		return yearName;
	}

	public void setYearName(String yearName) {
		this.yearName = yearName;
	}
	
	public YearModel(){
		
	}
	
	public YearModel(Map<String, String> map){
		this.yearId=Integer.parseInt(map.get(0));
		this.yearName=map.get(1);
	}

}
