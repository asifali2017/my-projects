package salu.sis.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="department")
public class DepartmentModel {
    
	@Id
	@GeneratedValue
	@Column(name="department_id")
	private Integer departmentId;
	@ManyToOne
	@JoinColumn(name="faculty_id")
	private FacultyModel facultyModel;
	@Column(name="department_name")
	private String departmentName;
	@Column(name="department_code")
	private String departmentCode;
	@Column(name = "active")
	private byte active;
	
	@OneToMany(targetEntity=TeacherModel.class,mappedBy="departmentModel",cascade=CascadeType.ALL)
	Set<TeacherModel> teachers = new HashSet<>();
	
	@OneToMany(targetEntity=ProgramModel.class,mappedBy="departmentModel",cascade=CascadeType.ALL)
	Set<ProgramModel> programs = new HashSet<>();
	
	@OneToMany(targetEntity=CourseModel.class,mappedBy="departmentModel",cascade=CascadeType.ALL)
	Set<CourseModel> courses = new HashSet<>();
	
	@OneToMany(targetEntity=UserModel.class,mappedBy="departmentModel",cascade=CascadeType.ALL)
	Set<UserModel> users = new HashSet<>();
	
		
	public Byte getActive() {
		return active;
	}
	public void setActive(Byte active) {
		this.active = active;
	}
	public Set<ProgramModel> getPrograms() {
		return programs;
	}
	public void setPrograms(Set<ProgramModel> programs) {
		this.programs = programs;
	}
	public Set<CourseModel> getCourses() {
		return courses;
	}
	public void setCourses(Set<CourseModel> courses) {
		this.courses = courses;
	}
	public Set<TeacherModel> getTeachers() {
		return teachers;
	}
	public void setTeachers(Set<TeacherModel> teachers) {
		this.teachers = teachers;
	}
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	public FacultyModel getFacultyModel() {
		return facultyModel;
	}
	public void setFacultyModel(FacultyModel facultyModel) {
		this.facultyModel = facultyModel;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	
	
	
}
