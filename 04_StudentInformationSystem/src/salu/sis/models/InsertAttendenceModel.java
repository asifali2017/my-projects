package salu.sis.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="insert_attendence")
public class InsertAttendenceModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "insert_attendence_id")
	private Integer insertAttendenceId;
	
	@Column(name="date")
	private String date;
	
	@Column(name="status")
	private String status;
	
	@Column(name="topic")
	private String topic;
	
	@Column(name="conduct_classes")
	private String conductClasses;
	
	private Integer count;
	
	private Integer totalStudents;
	
	@ManyToOne
	@JoinColumn(name = "course_assignment_id")
	private CourseAssignmentModel courseAssignmentModel;
	
	
	

	@ManyToOne
	@JoinColumn(name = "student_id")
	private StudentModel studentModel;

	public Integer getInsertAttendenceId() {
		return insertAttendenceId;
	}

	public void setInsertAttendenceId(Integer insertAttendenceId) {
		this.insertAttendenceId = insertAttendenceId;
	}
	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getConductClasses() {
		return conductClasses;
	}

	public void setConductClasses(String conductClasses) {
		this.conductClasses = conductClasses;
	}
	
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	

	public Integer getTotalStudents() {
		return totalStudents;
	}

	public void setTotalStudents(Integer totalStudents) {
		this.totalStudents = totalStudents;
	}

	public CourseAssignmentModel getCourseAssignmentModel() {
		return courseAssignmentModel;
	}

	public void setCourseAssignmentModel(CourseAssignmentModel courseAssignmentModel) {
		this.courseAssignmentModel = courseAssignmentModel;
	}

	public StudentModel getStudentModel() {
		return studentModel;
	}

	public void setStudentModel(StudentModel studentModel) {
		this.studentModel = studentModel;
	}

}
