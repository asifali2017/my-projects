package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.daoimpl.SemesterDAOImpl;
import salu.sis.daoimpl.YearDAOImpl;
import salu.sis.dto.SemesterDto;
import salu.sis.dto.YearDto;
import salu.sis.dtoTransformation.SemesterDtoTransformation;
import salu.sis.dtoTransformation.YearTransformer;
import salu.sis.models.SemesterModel;
import salu.sis.models.YearModel;

/**
 * Servlet implementation class SemesterServlet
 */
@WebServlet("/SemesterServlet")
public class SemesterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SemesterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		if (request.getParameter("method") != null) {
			if (request.getParameter("method").equals("getAllYears")) {

				response.setContentType("application/json");
				List<YearModel> years = new YearDAOImpl().getAllYears();
				List<YearDto> yearDtos = new ArrayList<>();
				for (int i = 0; i < years.size(); i++) {
					YearDto yearDto = YearTransformer.transform(years.get(i));
					yearDtos.add(yearDto);
				}

				Gson gson = new Gson();
				System.out.println(gson.toJson(yearDtos));
				printWriter.print(gson.toJson(yearDtos));
			} else if (request.getParameter("method").equals("addNewSemester")) {
				response.setContentType("application/json");
				SemesterModel semesterModel = new SemesterModel();
				String year = request.getParameter("year");
				String type = request.getParameter("type");
				String startDate = request.getParameter("startdate");
				String endDate = request.getParameter("enddate");
				YearModel yearsModel = new YearDAOImpl().getYearByName(year);
				semesterModel.setYearModel(yearsModel);
				if (type.equals("First")) {
					semesterModel.setType(1);
				} else {
					semesterModel.setType(2);
				}
				semesterModel.setStartDate(startDate);
				semesterModel.setEndDate(endDate);
				semesterModel.setActive((byte) 1);
				int i = new SemesterDAOImpl().addNewSemester(semesterModel);
				printWriter.print(i);

			} else if (request.getParameter("method").equals("viewSemesterData")) {
				response.setContentType("application/json");
				List<SemesterModel> semesters = new SemesterDAOImpl().viewSemesterData();
				List<SemesterDto> semesterDtos = new ArrayList<>();
				for (int i = 0; i < semesters.size(); i++) {

					SemesterDto semesterDto = SemesterDtoTransformation.transform(semesters.get(i));
					semesterDtos.add(semesterDto);
				}
				Gson gson = new Gson();
				System.out.println(gson.toJson(semesterDtos));
				printWriter.print(gson.toJson(semesterDtos));

			}else if(request.getParameter("method").equals("getSemesterModel")){
				response.setContentType("application/json");
				int id = Integer.parseInt(request.getParameter("id"));
				SemesterModel semesterModel = new SemesterDAOImpl().getSemesterModelById(id);
				SemesterDto semesterDto = SemesterDtoTransformation.transform(semesterModel);
				Gson gson = new Gson();
				System.out.println(gson.toJson(semesterDto));
				printWriter.print(gson.toJson(semesterDto));
			}else if(request.getParameter("method").equals("updateSemester")){
				response.setContentType("application/json");
				SemesterModel semesterModel = new SemesterModel();
				int id = Integer.parseInt(request.getParameter("semesterid"));
				String year = request.getParameter("year");
				String type = request.getParameter("type");
				String startDate = request.getParameter("startdate");
				String endDate = request.getParameter("enddate");
				YearModel yearsModel = new YearDAOImpl().getYearByName(year);
				semesterModel.setYearModel(yearsModel);
				if (type.equals("First")) {
					semesterModel.setType(1);
				} else {
					semesterModel.setType(2);
				}
				semesterModel.setSemesterId(id);
				semesterModel.setStartDate(startDate);
				semesterModel.setEndDate(endDate);
				semesterModel.setActive((byte) 1);
				int i = new SemesterDAOImpl().updateSemester(semesterModel);
				printWriter.print(i);	
			}
			else if(request.getParameter("method").equals("deleteSemester")){
			

				int id = Integer.parseInt(request.getParameter("id"));
				int i = new SemesterDAOImpl().deleteSemester(id);
				printWriter.print(i);
				
			}

		}

	}

}
