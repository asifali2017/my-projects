package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.dao.DurationDAO;
import salu.sis.dao.ProgramDAO;
import salu.sis.daoimpl.DurationDAOImpl;
import salu.sis.daoimpl.ProgramDAOImpl;
import salu.sis.dto.DurationDTO;
import salu.sis.dto.ProgramDTO;
import salu.sis.dtoTransformation.DurationTransformer;
import salu.sis.dtoTransformation.ProgramTransformer;
import salu.sis.models.DurationModel;
import salu.sis.models.ProgramModel;

/**
 * Servlet implementation class DurationServlet
 */
@WebServlet("/DurationServlet")
public class DurationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DurationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response.getWriter();
		DurationDAO durationDAO=new DurationDAOImpl();
		if(request.getParameter("method")!=null){
			if(request.getParameter("method").equals("getAllRecords")){
				List<DurationModel> durationsList=durationDAO.getAllDurations();
				List<DurationDTO> durationsDTOList=new ArrayList<>();
				System.out.println(durationsList);
				if(!durationsList.isEmpty()){
					for(int i=0;i<durationsList.size();i++){
						DurationDTO durationDTO=new DurationDTO();
						DurationModel durationModel=new DurationModel();
						durationModel.setDurationId(durationsList.get(i).getDurationId());
						durationModel.setDays(durationsList.get(i).getDays());
						durationDTO=DurationTransformer.transform(durationModel);
						durationsDTOList.add(durationDTO);
					}
					Gson gson=new Gson();
					String data=gson.toJson(durationsDTOList);
					System.out.println(data);
					pw.print(data);
				}
			}
		}
	}

}
