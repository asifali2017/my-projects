package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.dao.CourseDAO;
import salu.sis.dao.DepartmentDAO;
import salu.sis.daoimpl.CourseDAOImpl;
import salu.sis.daoimpl.DepartmentDAOImpl;
import salu.sis.dto.CourseDTO;
import salu.sis.dto.DepartmentDTO;
import salu.sis.dtoTransformation.CourseTransformer;
import salu.sis.dtoTransformation.DepartmentTransform;
import salu.sis.models.CourseModel;
import salu.sis.models.DepartmentModel;
import salu.sis.models.TeacherModel;

/**
 * Servlet implementation class CourseServlet
 */
@WebServlet("/CourseServlet")
public class CourseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CourseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter=response.getWriter();
		String method=request.getParameter("method");
		Gson gson=new Gson();
		
		if (method!=null) {
			if(method.equals("getAllDepartments")){
			DepartmentDAO departmentDAO=new DepartmentDAOImpl(); 
				List<DepartmentModel> departmentModels=departmentDAO.getAllDepartments();
				List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
				for (int i = 0; i < departmentModels.size(); i++) {
					DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(departmentModels.get(i));
					departmentDTOs.add(departmentDTO);
				}
				printWriter.write(gson.toJson(departmentDTOs));
				
			}
			else if(method.equals("getAllRecords")){
                response.setContentType("application/json"); 				
				Integer departmentId=Integer.parseInt(request.getParameter("department"));
				List<CourseModel> courses=new CourseDAOImpl().getCoursesByDepartment(departmentId);
				List<CourseDTO> courseDTOs=new ArrayList<CourseDTO>();
				for(int i=0; i<courses.size(); i++){
					CourseDTO courseDTO=CourseTransformer.transformer(courses.get(i));
					courseDTOs.add(courseDTO);
				}
				printWriter.write(gson.toJson(courseDTOs));
		}else if(method.equals("addCourse")){
			CourseDAO courseDAO=new CourseDAOImpl();
			CourseModel courseModel=new CourseModel();
			DepartmentModel departmentModel=new DepartmentModel();
			Integer departmentId = Integer.parseInt(request.getParameter("department"));
			courseModel.setCourseCode(request.getParameter("code"));
			courseModel.setCourseCredit(request.getParameter("credit"));
			courseModel.setCourseTitle(request.getParameter("title"));
			courseModel.setCourseType(request.getParameter("type"));
			courseModel.setActive(1);
			departmentModel.setDepartmentId(departmentId);
			courseModel.setDepartmentModel(departmentModel);
			Integer data=courseDAO.addCourse(courseModel);
			printWriter.write(gson.toJson(data));
		}else if(method.equals("updateCourse")){
			CourseDAO courseDAO=new CourseDAOImpl();
			CourseModel courseModel=new CourseModel();
			DepartmentModel departmentModel=new DepartmentModel();
			courseModel.setCourseCode(request.getParameter("code"));
			courseModel.setCourseCredit(request.getParameter("credit"));
			courseModel.setCourseTitle(request.getParameter("title"));
			courseModel.setCourseType(request.getParameter("type"));
			courseModel.setCourseId(Integer.parseInt(request.getParameter("updateId")));
			courseModel.setActive(1);
			departmentModel.setDepartmentId(6);
			courseModel.setDepartmentModel(departmentModel);
			Integer data=courseDAO.updateCourse(courseModel);
			printWriter.write(gson.toJson(data));
			
		}else if(method.equals("deleteRecord")){
			CourseDAO courseDAO=new CourseDAOImpl();
			String id=request.getParameter("deleteId");
			Integer data=courseDAO.deleteCourse(Integer.parseInt(id));
			printWriter.write(gson.toJson(data));
		}else if(method.equals("getDataById")){
			CourseDAO courseDAO=new CourseDAOImpl();
			String id=request.getParameter("updateId");
			CourseModel courseModel= courseDAO.getCourseById(Integer.parseInt(id));
			CourseDTO courseDTO=CourseTransformer.transformer(courseModel);
			printWriter.write(gson.toJson(courseDTO));			
		}
	}

}
}
