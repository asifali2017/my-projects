package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.dao.DurationDAO;
import salu.sis.dao.ProgramDAO;
import salu.sis.daoimpl.DurationDAOImpl;
import salu.sis.daoimpl.ProgramDAOImpl;
import salu.sis.dto.ProgramDTO;
import salu.sis.dtoTransformation.ProgramTransformer;
import salu.sis.models.DepartmentModel;
import salu.sis.models.DurationModel;
import salu.sis.models.ProgramModel;

/**
 * Servlet implementation class ProgramServlet
 */
@WebServlet("/ProgramServlet")
public class ProgramServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static List<String> programCodes = new ArrayList<>();
	private static List<String> programTitles = new ArrayList<>();
	private ProgramDAO programDAO = new ProgramDAOImpl();
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProgramServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json");
		DurationDAO durationDAO = new DurationDAOImpl();
		if (request.getParameter("method") != null) {
			if (request.getParameter("method").equals("getAllRecords")) {
				List<ProgramModel> programsList = programDAO.getAllPrograms();
				List<ProgramDTO> programsDTOList = new ArrayList<>();
				if (!programsList.isEmpty()) {
					for (int i = 0; i < programsList.size(); i++) {
						ProgramDTO programDTO = new ProgramDTO();
						programDTO = ProgramTransformer.transform(programsList.get(i));
						programsDTOList.add(programDTO);
					}
					Gson gson = new Gson();
					String data = gson.toJson(programsDTOList);
					System.out.println(data);
					pw.print(data);
				}
			} else if (request.getParameter("method").equals("getAllRecordsDepartmentWise")) {
					Integer departmentId=Integer.parseInt(request.getParameter("departmentId"));
					System.out.println(departmentId);
					List<ProgramModel> programsList = programDAO.getAllProgramsDepartmentWise(departmentId);
					List<ProgramDTO> programsDTOList = new ArrayList<>();
					if (!programsList.isEmpty()) {
						for (int i = 0; i < programsList.size(); i++) {
							ProgramDTO programDTO = new ProgramDTO();
							programDTO = ProgramTransformer.transform(programsList.get(i));
							programsDTOList.add(programDTO);
						}
						Gson gson = new Gson();
						String data = gson.toJson(programsDTOList);
						System.out.println(data);
						pw.print(data);
					}
				} else if (request.getParameter("method").equals("deleteRecord")) {
					Integer programId = Integer.parseInt(request.getParameter("deleteId"));
					System.out.println(programId);
					Integer row = programDAO.deleteProgram(programId);
					pw.print(row);
				} else if (request.getParameter("method").equals("getRecordById")) {
					Integer programId = Integer.parseInt(request.getParameter("editId"));
					ProgramModel programModel = programDAO.getProgramById(programId);
					if (programModel != null) {
						ProgramDTO programDTO = ProgramTransformer.transform(programModel);
						Gson gson = new Gson();
						String data = gson.toJson(programDTO);
						pw.print(data);
					}
				}

			}
			if (request.getParameter("addRecord") != null) {
				String programTitle = request.getParameter("program-title");
				String programCode = request.getParameter("program-code");
				Integer durationId = Integer.parseInt(request.getParameter("duration"));
				Integer row = 0;
				System.out.println(request.getParameter("department-id"));
				if(!programTitle.equals("") && !programTitle.trim().equals("") && programTitle!=null && !programCode.equals("") && !programCode.trim().equals("") && programCode!=null && durationId!=null && checkProgramTitleAndCode(programTitle, programCode)==false){
					ProgramModel programModel = new ProgramModel();
					programModel.setProgramTitle(programTitle);
					programModel.setProgramCode(programCode);
					DepartmentModel departmentModel = new DepartmentModel();
					Integer departmentId=Integer.parseInt(request.getParameter("department-id"));
					departmentModel.setDepartmentId(departmentId);
					programModel.setDepartmentModel(departmentModel);
					DurationModel durationModel = new DurationModel();
					durationModel.setDurationId(Integer.parseInt(request.getParameter("duration")));
					programModel.setDurationModel(durationModel);
					programModel.setActive((byte) 1);
					row = programDAO.addProgram(programModel);
				}
				if (row > 0) {
					response.sendRedirect("admin/program.jsp?added=success");
				} else {
					response.sendRedirect("admin/program.jsp?added=error");
				}

			}
			if (request.getParameter("updateRecord") != null) {
				
				Integer programId = Integer.parseInt(request.getParameter("program-id"));
				String programTitle = request.getParameter("program-title");
				String programCode = request.getParameter("program-code");
				Integer durationId = Integer.parseInt(request.getParameter("duration"));
				Integer row = 0;
				if(!programTitle.equals("") && !programTitle.trim().equals("") && programTitle!=null && !programCode.equals("") && !programCode.trim().equals("") && programCode!=null && durationId!=null){
					ProgramModel programModel = new ProgramModel();
					programModel.setProgramId(programId);
					programModel.setProgramTitle(programTitle);
					programModel.setProgramCode(programCode);
					DepartmentModel departmentModel = new DepartmentModel();
					Integer departmentId=Integer.parseInt(request.getParameter("department-id"));
					departmentModel.setDepartmentId(departmentId);
					programModel.setDepartmentModel(departmentModel);
					DurationModel durationModel = new DurationModel();
					durationModel.setDurationId(Integer.parseInt(request.getParameter("duration")));
					programModel.setDurationModel(durationModel);
					programModel.setActive((byte) 1);
					row = programDAO.updateProgram(programModel);
				}
				if (row > 0) {
					response.sendRedirect("admin/program.jsp?updated=success");
				} else {
					response.sendRedirect("admin/program.jsp?updated=error");
				}

			}
		}
	private Boolean checkProgramTitleAndCode(String programTitle , String programCode){
		Boolean check = false;
		List<ProgramModel> programList=programDAO.getAllPrograms();
		if(!programList.isEmpty()){
			for(int i=0;i<programList.size();i++){
				programCodes.add(programList.get(i).getProgramCode().toLowerCase());
				programTitles.add(programList.get(i).getProgramTitle().toLowerCase());
			}
		}
		if(programCodes.contains(programCode.toLowerCase())){
			check = true;
		}
		if(programTitles.contains(programTitle.toLowerCase())){
			check = true;
		}
		return check;
	}

}
