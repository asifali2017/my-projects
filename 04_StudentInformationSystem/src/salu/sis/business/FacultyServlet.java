package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.dao.FacultyDAO;
import salu.sis.daoimpl.FacultyDAOImpl;
import salu.sis.dto.FacultyDTO;
import salu.sis.models.FacultyModel;
import salu.sis.dtoTransformation.*;

/**
 * Servlet implementation class FacultyServlet
 */
@WebServlet("/FacultyServlet")
public class FacultyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static List<String> faculties = new ArrayList<>();
    FacultyDAO facultyDAO=new FacultyDAOImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FacultyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response .getWriter();
		response.setContentType("application/json");
		if(request.getParameter("method")!=null){
			if(request.getParameter("method").equals("addRecord")){
				String facultyName=request.getParameter("facultyName");
				Integer row=0;
				if(facultyName!=null && !facultyName.trim().equals("") && !facultyName.equals("") && facultyName.length()>=2 && checkFaculy(facultyName)==false){
					FacultyModel facultyModel=new FacultyModel();
					facultyModel.setFacultyName(facultyName);
					facultyModel.setActive((byte) 1);
					row=facultyDAO.addFaculty(facultyModel);
					pw.print(row);
				}else{
					pw.print(row);
				}
			}
			else if(request.getParameter("method").equals("updateRecord")){
				Integer facultyId=Integer.parseInt(request.getParameter("facultyId"));
				String facultyName=request.getParameter("facultyName");
				Integer row=0;
				System.out.println(checkFaculy(facultyName));
				if(facultyName!=null && !facultyName.trim().equals("") && !facultyName.equals("") && facultyName.length()>=2 && checkFaculy(facultyName)==false){
					FacultyModel facultyModel=new FacultyModel();
					facultyModel.setFacultyId(facultyId);
					facultyModel.setFacultyName(facultyName);
					facultyModel.setActive((byte) 1);
					row=facultyDAO.updateFaculty(facultyModel);
					pw.print(row);
				}else{
					pw.print(row);
				}
				
			}
			
			else if(request.getParameter("method").equals("deleteRecord")){
				Integer facultyId=Integer.parseInt(request.getParameter("deleteId"));
				System.out.println(facultyId);
				Integer row=facultyDAO.deleteFaculty(facultyId);
				pw.print(row);
			}
			
			else if(request.getParameter("method").equals("getAllRecords")){
				List<FacultyModel> facultyList=facultyDAO.getAllFaculties();
				List<FacultyDTO> facultyDTOList=new ArrayList<>();
				if(!facultyList.isEmpty()){
					for(int i=0;i<facultyList.size();i++){
						FacultyDTO facultyDTO=new FacultyDTO();
						facultyDTO=FacultyTransformer.transform(facultyList.get(i));
						facultyDTOList.add(facultyDTO);
					}
					Gson gson=new Gson();
					String data=gson.toJson(facultyDTOList);
					pw.print(data);
			}
				
			}else if(request.getParameter("method").equals("getRecordById")){
				Integer facultyId=Integer.parseInt(request.getParameter("editId"));
				FacultyModel facultyModel=facultyDAO.getFacultyById(facultyId);
				if(facultyModel!=null){
					FacultyDTO facultyDTO=FacultyTransformer.transform(facultyModel);
					Gson gson=new Gson();
					String data=gson.toJson(facultyDTO);
					pw.print(data);
				}
			}
		}
	}
	private Boolean checkFaculy(String facultyName){
		Boolean check = false;
		List<FacultyModel> facultyList=facultyDAO.getAllFaculties();
		if(!facultyList.isEmpty()){
			for(int i=0;i<facultyList.size();i++){
				faculties.add(facultyList.get(i).getFacultyName().toLowerCase());
			}
		}
		if(facultyList.contains(facultyName.toLowerCase())){
			check = true;
			return check;
		}
		return check;
	}
}
