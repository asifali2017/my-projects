package salu.sis.business;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;

import salu.sis.daoimpl.DepartmentDAOImpl;
import salu.sis.daoimpl.UserDAOImpl;
import salu.sis.dto.DepartmentDTO;
import salu.sis.dto.UserDTO;
import salu.sis.dto.UserTypeDTO;
import salu.sis.dtoTransformation.DepartmentTransform;
import salu.sis.dtoTransformation.UserTransformer;
import salu.sis.dtoTransformation.UserTypeTransformer;
import salu.sis.models.DepartmentModel;
import salu.sis.models.FacultyModel;
import salu.sis.models.UserModel;
import salu.sis.models.UserTypeModel;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		Gson gson = new Gson();
		String path = "";
		if (ServletFileUpload.isMultipartContent(request)) {
			HttpSession httpSession = request.getSession();
			int userId = Integer.parseInt(httpSession.getAttribute("id").toString());
			try {
				List<FileItem> multiparts = new ServletFileUpload(

						new DiskFileItemFactory()).parseRequest((request));
				UserModel userModel = new UserModel();
				for (FileItem item : multiparts) {
					if (!item.isFormField()) {
						String name = new File(item.getName()).getName();
                        path = File.separator + name;
						item.write(new File("C:\\upload" + File.separator + name));
                        httpSession.setAttribute("path", File.separator + name);
						userModel.setImagePath(path);
						userModel.setUserId(userId);
						int row = new UserDAOImpl().setUserImage(userModel);
					} 

				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		if (request.getParameter("method") != null) {
			if (request.getParameter("method").equals("getAllUserTypes")) {
				response.setContentType("application/json");

				List<UserTypeModel> userTypes = new UserDAOImpl().getAllUsersTypes();
				List<UserTypeDTO> userTypeDto = new ArrayList<>();
				for (int i = 0; i < userTypes.size(); i++) {

					UserTypeDTO typeDto = new UserTypeTransformer().transform(userTypes.get(i));
					userTypeDto.add(typeDto);
				}
				
				System.out.println(gson.toJson(userTypeDto));
				printWriter.print(gson.toJson(userTypeDto));
			}
			else if (request.getParameter("method").equals("getAllUsers")) {
				response.setContentType("application/json");
				UserModel userModel = new UserModel();
				DepartmentModel departmentModel = new DepartmentModel();
				FacultyModel facultyModel = new FacultyModel();
				UserTypeModel userTypeModel = new UserTypeModel();
				if (request.getParameter("faculty").equals("none")) {
					facultyModel.setFacultyName("");
				}else{
					facultyModel.setFacultyName(request.getParameter("faculty"));
				}
				if (request.getParameter("department").equals("none")) {
					departmentModel.setDepartmentName("");
					
				}else{
					departmentModel.setDepartmentName(request.getParameter("department"));
				}
				if (request.getParameter("usertype").equals("none")) {
					userTypeModel.setUserType("");
				}else{
					userTypeModel.setUserType(request.getParameter("usertype"));
				}
				userModel.setDepartmentModel(departmentModel);
				userModel.setFacultyModel(facultyModel);
				userModel.setUserTypeModel(userTypeModel);
				List<UserModel> users = new UserDAOImpl().getAllUsers(userModel);
				List<UserDTO> userDTO = new ArrayList<>();
				for (int i = 0; i < users.size(); i++) {
					UserDTO dto = new UserTransformer().transform(users.get(i));
					userDTO.add(dto);
				}
				
				System.out.println(gson.toJson(userDTO));
				printWriter.print(gson.toJson(userDTO));
			}
			else if (request.getParameter("method").equals("getAllDepartmentsWithFaculty")){
				response.setContentType("application/json");
				int facultyId = Integer.parseInt(request.getParameter("faculty"));
				List<DepartmentModel> departments = new DepartmentDAOImpl().getDepartmentWithFacultyId(facultyId);
				List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
				for(DepartmentModel dm:departments){
					DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(dm);
					departmentDTOs.add(departmentDTO);				
				}
				System.out.println(gson.toJson(departmentDTOs));
				printWriter.write(gson.toJson(departmentDTOs));
			
				
			}
			else if(request.getParameter("method").equals("addUsers")){
				String userName = request.getParameter("username");
				if(new UserDAOImpl().checkUser(userName)!=true){
				int userTypeId = Integer.parseInt(request.getParameter("usertype"));
				int faculty = Integer.parseInt(request.getParameter("faculty"));
				int department = Integer.parseInt(request.getParameter("department"));
				String pass = request.getParameter("pass");
			    UserModel userModel = new UserModel();
			    UserTypeModel userTypeModel = new UserTypeModel();
			    userTypeModel.setUserId(userTypeId);
			    FacultyModel facultyModel = new FacultyModel();
			    facultyModel.setFacultyId(faculty);
			    DepartmentModel departmentModel = new DepartmentModel();
			    departmentModel.setDepartmentId(department);
			    userModel.setUserName(userName);
			    userModel.setPassword(pass);
			    userModel.setImagePath("images/profile.png");
			    userModel.setUserTypeModel(userTypeModel);
			    userModel.setFacultyModel(facultyModel);
			    userModel.setDepartmentModel(departmentModel);
			    
			    int row = new UserDAOImpl().insertUserData(userModel);
			    if(row>0){
			    	printWriter.print("success");
			    }
				}else{
			    	printWriter.print("exists");
			    }
			    

			}
			else if(request.getParameter("method").equals("getUserById")){
				response.setContentType("application/json");
				int userId = Integer.parseInt(request.getParameter("userid"));
				UserModel userModel = new UserDAOImpl().getUserById(userId);
				UserDTO userDTO = new UserTransformer().transform(userModel);
				System.out.println(gson.toJson(userDTO));
				printWriter.write(gson.toJson(userDTO));
				
			}
			else if(request.getParameter("method").equals("updateUserData"))
			{  
				int userId = Integer.parseInt(request.getParameter("userid"));
				UserModel userModel = new UserModel();
				userModel.setUserId(userId);
				userModel.setUserName(request.getParameter("username"));
				userModel.setPassword(request.getParameter("password"));
				int row = new UserDAOImpl().updateUserData(userModel);
				if(row>0){
					printWriter.write("success");
				}
			}
			else if(request.getParameter("method").equals("deleteUser")){
				int userId = Integer.parseInt(request.getParameter("id"));
				int row = new UserDAOImpl().deleteUserData(userId);
				if(row>0){
					printWriter.write("success");
				}
				
			}
			

		}

	}

}
