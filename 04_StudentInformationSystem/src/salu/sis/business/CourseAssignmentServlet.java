package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.daoimpl.ClassesDAOImpl;
import salu.sis.daoimpl.CourseAssignmentDAOImpl;
import salu.sis.daoimpl.SemesterDAOImpl;
import salu.sis.dto.ClassesDTO;
import salu.sis.dto.CourseAssignmentDTO;
import salu.sis.dtoTransformation.ClassesDTOTransform;
import salu.sis.dtoTransformation.CourseAssignmentDTOTransform;
import salu.sis.models.ClassModel;
import salu.sis.models.CourseAssignmentModel;
import salu.sis.models.CourseModel;
import salu.sis.models.ProgramModel;
import salu.sis.models.SemesterModel;
import salu.sis.models.TeacherModel;

/**
 * Servlet implementation class CourseAssignmentServlet
 */
@WebServlet("/CourseAssignmentServlet")
public class CourseAssignmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CourseAssignmentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("method") != null) {
			Gson gson = new Gson();
			PrintWriter printWriter = response.getWriter();
			if (request.getParameter("method").equals("getClassWithSemesterAndProgram")) {
				response.setContentType("application/json");
				String semesterId = request.getParameter("semesterId");
				String programId = request.getParameter("programId");
				ClassModel classModel = new ClassModel();
				SemesterModel semesterModel = new SemesterModel();
				ProgramModel programModel = new ProgramModel();
				if (semesterId.equals("none")) {
					semesterModel.setSemesterId(0);
					classModel.setSemesterModel(semesterModel);
				} else {
					semesterModel.setSemesterId(Integer.parseInt(semesterId));
					classModel.setSemesterModel(semesterModel);
				}
				if (programId.equals("none")) {
					programModel.setProgramId(0);
					classModel.setProgramsModel(programModel);
				} else {
					programModel.setProgramId(Integer.parseInt(programId));
					classModel.setProgramsModel(programModel);
				}
				List<ClassModel> classes = new ClassesDAOImpl().getClassesWithSemesterAndProgram(classModel);
				List<ClassesDTO> classesDTOList = new ArrayList<ClassesDTO>();
				for (ClassModel c : classes) {
					ClassesDTO classesDTO = ClassesDTOTransform.transform(c);
					classesDTOList.add(classesDTO);
				}
				System.out.println(gson.toJson(classesDTOList));
				response.setContentType("application/json");
				printWriter.write(gson.toJson(classesDTOList));

			}else if(request.getParameter("method").equals("assignCourses")){
				int classId = Integer.parseInt(request.getParameter("classId"));
				int courseId = Integer.parseInt(request.getParameter("courseId"));
				int teacherId = Integer.parseInt(request.getParameter("teacherId"));
				ClassModel classModel = new ClassModel();
				TeacherModel teacherModel = new TeacherModel();
				CourseModel courseModel = new CourseModel();
				CourseAssignmentModel courseAssignmentModel = new CourseAssignmentModel();
				classModel.setClassId(classId);
				teacherModel.setTeacherId(teacherId);
				courseModel.setCourseId(courseId);
				courseAssignmentModel.setClassModel(classModel);
				courseAssignmentModel.setTeacherModel(teacherModel);
				courseAssignmentModel.setCourseModel(courseModel);
				int i = new CourseAssignmentDAOImpl().assignCourse(courseAssignmentModel);
				if(i>0){
					printWriter.print("success");
				}
			}
			else if(request.getParameter("method").equals("getAllAssignedCourses"))
			{
				response.setContentType("application/json");
				int classId = Integer.parseInt(request.getParameter("classId"));
				List<CourseAssignmentModel> getAssignedCourses = new CourseAssignmentDAOImpl().getAllAssignedCoursesWithClass(classId);
				List<CourseAssignmentDTO> courseAssignmentDTO = new ArrayList<>();
				for(int i=0;i<getAssignedCourses.size();i++)
				{
					CourseAssignmentDTO assignmentDTO = new CourseAssignmentDTOTransform().transform(getAssignedCourses.get(i));
				    courseAssignmentDTO.add(assignmentDTO);
				}
				System.out.println(gson.toJson(courseAssignmentDTO));
				printWriter.print(gson.toJson(courseAssignmentDTO));
			}
			else if(request.getParameter("method").equals("courseAssignmentModel")){
				response.setContentType("application/json");
				int courseAssignmentId = Integer.parseInt(request.getParameter("courseAssingmentId"));	
				CourseAssignmentModel courseAssignmentModel = new CourseAssignmentDAOImpl().getCousreAssignmentModel(courseAssignmentId);
				CourseAssignmentDTO courseAssignmentDTO = new CourseAssignmentDTOTransform().transform(courseAssignmentModel);
				System.out.println(gson.toJson(courseAssignmentDTO));
				printWriter.print(gson.toJson(courseAssignmentDTO));
				
			}
			else if(request.getParameter("method").equals("updateCourseAssignment")){
				int courseId = Integer.parseInt(request.getParameter("courseId"));
				int teacherId = Integer.parseInt(request.getParameter("teacherId"));
				int courseAssignmentId = Integer.parseInt(request.getParameter("courseAssingmentId"));
				TeacherModel teacherModel = new TeacherModel();
				CourseModel courseModel = new CourseModel();
				CourseAssignmentModel courseAssignmentModel = new CourseAssignmentModel();
				teacherModel.setTeacherId(teacherId);
				courseModel.setCourseId(courseId);
				courseAssignmentModel.setTeacherModel(teacherModel);
				courseAssignmentModel.setCourseModel(courseModel);
				courseAssignmentModel.setCourseAssigmentId(courseAssignmentId);
				int i = new CourseAssignmentDAOImpl().updateCourseAssignment(courseAssignmentModel);
				if(i>0){
					printWriter.print("success");
				}
			}else if(request.getParameter("method").equals("deleteCourseAssignment")){
			

				int id = Integer.parseInt(request.getParameter("id"));
				int i = new CourseAssignmentDAOImpl().deleteCourseAssignment(id);
				printWriter.print(i);
				
			}

		}
	}
}
