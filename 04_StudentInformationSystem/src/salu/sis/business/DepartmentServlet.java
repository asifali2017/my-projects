package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import salu.sis.dao.DepartmentDAO;
import salu.sis.dao.FacultyDAO;
import salu.sis.daoimpl.DepartmentDAOImpl;
import salu.sis.daoimpl.FacultyDAOImpl;
import salu.sis.daoimpl.UserDAOImpl;
import salu.sis.dto.DepartmentDTO;
import salu.sis.dtoTransformation.DepartmentTransform;
import salu.sis.models.DepartmentModel;
import salu.sis.models.FacultyModel;
import salu.sis.models.UserModel;

/**
 * Servlet implementation class DepartmentServlet
 */
@WebServlet("/DepartmentServlet")
public class DepartmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DepartmentDAO departmentDAO=new DepartmentDAOImpl();
	FacultyDAO facultyDAO=new FacultyDAOImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DepartmentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Gson gson = new Gson();
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json");
		if(request.getParameter("method")!=null){
			String action=request.getParameter("method");
			if(action.equals("getAllDepartments")){
				List<DepartmentModel> departmentModels=departmentDAO.getAllDepartments();
				List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
				for(DepartmentModel dm:departmentModels){
					DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(dm);
					departmentDTOs.add(departmentDTO);				
				}
				System.out.println(gson.toJson(departmentDTOs));
				pw.write(gson.toJson(departmentDTOs));
			}
			if(action.equals("getAllDepartmentNames")){
				List<DepartmentModel> departmentModels=departmentDAO.getAllDepartments();
				List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
				for(DepartmentModel dm:departmentModels){
					DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(dm);
					DepartmentDTO departmentDTO2=new DepartmentDTO();
					departmentDTOs.add(departmentDTO);				
				}
				String data=gson.toJson(departmentDTOs);
				pw.print(data);
			}
			if(action.equals("addDepartment")){
				DepartmentModel departmentModel=new DepartmentModel();
				FacultyModel facultyModel=new FacultyModel();
				departmentModel.setDepartmentCode(request.getParameter("departmentCode"));
				departmentModel.setDepartmentName(request.getParameter("departmentName"));
				Integer  facultyId=Integer.parseInt(request.getParameter("faculty"));
				facultyModel.setFacultyId(facultyId);
				departmentModel.setFacultyModel(facultyModel);
				departmentModel.setActive((byte)1);
				departmentDAO.addDepartment(departmentModel);
				pw.write(gson.toJson("success"));
				
			}
			if(action.equals("getDepartmentById")){
				Integer departmentId=Integer.parseInt(request.getParameter("departmentId"));
				DepartmentModel  departmentModel=new DepartmentModel();
				departmentModel=departmentDAO.getDepartmentModelById(departmentId);
				DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(departmentModel);
				pw.write(gson.toJson(departmentDTO));
				
			}
			if(action.equals("updateDepartment")){
				DepartmentModel  departmentModel=new DepartmentModel();
				FacultyModel facultyModel=new FacultyModel();
				Integer departmentId=Integer.parseInt(request.getParameter("departmentId"));
				departmentModel.setDepartmentId(departmentId); 
				departmentModel.setDepartmentCode(request.getParameter("departmentCode"));
				departmentModel.setDepartmentName(request.getParameter("departmentName"));
				Integer facultyId=Integer.parseInt(request.getParameter("faculty"));
				facultyModel.setFacultyId(facultyId);
				departmentModel.setFacultyModel(facultyModel);
				departmentModel.setActive((byte)1);
				departmentDAO.updateDepartment(departmentModel);
				pw.write(gson.toJson("success"));
			}
			if(action.equals("deleteDepartment")){
				Integer departmentId=Integer.parseInt(request.getParameter("departmentId"));
				if(departmentId!=null){
					DepartmentModel departmentModel=departmentDAO.getDepartmentModelById(departmentId);
					departmentModel.setActive((byte) 0);
					Integer result=departmentDAO.deleteDepartment(departmentModel);
					if(result>0){
						pw.write(gson.toJson("success"));
					}
					else{
						pw.print("error");
					}
				}
			}
			if(action.equals("getDepartment")){
				HttpSession httpSession = request.getSession();
				if(httpSession.getAttribute("id")!=null){
					Integer userId = Integer.parseInt(httpSession.getAttribute("id").toString());
					UserModel userModel = new UserDAOImpl().getUserById(userId);
					if(userModel.getUserTypeModel().getUserType().toLowerCase().equals("admin")){
						List<DepartmentModel> departmentModels=departmentDAO.getAllDepartments();
						List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
						for(DepartmentModel dm:departmentModels){
							DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(dm);
							DepartmentDTO departmentDTO2=new DepartmentDTO();
							departmentDTOs.add(departmentDTO);				
						}
						String data=gson.toJson(departmentDTOs);
						pw.print(data);
					}else if(userModel.getUserTypeModel().getUserType().toLowerCase().equals("director")){
						List<DepartmentModel> departmentModels=departmentDAO.getAllDepartments();
						List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
						for(DepartmentModel dm:departmentModels){
							DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(dm);
							DepartmentDTO departmentDTO2=new DepartmentDTO();
							departmentDTOs.add(departmentDTO);				
						}
						String data=gson.toJson(departmentDTOs);
						pw.print(data);
					}else if(userModel.getUserTypeModel().getUserType().toLowerCase().equals("teacher")){
						DepartmentModel departmentModel = userModel.getDepartmentModel();
						DepartmentDTO departmentDTO = DepartmentTransform.transformDepartment(departmentModel);
						String data=gson.toJson(departmentDTO);
						pw.write(data);
					}
				}
			}
		}
	}
	private void getDepartments(){
		
	}
	

}
