package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import salu.sis.daoimpl.DepartmentDAOImpl;
import salu.sis.daoimpl.PrefixDAOImpl;
import salu.sis.daoimpl.TeacherDAOImpl;
import salu.sis.daoimpl.UserDAOImpl;
import salu.sis.dto.DepartmentDTO;
import salu.sis.dto.PrefixDTO;
import salu.sis.dto.TeacherDTO;
import salu.sis.dtoTransformation.DepartmentTransform;
import salu.sis.dtoTransformation.PrefixTransform;
import salu.sis.dtoTransformation.TeacherTransform;
import salu.sis.models.DepartmentModel;
import salu.sis.models.PrefixModel;
import salu.sis.models.TeacherModel;
import salu.sis.models.UserModel;

/**
 * Servlet implementation class TeacherServlet
 */
@WebServlet("/TeacherServlet")
public class TeacherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeacherServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter=response.getWriter();
		Gson gson=new Gson();
		response.setContentType("application/json");
		if(request.getParameter("addnewteacher")!=null){
			response.sendRedirect("./admin/addteacher.jsp");
		}
		if(request.getParameter("method")!=null){
			String action=request.getParameter("method");
			if(action.equals("getAllDepartments")){
				List<DepartmentModel> departmentModels=new DepartmentDAOImpl().getAllDepartments();
				List<DepartmentDTO> departmentDTOs=new ArrayList<DepartmentDTO>();
				if(departmentModels!=null){
					for(int i=0; i<departmentModels.size(); i++){
						DepartmentDTO departmentDTO=DepartmentTransform.transformDepartment(departmentModels.get(i));
						departmentDTOs.add(departmentDTO);
					}
				}
				if(departmentModels==null){
					response.sendRedirect("error.jsp");
				}
				else{
					printWriter.write(gson.toJson(departmentDTOs));
				}
			}
			else if(action.equals("getAllPrefixes")){
				List<PrefixModel> prefixModels=new PrefixDAOImpl().getAllPrefixes();
				List<PrefixDTO> prefixDTOs=new ArrayList<PrefixDTO>();
				if(prefixModels!=null){
					for(int i=0; i<prefixModels.size(); i++){
						PrefixDTO prefixDTO=PrefixTransform.transformDepartment(prefixModels.get(i));
						prefixDTOs.add(prefixDTO);
					}
				}
				if(prefixModels==null){
					response.sendRedirect("error.jsp");
				}
				else{
					printWriter.write(gson.toJson(prefixDTOs));
				}			
			}
			else if(action.equals("getAllTeachers")){
				response.setContentType("application/json");
				if(request.getParameter("dId").equals("none")){
					printWriter.write(gson.toJson("none"));
				}
				else{
					Integer departmentId=Integer.parseInt(request.getParameter("dId"));
					if(departmentId!=null){
						List<TeacherModel> teacherModels=new TeacherDAOImpl().getTeachersByDepartment(departmentId);
						List<TeacherDTO> teacherDTOs=new ArrayList<TeacherDTO>();
						if(teacherModels!=null){
							for(int i=0; i<teacherModels.size(); i++){
								TeacherDTO teacherDTO=TeacherTransform.transformTeacher(teacherModels.get(i));
								teacherDTOs.add(teacherDTO);
							}
						}
						if(teacherModels==null){
							response.sendRedirect("error.jsp");
						}
						else{
							printWriter.write(gson.toJson(teacherDTOs));
						}
					}
					
				}
				
				
			}
			else if(action.equals("addTeacher")){
				String firstName=request.getParameter("firstName");
				String lastName=request.getParameter("lastName");
				String prefix=request.getParameter("prefix");
				Integer departmentId=Integer.parseInt(request.getParameter("deptId"));
				if(firstName.trim().equals("") || firstName!=null &&
				lastName.trim().equals("") || lastName!=null &&
				prefix.trim().equals("") || prefix!=null && departmentId!=null){
					TeacherModel teacherModel=new TeacherModel();
					PrefixModel prefixModel=new PrefixModel();
					DepartmentModel departmentModel=new DepartmentModel();
					prefixModel.setPrefixId(Integer.parseInt(prefix));
					departmentModel.setDepartmentId(departmentId);
					teacherModel.setFirstName(firstName);
					teacherModel.setLastName(lastName);
					teacherModel.setActive(1);
					teacherModel.setPrefixModel(prefixModel); 
					teacherModel.setDepartmentModel(departmentModel);
					int row=new TeacherDAOImpl().addTeacher(teacherModel);
					if(row>0){
						printWriter.write(gson.toJson("success"));
					}
					else{
						printWriter.write(gson.toJson("error"));
					}
						
				}
				
			}
			else if(action.equals("getTeacherModelById")){
				Integer teacherId=Integer.parseInt(request.getParameter("teacherId"));
				TeacherModel teacherModel=new TeacherDAOImpl().getTeacherModelById(teacherId);
				TeacherDTO teacherDTO=TeacherTransform.transformTeacher(teacherModel);
				printWriter.write(gson.toJson(teacherDTO));
				
			}
			else if(action.equals("updateTeacher")){
				Integer teacherId=Integer.parseInt(request.getParameter("teacherId"));
				String firstName=request.getParameter("firstName");
				String lastName=request.getParameter("lastName");
				String prefixId=request.getParameter("prefix");
				System.out.println(prefixId);
				Integer departmentId=Integer.parseInt(request.getParameter("deptId"));
				if(firstName.trim().equals("") || firstName!=null &&
				lastName.trim().equals("") || lastName!=null &&
				prefixId.trim().equals("") || prefixId!=null){
					TeacherModel teacherModel=new TeacherModel();
					PrefixModel prefixModel=new PrefixModel();
					DepartmentModel departmentModel=new DepartmentModel();
					prefixModel.setPrefixId(Integer.parseInt(prefixId));
					departmentModel.setDepartmentId(departmentId);
					teacherModel.setTeacherId(teacherId);
					teacherModel.setFirstName(firstName);
					teacherModel.setLastName(lastName);
					teacherModel.setActive(1);
					teacherModel.setPrefixModel(prefixModel); 
					teacherModel.setDepartmentModel(departmentModel);
					int row=new TeacherDAOImpl().updateTeacher(teacherModel);
					if(row>0){
						printWriter.write(gson.toJson("success"));
					}
					else{
						printWriter.write(gson.toJson("error"));
					}
				}	
			}
			else if(action.equals("deleteTeacher")){
				String id=request.getParameter("teacherId");
				if(id!=null){
					Integer teacherId=Integer.parseInt(request.getParameter("teacherId"));
					TeacherModel teacherModel=new TeacherDAOImpl().getTeacherModelById(teacherId);
					teacherModel.setActive(0);
					int row=new TeacherDAOImpl().deleteTeacher(teacherModel);
					if(row>0){
						printWriter.write(gson.toJson("success"));
					}
					else{
						printWriter.write(gson.toJson("error"));
					}
				}
			}
			else if(action.equals("getTeacher")){
				HttpSession httpSession = request.getSession();
				if(httpSession.getAttribute("id")!=null){
					Integer userId = Integer.parseInt(httpSession.getAttribute("id").toString());
					UserModel userModel = new UserDAOImpl().getUserById(userId);
					if(userModel.getUserTypeModel().getUserType().toLowerCase().equals("admin")){
						List<TeacherModel> teacherModels=new TeacherDAOImpl().getAllTeachers();
						List<TeacherDTO> teacherDTOs=new ArrayList<TeacherDTO>();
						for(TeacherModel tm:teacherModels){
							TeacherDTO teacherDTO=TeacherTransform.transformTeacher(tm);
							teacherDTOs.add(teacherDTO);
						}
						String data=gson.toJson(teacherDTOs);
						printWriter.print(data);
					}else if(userModel.getUserTypeModel().getUserType().toLowerCase().equals("director")){
						List<TeacherModel> teacherModels=new TeacherDAOImpl().getAllTeachers();
						List<TeacherDTO> teacherDTOs=new ArrayList<TeacherDTO>();
						for(TeacherModel tm:teacherModels){
							TeacherDTO teacherDTO=TeacherTransform.transformTeacher(tm);
							teacherDTOs.add(teacherDTO);
						}
						String data=gson.toJson(teacherDTOs);
						printWriter.print(data);
					}else if(userModel.getUserTypeModel().getUserType().toLowerCase().equals("teacher")){
						TeacherModel teacherModel=userModel.getTeacherModel();
						TeacherDTO teacherDTO=TeacherTransform.transformTeacher(teacherModel);
						String data=gson.toJson(teacherDTO);
						printWriter.write(data);
					}
				}
			}
		}
	}

}
