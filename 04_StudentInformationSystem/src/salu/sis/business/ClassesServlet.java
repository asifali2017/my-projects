package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.daoimpl.ClassesDAOImpl;
import salu.sis.daoimpl.DepartmentDAOImpl;
import salu.sis.daoimpl.PartDAOImpl;
import salu.sis.daoimpl.ProgramDAOImpl;
import salu.sis.daoimpl.SemesterDAOImpl;
import salu.sis.dto.ClassesDTO;
import salu.sis.dtoTransformation.ClassesDTOTransform;
import salu.sis.models.ClassModel;
import salu.sis.models.DepartmentModel;
import salu.sis.models.PartModel;
import salu.sis.models.ProgramModel;
import salu.sis.models.SemesterModel;
import salu.sis.models.YearModel;

/**
 * Servlet implementation class ClassesServlet
 */
@WebServlet("/ClassesServlet")
public class ClassesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ClassesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("method")!=null)
		{
			PrintWriter pw = response.getWriter();
			Gson gson = new Gson();
			if(request.getParameter("method").equals("getAllClasses"))
			{
				List<ClassModel> classesList = new ClassesDAOImpl().getAllClasses();
				List<ClassesDTO> classesDTOList = new ArrayList<ClassesDTO>();
				if(classesList!=null)
				{
					for(ClassModel c : classesList)
					{
						ClassesDTO classesDTO = ClassesDTOTransform.transform(c);
						classesDTOList.add(classesDTO);
					}
				}
				if(classesDTOList==null)
				{
					response.sendRedirect("error.jsp");
				}else{
					System.out.println(gson.toJson(classesDTOList));
					response.setContentType("application/json");
					pw.write(gson.toJson(classesDTOList));
				}
			}
			else if(request.getParameter("method").equals("getClasessBySemester")){
				System.out.println("Heeeelooo");
				if(!(request.getParameter("semsterId")).equals("none")){
					Integer semsterId=Integer.parseInt(request.getParameter("semsterId"));
					List<ClassModel> classModels=new ClassesDAOImpl().getClassesBySemster(semsterId);
					List<ClassesDTO> classesDTOs=new ArrayList<ClassesDTO>();
					if(classModels!=null){
						for(int i=0; i<classModels.size(); i++){
							ClassesDTO classesDTO=ClassesDTOTransform.transform(classModels.get(i));
							classesDTOs.add(classesDTO);
						}
					}
					if(classModels==null){
						response.sendRedirect("error.jsp");
					}
					else{
						response.setContentType("application/json");
						pw.print(gson.toJson(classesDTOs));
					}
				}	
			}else if(request.getParameter("method").equals("addClass"))
			{
				String programId = request.getParameter("programId");
				String part = request.getParameter("part");
				String departmentId = request.getParameter("departmentId");
				String semesterId = request.getParameter("semesterId");
				String result = "error";
				if(programId.trim().equals("") || programId!=null && part.trim().equals("") || part!=null && semesterId.trim().equals("")
						|| semesterId!=null && departmentId.trim().equals("") || departmentId!=null)
				{
					ProgramModel programModel = new ProgramDAOImpl().getProgramById(Integer.parseInt(programId));
					PartModel partModel = new PartDAOImpl().getPartModelByPartName(part);
					DepartmentModel departmentModel = new DepartmentDAOImpl().getDepartmentModelById(Integer.parseInt(departmentId));
					SemesterModel s = new SemesterModel();
					SemesterModel semesterModel = new SemesterDAOImpl().getSemesterModelById(Integer.parseInt(semesterId));
					System.out.println("Got all models");
					if(programModel==null || departmentModel==null)
					{
						System.out.println("Some models are empty."+programModel.getProgramTitle()+" "+departmentModel.getDepartmentName());
						result = "error";
					}else
					{
						ClassModel classModel = new ClassModel();
						classModel.setActive(1);
						classModel.setPartModel(partModel);
						classModel.setProgramsModel(programModel);
						classModel.setSemesterModel(semesterModel);
						System.out.println("Checking");
						boolean check = new ClassesDAOImpl().validateClasses(classModel);
						if(check==true)
						{
							result="error";
							System.out.println("Record already exist.");
							response.setContentType("application/json");
							System.out.println(gson.toJson("error"));
							pw.write(gson.toJson("error"));
						}else{
							Integer rs = new ClassesDAOImpl().addNewClass(classModel);
							response.setContentType("application/json");
							System.out.println(gson.toJson(rs));
							pw.write(gson.toJson(rs));
						}
					}
					
				}else{
					response.setContentType("application/json");
					pw.write(gson.toJson("error"));
				}
			}else if(request.getParameter("method").equals("searchClasses"))
			{
				String semesterId = request.getParameter("semesterId");
				String departmentId = request.getParameter("departmentId");
				System.out.println(semesterId+" "+departmentId);
				response.setContentType("application/json");
				if(semesterId!=null || semesterId.trim().equals("") && departmentId!=null || departmentId.trim().equals(""))
				{
					SemesterModel semesterModel = new SemesterModel();
					DepartmentModel departmentModel = new DepartmentModel();
					if(!semesterId.equals("none"))
					{
						semesterModel.setSemesterId(Integer.parseInt(semesterId));
					}
					if(!departmentId.equals("none"))
					{
						departmentModel.setDepartmentId(Integer.parseInt(departmentId));
					}
					ProgramModel programModel = new ProgramModel();
					programModel.setDepartmentModel(departmentModel);
					
					List<ClassModel> classesList = new ClassesDAOImpl().searchClasses(programModel, semesterModel);
					List<ClassesDTO> classesDTOList = new ArrayList<ClassesDTO>();
					if(classesList!=null)
					{
						for(ClassModel c : classesList)
						{
							ClassesDTO classesDTO = ClassesDTOTransform.transform(c);
							classesDTOList.add(classesDTO);
						}
					}
					if(classesDTOList==null)
					{
						response.sendRedirect("error.jsp");
					}else{
						System.out.println(gson.toJson(classesDTOList));
						response.setContentType("application/json");
						pw.write(gson.toJson(classesDTOList));
					}

				}else {

					pw.write("error");
				}
			}else if(request.getParameter("method").equals("deleteClass"))
			{
				Integer classId = Integer.parseInt(request.getParameter("classId"));
				ClassModel classModel = new ClassModel();
				classModel.setActive(0);
				classModel.setClassId(classId);
				if(classId!=null || classId.equals(""))
				{
					Integer result = new ClassesDAOImpl().deleteClass(classModel);
					response.setContentType("application/json");
					System.out.println(gson.toJson(result));
					pw.write(gson.toJson(result));
				}
				
			}
			
		}
	}

}
