package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.daoimpl.SemesterDAOImpl;
import salu.sis.daoimpl.StudentDAOImpl;
import salu.sis.dto.SemesterDto;
import salu.sis.dto.StudentDTO;
import salu.sis.dtoTransformation.SemesterDtoTransformation;
import salu.sis.dtoTransformation.StudentTransform;
import salu.sis.models.ClassModel;
import salu.sis.models.SemesterModel;
import salu.sis.models.StudentModel;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter printWriter=response.getWriter();
		Gson gson=new Gson();
		response.setContentType("application/json");
		if(request.getParameter("addnewstudent")!=null){
			Integer classId=Integer.parseInt(request.getParameter("class"));
			response.sendRedirect("admin/addstudent.jsp?classId="+classId+"");
			
		}
		if(request.getParameter("method")!=null){
			String action=request.getParameter("method");
			if(action.equals("getAllSemsters")){
				List<SemesterModel> semesterModels=new SemesterDAOImpl().viewSemesterData();
				List<SemesterDto> semesterDtos=new ArrayList<SemesterDto>();
				if(semesterModels!=null){
					for(int i=0; i<semesterModels.size(); i++){
						SemesterDto semesterDto=SemesterDtoTransformation.transform(semesterModels.get(i));
						semesterDtos.add(semesterDto);
					}
				}
				if(semesterModels==null){
					response.sendRedirect("error.jsp");
				}
				else{
					printWriter.write(gson.toJson(semesterDtos));
				}
			}
			else if(action.equals("getAllStudents")){
				String id=request.getParameter("classId");
				System.out.println("classs id "+id);
				if(id!=null){
					Integer classId=Integer.parseInt(id);
					List<StudentModel> studentModels=new StudentDAOImpl().getAllStudentsByClass(classId);
					List<StudentDTO> studentDTOs=new ArrayList<StudentDTO>();
					if(studentModels!=null){
						for(int i=0; i<studentModels.size(); i++){
							StudentDTO studentDTO=StudentTransform.transform(studentModels.get(i));
							studentDTOs.add(studentDTO);	
						}
					}
					if(studentModels==null){
						response.sendRedirect("error.jsp");
					}
					else{
						printWriter.write(gson.toJson(studentDTOs));
					}
				}		
			}
			else if(action.equals("addStudent")){
				String firstName=request.getParameter("firstName");
				String middleName=request.getParameter("middleName");
				String lastName=request.getParameter("lastName");
				String rollNumber=request.getParameter("rollNumber");
				boolean found=new StudentDAOImpl().checkRollNumber(rollNumber);
				if(found){
					printWriter.write(gson.toJson("rollNumber exists"));
				}else{
					Integer classId=Integer.parseInt(request.getParameter("classId"));
					if(firstName.trim().equals("") || firstName!=null && 
					middleName.trim().equals("") || middleName!=null &&
					lastName.trim().equals("") || lastName!=null &&
					classId!=null){
						StudentModel studentModel=new StudentModel();
						ClassModel classModel=new ClassModel();
						studentModel.setFirstName(firstName);
						studentModel.setMiddleName(middleName);
						studentModel.setLastName(lastName);
						studentModel.setRollNumber(rollNumber);
						studentModel.setActive(1);
						classModel.setClassId(classId);
						studentModel.setClassModel(classModel); 
						int row=new StudentDAOImpl().addStudent(studentModel);
						if(row>0){
							printWriter.print(gson.toJson("success")); 
						}
						else{
							printWriter.print(gson.toJson("error"));
						}
						
					}
				}
				
				
			}
			else if(action.equals("getStudentModelById")){
				Integer studentId=Integer.parseInt(request.getParameter("studentId"));
				StudentModel studentModel=new StudentDAOImpl().getStudentById(studentId);
				StudentDTO studentDTO=StudentTransform.transform(studentModel);
				printWriter.write(gson.toJson(studentDTO)); 
			}
			else if(action.equals("updateStudent")){
				Integer studentId=Integer.parseInt(request.getParameter("studentId"));
				String firstName=request.getParameter("firstName");
				String middleName=request.getParameter("middleName");
				String lastName=request.getParameter("lastName");
				String rollNumber=request.getParameter("rollNumber");
				boolean found=new StudentDAOImpl().checkRollNumberWithId(rollNumber, studentId);
				if(found){
					printWriter.write(gson.toJson("rollNumber exists"));
				}
				else{
					if(firstName.trim().equals("") || firstName!=null && 
							middleName.trim().equals("") || middleName!=null &&
							lastName.trim().equals("") || lastName!=null &&
							studentId!=null){
								StudentModel studentModel=new StudentDAOImpl().getStudentById(studentId);
								ClassModel classModel=new ClassModel();
								classModel.setClassId(studentModel.getClassModel().getClassId());
								studentModel.setStudentId(studentId);
								studentModel.setFirstName(firstName);
								studentModel.setMiddleName(middleName);
								studentModel.setLastName(lastName);
								studentModel.setRollNumber(rollNumber);
								studentModel.setActive(1);
								studentModel.setClassModel(classModel); 
								int row=new StudentDAOImpl().updateStudent(studentModel);
								if(row>0){
									printWriter.print(gson.toJson("success")); 
								}
								else{
									printWriter.print(gson.toJson("error"));
								}
							}
				}
				
			}
			else if(action.equals("deleteStudent")){
				Integer studentId=Integer.parseInt(request.getParameter("studentId"));
				if(studentId!=null){
					StudentModel studentModel=new StudentDAOImpl().getStudentById(studentId);
					studentModel.setActive(0); 
					int row=new StudentDAOImpl().deleteStudent(studentModel);
					if(row>0){
						printWriter.print(gson.toJson("success")); 
					}
					else{
						printWriter.print(gson.toJson("error"));
					} 
				}
			}
		}		
	}
}
