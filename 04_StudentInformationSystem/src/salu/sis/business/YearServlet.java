package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import salu.sis.dao.YearDAO;
import salu.sis.dao.YearDAO;
import salu.sis.daoimpl.YearDAOImpl;
import salu.sis.daoimpl.YearDAOImpl;

import salu.sis.dto.YearDto;
import salu.sis.models.YearModel;
import salu.sis.dtoTransformation.*;

/**
 * Servlet implementation class YearServlet
 */
@WebServlet("/YearServlet")
public class YearServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public YearServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter pw=response .getWriter();
		YearDAO yearDAO=new YearDAOImpl();
		if(request.getParameter("method")!=null){
			if(request.getParameter("method").equals("addRecord")){
				String yearName=request.getParameter("yearName");
				YearModel yearModel=new YearModel();
				yearModel.setYearName(yearName);
				yearModel.setActive((byte)1);
				Integer row=yearDAO.addYear(yearModel);
				pw.print(row);
			}
			
			else if(request.getParameter("method").equals("updateRecord")){
				Integer yearId=Integer.parseInt(request.getParameter("yearId"));
				String yearName=request.getParameter("yearName");
				YearModel yearModel=new YearModel();
				yearModel.setYearId(yearId);
				yearModel.setYearName(yearName);
				yearModel.setActive((byte)1);
				Integer row=yearDAO.updateYear(yearModel);
				pw.print(row);
			}
			
			else if(request.getParameter("method").equals("deleteRecord")){
				Integer yearId=Integer.parseInt(request.getParameter("deleteId"));
				Integer row=yearDAO.deleteYear(yearId);
				pw.print(row);
			}
			
			else if(request.getParameter("method").equals("getAllRecords")){
				List<YearModel> yearList=yearDAO.getAllYears();
				List<YearDto> yearDTOList=new ArrayList<>();
				if(!yearList.isEmpty()){
					for(int i=0;i<yearList.size();i++){
						YearDto yearDTO=new YearDto();
						yearDTO=YearTransformer.transform(yearList.get(i));
						yearDTOList.add(yearDTO);
					}
					Gson gson=new Gson();
					String data=gson.toJson(yearDTOList);
					System.out.println(data);
					pw.print(data);
			}
				
			}else if(request.getParameter("method").equals("getRecordById")){
				Integer yearId=Integer.parseInt(request.getParameter("editId"));
				YearModel yearModel=yearDAO.getYearById(yearId);
				if(yearModel!=null){
					YearDto yearDTO=YearTransformer.transform(yearModel);
					Gson gson=new Gson();
					String data=gson.toJson(yearDTO);
					pw.print(data);
				}
			}
		}
	}

}
