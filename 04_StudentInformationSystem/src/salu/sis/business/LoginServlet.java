package salu.sis.business;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import salu.sis.daoimpl.LoginDAOImpl;
import salu.sis.daoimpl.UserDAOImpl;
import salu.sis.models.UserModel;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter printWriter = response.getWriter();
		Integer userId = 0;
		String userType = "";
		String userName = "";
		String path = null;
		if (request.getParameter("method") != null) {
			if (request.getParameter("method").equals("login")) {

				String user = request.getParameter("username");
				String password = request.getParameter("password");

				List<UserModel> users = new LoginDAOImpl().login(user, password);
				for (UserModel userModel : users) {
					userId = userModel.getUserId();
					userType = userModel.getUserTypeModel().getUserType();
					userName = userModel.getUserName();
					path = userModel.getImagePath();
				}
				if (users.isEmpty()) {

					printWriter.print("unsuccess");

				} else {
					HttpSession httpSession = request.getSession();
					httpSession.setAttribute("name", userName);
					httpSession.setAttribute("id", userId);
					httpSession.setAttribute("usertype", userType);
					httpSession.setAttribute("path", path);
					printWriter.print("success");
				}

			} else if (request.getParameter("method").equals("checkUserPassword")) {
				HttpSession httpSession = request.getSession();
				int user = Integer.parseInt(httpSession.getAttribute("id").toString());
				String password = request.getParameter("password");
				List<UserModel> users = new LoginDAOImpl().checkOldPassword(user, password);
				if (users.isEmpty()) {

					printWriter.print("unsuccess");
				} else {

					printWriter.print("success");
				}
			}else if(request.getParameter("method").equals("updateUserPassword")){
				HttpSession httpSession = request.getSession();
				int user = Integer.parseInt(httpSession.getAttribute("id").toString());
				String password = request.getParameter("newpassword");
				int row = new LoginDAOImpl().updateUserPassword(user, password);
				printWriter.print(row);		
			}

		}

	}

}
