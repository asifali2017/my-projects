 package salu.sis.utils;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet Filter implementation class AuthenticationFilter
 */
@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {

    /**
     * Default constructor. 
     */
    public AuthenticationFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		// pass the request along the filter chain
		HttpServletRequest httpRequst=(HttpServletRequest)request;
		HttpServletResponse httpResponse=(HttpServletResponse)response;
		HttpSession session=httpRequst.getSession();
		String uri=httpRequst.getRequestURI();
		System.out.println(uri);
		if((session==null || session.getAttribute("name")==null)  && httpRequst.getRequestURI().endsWith("jsp")) {
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("login.jsp");
			rd.forward(httpRequst, httpResponse);
		}
		else if(session.getAttribute("name")!=null && uri.equals("index.jsp"))
		{
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("index.jsp");
			rd.forward(httpRequst, httpResponse);
		}
		else if (session.getAttribute("name")!=null && uri.equals("faculty.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("faculty.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("year.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("year.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("semester.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("semester.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("user.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("user.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("program.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("program.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("teacher.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("teacher.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("student.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("student.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("course.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("course.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("class.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("class.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("courseassignment.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("courseassignment.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("assigncourse.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("assigncourse.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("addcourse.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("addcourse.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("addstudent.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("addstudent.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("addteacher.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("addteacher.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("adduser.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("adduser.jsp");
			rd.forward(httpRequst, httpResponse);
                                  			
		}else if (session.getAttribute("name")!=null && uri.equals("new-class.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("new-class.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("new-part.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("new-part.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("programentry.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("programentry.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("cng-password.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("cng-password.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}
        else if (session.getAttribute("name")!=null && uri.equals("insertattendance.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("insertattendance.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("labreport.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("labreport.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}else if (session.getAttribute("name")!=null && uri.equals("viewlabreport.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("viewlabreport.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}
       else if (session.getAttribute("name")!=null && uri.equals("course-assignment.jsp")){
			
			RequestDispatcher rd=	httpRequst.getRequestDispatcher("course-assignment.jsp");
			rd.forward(httpRequst, httpResponse);
			
		}
		else {
			chain.doFilter(httpRequst, httpResponse);
			
		}
			

		}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
