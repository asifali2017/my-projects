package salu.sis.dao;

import java.util.List;

import salu.sis.models.CourseAssignmentModel;

public interface CourseAssignmentDAO {

	public int assignCourse(CourseAssignmentModel courseAssignmentModel);
	public List<CourseAssignmentModel> getAllAssignedCoursesWithClass(Integer classId);
	public CourseAssignmentModel getCousreAssignmentModel(Integer courseId);
	public int updateCourseAssignment(CourseAssignmentModel courseAssignmentModel);
	public int deleteCourseAssignment(int courseAssignmentId);
	public List<CourseAssignmentModel> getCourseAssignments(Integer classId,Integer teacherId);
}
