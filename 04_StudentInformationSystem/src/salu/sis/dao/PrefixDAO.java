package salu.sis.dao;

import java.util.List;

import salu.sis.models.PrefixModel;

public interface PrefixDAO {
	public List<PrefixModel> getAllPrefixes();
}
