package salu.sis.dao;

import salu.sis.models.PartModel;

public interface PartDAO {

	public PartModel getPartModelByPartName(String partName);
}
