package salu.sis.dao;

import java.util.List;

import salu.sis.models.UserModel;
import salu.sis.models.UserTypeModel;

public interface UserDAO {

	public List<UserModel> getAllUsers(UserModel userModel);

	public List<UserTypeModel> getAllUsersTypes();

	public UserModel getUserById(int userId);

	public int insertUserData(UserModel userModel);

	public int updateUserData(UserModel userModel);

	public int deleteUserData(int id);
	
	public boolean checkUser(String userName);
	
	public int setUserImage(UserModel userModel);
	
}
