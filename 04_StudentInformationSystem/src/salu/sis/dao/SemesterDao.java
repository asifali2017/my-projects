package salu.sis.dao;

import java.util.List;

import salu.sis.models.*;
public interface SemesterDao {

  public int addNewSemester(SemesterModel semesterModel);
  public List<SemesterModel> viewSemesterData();
  public SemesterModel getSemesterModelById(Integer id);	
  public int updateSemester(SemesterModel semester);
  public int deleteSemester(int id);
  public SemesterModel getSemesterModel(SemesterModel semesterModel);
  public List<SemesterModel> getAllSemester();
}
