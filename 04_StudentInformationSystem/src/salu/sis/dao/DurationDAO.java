package salu.sis.dao;

import java.util.List;

import salu.sis.models.DurationModel;

public interface DurationDAO {
	public DurationModel getDurationById(Integer durationId);
	public List<DurationModel> getAllDurations();
}
