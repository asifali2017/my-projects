package salu.sis.dao;

import java.util.List;

import salu.sis.models.ProgramModel;

public interface ProgramDAO {
	public Integer addProgram(ProgramModel programModel);
	public Integer updateProgram(ProgramModel programModel);
	public Integer deleteProgram(Integer programId);
	public List<ProgramModel> getAllPrograms();
	public List<ProgramModel> getAllProgramsDepartmentWise(Integer departmentId);
	public ProgramModel getProgramById(Integer programId);
	public ProgramModel getProgramByName(String programName);
}
