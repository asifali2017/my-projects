package salu.sis.dao;

import java.util.List;

import salu.sis.models.DepartmentModel;

public interface DepartmentDAO {
	public Integer addDepartment(DepartmentModel departmentModel);
	public Integer deleteDepartment(DepartmentModel departmentModel);
	public Integer updateDepartment(DepartmentModel departmentModel);
	public List<DepartmentModel> getAllDepartments();
	public DepartmentModel getDepartmentModelById(Integer departmentId);
	public DepartmentModel getDepartmentModelByName(String departmentName);
	public List<DepartmentModel> getDepartmentWithFacultyId(Integer facultyId);
	public boolean checkDepartmentCode(String departmentCode);
	public boolean checkDepartmentName(String departmentName);
	public boolean checkCode(String code,Integer id);
	public boolean checkName(String name,Integer id);
}
