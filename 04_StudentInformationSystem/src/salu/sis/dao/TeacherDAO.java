package salu.sis.dao;

import java.util.List;


import salu.sis.models.TeacherModel;

public interface TeacherDAO {
	public Integer addTeacher(TeacherModel teacherModel);
	public Integer updateTeacher(TeacherModel teacherModel);
	public Integer deleteTeacher(TeacherModel teacherModel);
	public TeacherModel getTeacherModelById(Integer teacherId);
	public List<TeacherModel> getAllTeachers();
	public List<TeacherModel> getTeachersByDepartment(Integer departmentId);
}
