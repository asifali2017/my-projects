package salu.sis.dao;

import java.util.List;

import salu.sis.models.CourseModel;

public interface CourseDAO {
	public Integer addCourse(CourseModel courseModel);
	public Integer deleteCourse(Integer courseId);
	public Integer updateCourse(CourseModel courseModel);
	public List<CourseModel> getAllCourses();
	public CourseModel getCourseById(Integer courseId);
	public List<CourseModel> getCoursesByDepartment(Integer departmentId);
}
