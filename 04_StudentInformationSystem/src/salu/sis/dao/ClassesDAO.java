package salu.sis.dao;

import java.util.List;

import salu.sis.models.ClassModel;
import salu.sis.models.DepartmentModel;
import salu.sis.models.ProgramModel;
import salu.sis.models.SemesterModel;
import salu.sis.models.YearModel;
// by Kashif
public interface ClassesDAO {

	public Integer addNewClass(ClassModel classModel);
	public Integer updateClass(ClassModel classModel);
	public Integer deleteClass(ClassModel classModel);
	public ClassModel getClassById(Integer classId);
	public List<ClassModel> getAllClasses();
	public List<ClassModel> searchClasses(ProgramModel programModel,SemesterModel semesterModel);
	public boolean validateClasses(ClassModel classModel);
	public List<ClassModel> getClassesWithSemesterAndProgram(ClassModel classModel);
	public List<ClassModel> getClassesBySemster(Integer semsterId);

}
