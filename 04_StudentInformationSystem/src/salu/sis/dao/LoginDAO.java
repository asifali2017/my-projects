package salu.sis.dao;


import java.util.List;

import salu.sis.models.UserModel;

public interface LoginDAO {

	public List<UserModel>  login(String userName , String password);
	public List<UserModel> checkOldPassword(Integer userId , String oldPassword);
	public int updateUserPassword(Integer userId , String newPassword);
}
