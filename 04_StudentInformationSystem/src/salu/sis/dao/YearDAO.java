package salu.sis.dao;

import java.util.List;

import salu.sis.models.YearModel;

public interface YearDAO {
	public Integer addYear(YearModel yearModel);

	public List<YearModel> getAllYears();

	public Integer updateYear(YearModel yearModel);

	public YearModel getYearById(int id);

	public Integer deleteYear(int deleteId);

	public YearModel getYearByName(String year);
}
