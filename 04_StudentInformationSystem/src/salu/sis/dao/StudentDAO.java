package salu.sis.dao;

import java.util.List;

import salu.sis.models.StudentModel;

public interface StudentDAO {
	public Integer addStudent(StudentModel studentModel);
	public Integer deleteStudent(StudentModel studentModel);
	public Integer updateStudent(StudentModel studentModel);
	public List<StudentModel> getAllStudents();
	public StudentModel getStudentById(Integer studentId);
	public List<StudentModel> getAllStudentsByClass(Integer classId);
	public boolean checkRollNumber(String rollNumber);
	public boolean checkRollNumberWithId(String rollNumber,Integer studentId);
	

}
