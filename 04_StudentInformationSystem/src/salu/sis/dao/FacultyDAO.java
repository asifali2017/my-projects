package salu.sis.dao;

import java.util.List;

import salu.sis.models.FacultyModel;

public interface FacultyDAO {
	public Integer addFaculty(FacultyModel facultyModel);
	public Integer updateFaculty(FacultyModel facultyModel);
	public Integer deleteFaculty(Integer facultyId);
	public List<FacultyModel> getAllFaculties();
	public FacultyModel getFacultyById(Integer facultyId);
	public Integer getFacultyIdByName(String facultyName);
}
